<?php include("kl_inc_header.php"); ?>

<?php 
	include 'db/dbheader.php';
	include 'db/Iregion.class.php';
	include 'db/IregionDAO.class.php';
	include 'db/Iregion_custom_properties.class.php';
	include 'db/Iregion_custom_propertiesDAO.class.php';
	include 'db/Displaygroup.class.php';
	include 'db/DisplaygroupDAO.class.php';

	$displaygroupDAO=new DisplaygroupDAO($con);
	$displayGroup=$displaygroupDAO->load($_GET['id']);
?>

	<body>

<?php include("kl_inc_navbar.php"); ?>



<?php include("kl_inc_sidebar.php"); ?>

                    

			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<script type="text/javascript">
						try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
					</script>

			
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="#">Home</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>

							<a href="kl_groups.php"> Display Groups </a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
								<?php echo $displayGroup->DisplayGroup ?>


						</span></li>
					</ul>

					<?php include("kl_inc_nav_search.php"); ?></div>

				<div class="page-content">
               
				  <div class="row-fluid">
                  	  <h3>Layout for <?php echo $displayGroup->DisplayGroup ?>
</h3>
					  
					
					<div class="span12">
						<div class="kl_layout_container" style="width:1024px; height:768px;">
					 <?php
						$regionsDAO = new IregionDAO($con);
						$regions = $regionsDAO->findByDisplayGroup_Id($_GET['id']);
						foreach ($regions as $region) {

							//echo json_encode($region);
							$region_cust_prop=new Iregion_custom_propertiesDAO($con);
							$style = new stdClass();
							foreach ($region_cust_prop->findById($region->id) as $region_custom_properties) {
								echo json_encode($region_custom_properties);
								$style->{$region_custom_properties->property}=$region_custom_properties->value;
							}
								?>

								<div class="kl_region video <?php echo $region->type ?>" style="left:<?php echo $style->left ?>px; top:<?php echo $style->top ?>px; width:<?php echo $style->width ?>px; height:<?php echo $style->height ?>px;">
							
										<div class="kl_region_controls_right">
										
												<a class="btn btn-pink btn-mini" href="kl_segments.php?id=<?php echo $region->id ?>">
													<i class="icon-tasks"></i>
													segments
												</a>
												
												
										</div>
										
										
							
							    </div>


								<?php




							
						}
							?>	
							<!--
						
						
							<div class="kl_region video main" style="left:0px; top:100px; width:800px; height:450px;">
							
										<div class="kl_region_controls_right">
										
												<a class="btn btn-pink btn-mini" href="kl_segments.php">
													<i class="icon-tasks"></i>
													segments
												</a>
												
												
										</div>
										
										<div class="kl_region_controls_left">

											
												<a class="btn btn-mini btn-light">
													<i class="icon-tasks"></i>
													properties
												</a>
			
										</div>
							
							</div>
							
							<div class="kl_region banner" style="left:800px; top:0px; height:768px; width:224px;">


																<div class="kl_region_controls_right">
										
												<a class="btn btn-pink btn-mini" href="kl_segments.php">
													<i class="icon-tasks"></i>
													segments
												</a>
												
												
										</div>
										
										<div class="kl_region_controls_left">

											
												<a class="btn btn-mini btn-light">
													<i class="icon-tasks"></i>
													properties
												</a>
			
										</div>
							
							</div>
							
							<div class="kl_region ticker" style="left:0px; top:0px; height:100px; width:800px;">

																<div class="kl_region_controls_right">
										
												<a class="btn btn-pink btn-mini" href="kl_segments.php">
													<i class="icon-tasks"></i>
													segments
												</a>
												
												
										</div>
										
										<div class="kl_region_controls_left">

											
												<a class="btn btn-mini btn-light">
													<i class="icon-tasks"></i>
													properties
												</a>
			
										</div>
							
							</div>
							
							<div class="kl_region sponsor" style="left:0px; top:550px; height:230px; width:800px;">
							
								<div class="kl_region_controls_right">
										
												<a class="btn btn-pink btn-mini" href="kl_segments.php">
													<i class="icon-tasks"></i>
													segments
												</a>
												
												
										</div>
										
										<div class="kl_region_controls_left">

											
												<a class="btn btn-mini btn-light">
													<i class="icon-tasks"></i>
													properties
												</a>
			
										</div>

							
							</div>
							-->
						
						</div>
					</div>

					  
					  
				  
				  </div>






				</div><!--/.page-content-->

<?php include("kl_inc_ace_settings.php"); ?>




			</div><!--/.main-content-->
		</div><!--/.main-container-->

		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
			<i class="icon-double-angle-up icon-only bigger-110"></i>
		</a>

		<!--basic scripts-->

		<!--[if !IE]>-->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!--<![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>

		<!--page specific plugin scripts-->
        
        	<script src="assets/js/jquery.dataTables.min.js"></script>
		<script src="assets/js/jquery.dataTables.bootstrap.js"></script>

		<!--ace scripts-->

		<script src="assets/js/ace-elements.min.js"></script>
		<script src="assets/js/ace.min.js"></script>

		<!--inline scripts related to this page-->

		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#layout_regions').dataTable( );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
		</script>
	</body>
</html>
