<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/Isms_actionDAOAbstract.class.php';
class Isms_actionDAO extends Isms_actionDAOAbstract {
	
		public function updatevalue($isms_action) {
		$ps = new PreparedStatement("INSERT INTO isms_action 
		(action_id, content) VALUES (?, ?) ON DUPLICATE KEY UPDATE content=?");
		$ps->setInt($isms_action->action_id);
		$ps->setString($isms_action->content);
		$ps->setString($isms_action->content);
		return $this->connection->executeUpdate($ps);
	}

}
