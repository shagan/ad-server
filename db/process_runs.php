<?php
	include 'dbheader.php';
    include 'Irun.class.php';
    include 'IrunDAO.class.php';

    if(isset($_GET["action"])){
        if(isset($_GET["startDate"]) && isset($_GET["endDate"])){

        	$runDAO=new IrunDAO($con);
        	$runs = $runDAO->find($_GET["segment_id"], $_GET["startDate"],$_GET["endDate"]);
        	echo json_encode($runs);
        }
    }
    else if(isset($_POST["action"])){

        if("create"==$_POST["action"]){
            $run = new Irun();
            $run->loadFromArray($_POST);
            $runDAO=new IrunDAO($con);
            $runDAO->insert($run);
            echo mysql_insert_id();
        }
        else if("update"==$_POST["action"]){
            $run = new Irun();
            $run->loadFromArray($_POST);
            $runDAO=new IrunDAO($con);
            echo $runDAO->update($run);
        }
        else if("delete"==$_POST["action"]){
            $run = new Irun();
            $run->loadFromArray($_POST);
            $runDAO=new IrunDAO($con);
            echo $runDAO->delete($_POST["id"]);
        }

    }
?>