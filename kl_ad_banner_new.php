<?php // kl_ad_banner_new.php
include("kl_inc_header.php"); ?>
<body>
	<?php include("kl_inc_navbar.php"); ?>
	<?php include("kl_inc_sidebar.php"); ?>
	<div class="main-content">
		<div class="breadcrumbs" id="breadcrumbs"> 
			<script type="text/javascript">
			try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
			</script>
			<?php include("kl_inc_breadcrumbs.php"); ?>
			<?php include("kl_inc_nav_search.php"); ?>
        </div>
        <?php
		//
		// Initialise Variables to Add
		$edit = new DOMDocument;
		$edit->bannername   = '';
		$edit->title        = '';
		$edit->subtitle     = '';
		$edit->buttonname   = '';
		$edit->popupname    = '';
		$edit->popuptype    = '';
		$edit->videoid      = '';
		$edit->websitename  = '';
		$edit->introtext    = '';
		$edit->exittext     = '';
		$edit->emailsubject = '';
		$edit->emailcontent = '';
		$edit->smscontent   = '';
		$edit->qrcontent    = '';
		$edit->radio1popup  = 'checked';
		$edit->radio2popup  = '';
		$edit->radio3popup  = '';
		$edit->qrcheck1     = 'checked';
		$edit->qrcheck2     = '';
		$edit->qrcheck3     = '';
		$edit->qrcheck4     = '';
		$edit->switchfield1 = '';
		$edit->switchfield2 = '';
		$edit->switchfield3 = '';
		// Populate variables for Edit
		if (isset($_GET['addedit'])) {
            // Include file headers for DAO
            include 'db/dbheader.php';
            include 'db/Isequence.class.php';
            include 'db/IsequenceDAO.class.php';
            include 'db/Isequence_custom_properties.class.php';
            include 'db/Isequence_custom_propertiesDAO.class.php';
			include 'db/Ipopup.class.php';
            include 'db/IpopupDAO.class.php';
            include 'db/Ipopup_custom_properties.class.php';
            include 'db/Ipopup_custom_propertiesDAO.class.php';
            include 'db/Ilkbannerpopup.class.php';
            include 'db/IlkbannerpopupDAO.class.php';
            include 'db/Iaction.class.php';
            include 'db/IactionDAO.class.php';
            include 'db/Iaction_custom_properties.class.php';
            include 'db/Iaction_custom_propertiesDAO.class.php';
            include 'db/Ilkpopupaction.class.php';
            include 'db/IlkpopupactionDAO.class.php';
            include 'db/Iemail_action.class.php';
            include 'db/Iemail_actionDAO.class.php';
            include 'db/Isms_action.class.php';
            include 'db/Isms_actionDAO.class.php';
            include 'db/Iqrcode_action.class.php';
            include 'db/Iqrcode_actionDAO.class.php';
            include 'db/Media.class.php';
            include 'db/MediaDAO.class.php';
            include 'db/Ilkadvertisermedia.class.php';
            include 'db/IlkadvertisermediaDAO.class.php';
            include 'db/Ilkcampaignsequence.class.php';
            include 'db/IlkcampaignsequenceDAO.class.php';
            include 'db/Setting.class.php';
            include 'db/SettingDAO.class.php';
            include 'db/Ilkadvertisercampaign.class.php';
            include 'db/IlkadvertisercampaignDAO.class.php';

            // Query Sequence File
            $sequenceDAO = new IsequenceDAO($con);
            $sequences = $sequenceDAO->findById($_GET['sequence_id']);
            foreach ($sequences as $sequence) {$edit->bannername = $sequence->name;}
            // Query Sequence Custom Properties File
            $seqpropDAO = new Isequence_custom_propertiesDAO($con);
            $seqprops = $seqpropDAO->findById($_GET['sequence_id']);
            foreach ($seqprops as $seqprop) {
	            if ($seqprop->property == 'title') {$edit->title = $seqprop->value;}
	            if ($seqprop->property == 'subtitle') {$edit->subtitle = $seqprop->value;}
	            if ($seqprop->property == 'buttonname') {$edit->buttonname = $seqprop->value;}
            }
            // Get Popup ID and Query Popup Custom Properties File
            $ilkbannerpopupDAO = new IlkbannerpopupDAO($con);
            $sequences = $ilkbannerpopupDAO->findBySequence_id($_GET['sequence_id']);
	        foreach ($sequences as $sequence) {$edit->popup_id = $sequence->popup_id;}
            $popuppropDAO = new Ipopup_custom_propertiesDAO($con);
            $popupprops = $popuppropDAO->findById($edit->popup_id);
            foreach ($popupprops as $popupprop) {
	            if ($popupprop->property == 'popupname') {$edit->popupname = $popupprop->value;}
	            if ($popupprop->property == 'type') {$edit->popuptype = $popupprop->value;}
	            if ($popupprop->property == 'uri') {$edit->uri = $popupprop->value;}
	            if ($popupprop->property == 'videoid') {$edit->videoid = $popupprop->value;}
            }
			if ($edit->popuptype == 'image') {$edit->radio1popup  = 'checked';}
			if ($edit->popuptype == 'video') {$edit->radio2popup  = 'checked'; $edit->videochecked = $edit->uri;}
			if ($edit->popuptype == 'website') {$edit->radio3popup  = 'checked'; $edit->websitename = $edit->uri;}
            // Query Action Custom Properties File
			$ilkpopupactionDAO=new IlkpopupactionDAO($con);
        	$actions = $ilkpopupactionDAO->findByPopup_id($edit->popup_id);
            foreach ($actions as $action) {$edit->action_id = $action->action_id;}
            $actpropDAO = new Iaction_custom_propertiesDAO($con);
            $actprops = $actpropDAO->findById($edit->action_id);
            foreach ($actprops as $actprop) {
	            if ($actprop->property == 'introtext') {$edit->introtext = $actprop->value;}
	            if ($actprop->property == 'exittext') {$edit->exittext = $actprop->value;}
            }
            // Query Action File
            $actionDAO = new IactionDAO($con);
            $actions = $actionDAO->findById($edit->action_id);
            foreach ($actions as $action) {$edit->actiontypes = $action->type;}
			if (strpos($edit->actiontypes,'EMAIL') !== false) {$edit->switchfield1  = 'checked';}
			if (strpos($edit->actiontypes,'SMS') !== false) {$edit->switchfield2  = 'checked';}
			if (strpos($edit->actiontypes,'QRCODE') !== false) {$edit->switchfield3  = 'checked';}
            // Query Email File
            $emailactionDAO = new Iemail_actionDAO($con);
            $emailactions = $emailactionDAO->findByAction_id($edit->action_id);
            foreach ($emailactions as $emailaction) {$edit->emailsubject = $emailaction->subject; $edit->emailcontent = $emailaction->body;}
            // Query SMS File
            $smsactionDAO = new Isms_actionDAO($con);
            $smsactions = $smsactionDAO->findByAction_id($edit->action_id);
            foreach ($smsactions as $smsaction) {$edit->smscontent = $smsaction->content;}
            // Query QRCode File
            $qrcodeactionDAO = new Iqrcode_actionDAO($con);
            $qrcodeactions = $qrcodeactionDAO->findByAction_id($edit->action_id);
            foreach ($qrcodeactions as $qrcodeaction) {
				$edit->qrcontent = $qrcodeaction->content;
	            if ($qrcodeaction->type == 'EMAIL')   {$edit->qrcheck1 = 'checked';}
	            if ($qrcodeaction->type == 'PHONE')   {$edit->qrcheck2 = 'checked';}
	            if ($qrcodeaction->type == 'TEXT')    {$edit->qrcheck3 = 'checked';}
	            if ($qrcodeaction->type == 'WEBSITE') {$edit->qrcheck4 = 'checked';}
				//error_log('$qrcodeaction->type: '.$qrcodeaction->type.'**'.$qrcodeaction->content.'**'.$qrcodeaction->action_id);
			}
		}

        ?>
			<div class="page-content">
				<div class="page-header position-relative">
					<h1> Form Wizard <small> <i class="icon-double-angle-right"></i> and Validation </small> </h1>
				</div>
				<!--/.page-header-->

				<div class="row-fluid">
					<div class="span12"> 
						<!--PAGE CONTENT BEGINS-->

						<div class="row-fluid">
							<div class="span12">
								<div class="widget-box">
									<div class="widget-header widget-header-blue widget-header-flat">
										<h4 class="lighter">New Item Wizard</h4>


									</div>

									<div class="widget-body">
										<div class="widget-main">
											<div class="row-fluid">
												<div id="fuelux-wizard" class="row-fluid hide" data-target="#step-container">
													<ul class="wizard-steps">
														<li data-target="#step1" class="active">
															<span class="step">1</span>
															<span class="title">Client &amp; Title</span>
														</li>

														<li data-target="#step2">
															<span class="step">2</span>
															<span class="title">Banner Image File</span>
														</li>

														<li data-target="#step3">
															<span class="step">3</span>
															<span class="title">Video/Image File</span>
														</li>

														<li data-target="#step4">
															<span class="step">4</span>
															<span class="title">Campaigns</span>
														</li>
													</ul>
												</div>

												<hr />
												<div class="step-content row-fluid position-relative" id="step-container">
													<form class="form-horizontal" id="banner_form" 
                                                    action="db/process_banner.php<?php 
													if (isset($_GET['addedit']) && isset($_GET['sequence_id'])) 
													    {echo '?addedit=edit&sequence_id='.$_GET['sequence_id'];} ?>" 
                                                    method="post" enctype="multipart/form-data">
														<div class="step-pane active" id="step1">



															<div class="control-group">		
																<label class="control-label" for="select_advertiser">Advertiser</label>
																<div class="controls">
																	<select class="chosen-select-advertiser" 
                                                                    data-placeholder="(Select Advertiser::Campaign)" 
                                                                    name="campaign_id" id="select_advertiser">
																		<option>(Select Advertiser::Campaign)</option>
																	</select>
																</div>
															</div>
															<div class="control-group">
																<label class="control-label" for="name">Banner Name</label>
																<div class="controls">
																	<input type="text" id="name" name="name" placeholder="Untitled" 
                                                                    value="<?php echo $edit->bannername;?>" />
                                                                    <input type="hidden" id="type" name="type" 
                                                                    value="com.instillo.layout.model.BannerSequence" />
																</div>
															</div>

														</div>
														<div class="step-pane" id="step2">
															<div class="control-group">
															<label class="control-label" for="title">Title</label>
																<div class="controls">
        															<input type="text" id="title" name="title" 
                                                                    value="<?php echo $edit->title;?>" />
                                                                </div>
                                                            </div>
															<div class="control-group">
															<label class="control-label" for="subtitle">Sub-Title</label>
																<div class="controls">
        															<input type="text" id="subtitle" name="subtitle" 
                                                                    value="<?php echo $edit->subtitle;?>" />
                                                                </div>
                                                            </div>
															<div class="control-group">
															<label class="control-label" for="buttonname">Button Name</label>
																<div class="controls">
        															<input type="text" id="buttonname" name="buttonname" 
                                                                    value="<?php echo $edit->buttonname;?>" />
                                                                </div>
                                                            </div>

															<input name="userfile1[]" id="id-input-file-1" type="file" />
                                                            <br />

														</div>
														
														
														<!-- step 3 -->
                                                        
                                                        
														<div class="step-pane" id="step3">
															<div class="control-group">
																<div class="row-fluid">
																	<label>
																		<input id="radio_1" name="radiopopup" 
                                                                        type="radio" class="ace" value="radio_1" <?php echo $edit->radio1popup;?> />
																		<span class="lbl"> Image</span>
																		<input id="radio_2" name="radiopopup"  
                                                                        type="radio" class="ace" value="radio_2" <?php echo $edit->radio2popup;?> />
																		<span class="lbl"> Video</span>
																		<input id="radio_3" name="radiopopup" 
                                                                        type="radio" class="ace" value="radio_3" <?php echo $edit->radio3popup;?> />
    																	<span class="lbl"> Website</span>
																	</label>
                                                                    <hr/>

                                                                    <!-- Image input -->
                                                                    <div id="clabel1" name="clabel1">
                                                                        <label class="control-label" for="popupname">Popup Name</label>
                                                                    </div>
																    <div id="popupdiv" name="popupdiv" class="controls">
        															    <input type="text" id="popupname" name="popupname" 
                                                                        value="<?php echo $edit->popupname;?>" />
                                                                    </div>

                                                                    <!-- Video input -->
                                                                    <div class="control-group">
                                                                        <div id="clabel2" name="clabel2" style="display: none;">
                                                                            <label class="control-label" for="select_video">Select Video</label>
                                                                        </div>		
																        <div id="videodiv" name="videodiv" class="controls" style="display: none;">
																	        <select class="chosen-select-video" data-placeholder="(Select Video)" 
                                                                            name="video_id" id="select_video">
																		        <option>(Select Video)</option>
																	        </select>
																        </div>
															        </div>

                                                                    <!-- Website input -->
                                                                    <div id="clabel3" name="clabel3" style="display: none;">
                                                                        <label class="control-label" for="websitename">Website URL</label>
                                                                    </div>
																    <div id="websitediv" name="websitediv" class="controls" style="display: none;">
        															    <input type="text" class="span12" id="websitename" name="websitename" 
                                                                        placeholder="http://www..." value="<?php echo $edit->websitename;?>" />
                                                                    </div>
																</div>
                                                            </div>
                                                            <div id="fileinput">
    															<input name="userfile2[]" id="id-input-file-2" type="file" /><br />
                                                            </div>
														</div>

														<!-- step 4 -->	
														<div class="step-pane" id="step4">
															<div class="control-group">
															<label class="control-label" for="introtext">Text on Introduction</label>
																<div class="controls">
        															<input type="text" id="introtext" name="introtext"
                                                                    value="<?php echo $edit->introtext;?>" />
                                                                </div>
                                                            </div>
															<div class="control-group">
															<label class="control-label" for="exittext">Text on Exit</label>
																<div class="controls">
        															<input type="text" id="exittext" name="exittext" 
                                                                    value="<?php echo $edit->exittext;?>" />
                                                                </div>
                                                            </div>
															<div class="control-group">		
																<label class="control-label" for="switchfield1">Email Campaign</label>
																<div class="controls">
																	<label>
																		<input id="switchfield1" name="email" class="ace ace-switch ace-switch-6"
                                                                        type="checkbox" <?php echo $edit->switchfield1;?> />
																		<span class="lbl"></span>
																	</label>
																</div>
															</div>
															<div class="widget-box" style="display:none" id="email_campaign_detail">


																<div class="widget-body">
																	<div class="widget-main">
																		<div class="row-fluid">
																			<label for="form-field-8">Subject of email</label>

																			<input type="text" class="span12" id="form-field-8" 
                                                                            name="emailsubject" placeholder="an interesting subject.."
                                                                            value="<?php echo $edit->emailsubject;?>"></input>
																		</div>

																		<hr />
																		<div class="row-fluid">
																			<label for="form-field-9">Email Content</label>

																			<textarea class="span12 limited" id="form-field-9" 
                                                                            name="emailcontent" maxlength="50"
                                                                            ><?php echo $edit->emailcontent;?></textarea>
																		</div>


																	</div>
																</div>
															</div>

															<div class="control-group">		
																<label class="control-label" for="switchfield2">SMS Campaign</label>
																<div class="controls">
																	<label>
																		<input id="switchfield2" name="sms" class="ace ace-switch ace-switch-3" 
                                                                        type="checkbox" <?php echo $edit->switchfield2;?> />
																		<span class="lbl"></span>
																	</label>
																</div>
															</div>
															<div class="widget-box" style="display:none" id="sms_campaign_detail">


																<div class="widget-body">
																	<div class="widget-main">

																		<div class="row-fluid">
																			<label for="form-field-9">SMS Content</label>

																			<textarea class="span12 limited" id="form-field-9" 
                                                                            name="smscontent" maxlength="50"
																			><?php echo $edit->smscontent;?></textarea>
																		</div>


																	</div>
																</div>
															</div>

															<div class="control-group">		
																<label class="control-label" for="switchfield3">QR Code Campaign</label>
																<div class="controls">
																	<label>
																		<input id="switchfield3" name="qrcode" class="ace ace-switch ace-switch-3"
                                                                        type="checkbox" <?php echo $edit->switchfield3;?> />
																		<span class="lbl"></span>
																	</label>
																</div>
															</div>
															<div class="widget-box" style="display:none" id="qr_campaign_detail">


																<div class="widget-body">
																	<div class="widget-main">

																		<div class="row-fluid">
																			<div class="controls">
																				<label>
																					<input name="radioqrcode" type="radio" class="ace" 
                                                                                    value="qrcheck1" <?php echo $edit->qrcheck1; ?> />
																					<span class="lbl"> Email</span>
																					<input name="radioqrcode" type="radio" class="ace" 
                                                                                    value="qrcheck2" <?php echo $edit->qrcheck2; ?> />
																					<span class="lbl"> Phone</span>
																					<input name="radioqrcode" type="radio" class="ace"
                                                                                    value="qrcheck3" <?php echo $edit->qrcheck3; ?> />
																					<span class="lbl"> Text</span>
																					<input name="radioqrcode" type="radio" class="ace"
                                                                                    value="qrcheck4" <?php echo $edit->qrcheck4; ?> />
																					<span class="lbl"> Website</span>
																				</label>

																			</div>
																			<input type="text" class="span12 limited" id="form-field-9" 
                                                                            name="qrcontent" maxlength="50"
                                                                            value="<?php echo $edit->qrcontent;?>" />
																		</div>


																	</div>
																</div>
															</div>
														</div>
													</form>
												</div>

												<hr />
												<div class="row-fluid wizard-actions">
													<button class="btn btn-prev">
														<i class="icon-arrow-left"></i>
														Prev
													</button>

													<button class="btn btn-success btn-next" data-last="Finish ">
														Next
														<i class="icon-arrow-right icon-on-right"></i>
													</button>
												</div>
											</div>
										</div><!--/widget-main-->
									</div><!--/widget-body-->
								</div>
							</div>
						</div>



						<div class="ace-settings-container" id="ace-settings-container">
							<div class="btn btn-app btn-mini btn-warning ace-settings-btn" id="ace-settings-btn">
								<i class="icon-cog bigger-150"></i>
							</div>

							<div class="ace-settings-box" id="ace-settings-box">
								<div>
									<div class="pull-left">
										<select id="skin-colorpicker" class="hide">
											<option data-skin="default" value="#438EB9">#438EB9</option>
											<option data-skin="skin-1" value="#222A2D">#222A2D</option>
											<option data-skin="skin-2" value="#C6487E">#C6487E</option>
											<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
										</select>
									</div>
									<span>&nbsp; Choose Skin</span>
								</div>

								<div>
									<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar" />
									<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
								</div>

								<div>
									<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar" />
									<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
								</div>

								<div>
									<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-breadcrumbs" />
									<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
								</div>

								<div>
									<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" />
									<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
								</div>
							</div>
						</div><!--/#ace-settings-container-->





						<!--PAGE CONTENT ENDS--> 
					</div>
					<!--/.span--> 
				</div>
				<!--/.row-fluid--> 
			</div>
		</div>
		<!--/.row-fluid--> 
	</div>
	<!--/.page-content-->

	<?php include("kl_inc_ace_settings.php"); ?>
</div>
<!--/.main-content-->
</div>
<!--/.main-container--> 

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse"> <i class="icon-double-angle-up icon-only bigger-110"></i> </a> 

<!--basic scripts--> 

<!--[if !IE]>--> 

<script type="text/javascript">
window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script> 

<!--<![endif]--> 

<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]--> 

<script type="text/javascript">
if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script> 
<script src="assets/js/bootstrap.min.js"></script> 

<!--page specific plugin scripts--> 

<script src="assets/js/fuelux/fuelux.wizard.min.js"></script> 
<script src="assets/js/jquery.validate.min.js"></script> 
<script src="assets/js/additional-methods.min.js"></script> 
<script src="assets/js/bootbox.min.js"></script> 
<script src="assets/js/jquery.maskedinput.min.js"></script> 
<script src="assets/js/select2.min.js"></script> 



<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="assets/js/chosen.jquery.min.js"></script>
<script src="assets/js/fuelux/fuelux.spinner.min.js"></script>
<script src="assets/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="assets/js/date-time/bootstrap-timepicker.min.js"></script>
<script src="assets/js/date-time/moment.min.js"></script>
<script src="assets/js/date-time/daterangepicker.min.js"></script>
<script src="assets/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/js/jquery.knob.min.js"></script>
<script src="assets/js/jquery.autosize-min.js"></script>
<script src="assets/js/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="assets/js/jquery.maskedinput.min.js"></script>
<script src="assets/js/bootstrap-tag.min.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>



<!--ace scripts--> 

<script src="assets/js/ace-elements.min.js"></script> 
<script src="assets/js/ace.min.js"></script> 

<!--inline scripts related to this page-->
<script type="text/javascript">
$('input:radio[id="radio_1"]').ready(function(){
    if ($('input:radio[id="radio_1"]:checked').length > 0) {
        $('#popupdiv').show();
        $('#fileinput').show();
        $('#clabel1').show();
        $('#videodiv').hide();
        $('#clabel2').hide();
        $('#websitediv').hide();
        $('#clabel3').hide();
    }
})
</script>
<script type="text/javascript">
$('input:radio[id="radio_2"]').ready(function(){
    if ($('input:radio[id="radio_2"]:checked').length > 0) {
        $('#popupdiv').hide();
        $('#fileinput').hide();
        $('#clabel1').hide();
        $('#videodiv').show();
        $('#clabel2').show();
        $('#websitediv').hide();
        $('#clabel3').hide();
    }
})
</script>
<script type="text/javascript">
$('input:radio[id="radio_3"]').ready(function(){
    if ($('input:radio[id="radio_3"]:checked').length > 0) {
        $('#popupdiv').hide();
        $('#fileinput').hide();
        $('#clabel1').hide();
        $('#videodiv').hide();
        $('#clabel2').hide();
        $('#websitediv').show();
        $('#clabel3').show();
    }
})
</script>
<script type="text/javascript">
$('input:radio[id="radio_1"]').change(function(){
        $('#popupdiv').show();
        $('#fileinput').show();
        $('#clabel1').show();
        $('#videodiv').hide();
        $('#clabel2').hide();
        $('#websitediv').hide();
        $('#clabel3').hide();
})
</script>
<script type="text/javascript">
$('input:radio[id="radio_2"]').change(function(){
    $('#popupdiv').hide();
    $('#fileinput').hide();
    $('#clabel1').hide();
    $('#videodiv').show();
    $('#clabel2').show();
    $('#websitediv').hide();
    $('#clabel3').hide();
})
</script>
<script type="text/javascript">
$('input:radio[id="radio_3"]').change(function(){
    $('#popupdiv').hide();
    $('#fileinput').hide();
    $('#clabel1').hide();
    $('#videodiv').hide();
    $('#clabel2').hide();
    $('#websitediv').show();
    $('#clabel3').show();
})
</script>
<script type="text/javascript">
jQuery(function($) {

	$('[data-rel=tooltip]').tooltip();


	var $validation = false;
	$('#fuelux-wizard').ace_wizard().on('change' , function(e, info){
		if(info.step == 1 && $validation) {
			if(!$('#validation-form').valid()) return false;
		}
	}).on('finished', function(e) {

		console.log("form:"+$("#banner_form").serialize());
		$("#banner_form").submit();

		
		

	}).on('stepclick', function(e){
					//return false;//prevent clicking on steps
	});

	$('#banner_form').ajaxForm({
	    beforeSend: function() {
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	    },
	    success: function() {
	    },
		complete: function(xhr) {
			console.log(xhr.responseText);
				bootbox.dialog("Thank you! "+xhr.responseText, [{
				"label" : "OK",
				"class" : "btn-small btn-primary",
				"callback": function() {window.history.back(-1);}
			}]
		);
		}
	}); 


	
				//documentation : http://docs.jquery.com/Plugins/Validation/validate

				$('#modal-wizard .modal-header').ace_wizard();
				$('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');

				var advertiser_campaignmap={};
                $.getJSON( "db/process_campaign.php?action=advertiser_campaign_list", function( data ) {
					unassignedData = data;
					$.each( data, function( obj ) {
						$('#select_advertiser').append($('<option>', { 
							value: data[obj].campaign_id,
                        	text : data[obj].advertiser_name+"::"+data[obj].campaign_name
						}));
						advertiser_campaignmap[data[obj].campaign_id]=data[obj].advertiser_id;
				    });
					$(".chosen-select-advertiser").val(<?php echo $_GET['campaign_id']; ?>)
					$(".chosen-select-advertiser").chosen().change(console.log("changed!!!"));
					resetVideoList(<?php echo $_GET['advertiser_id']; ?>);
		                //chosen plugin inside a modal will have a zero width because the select element is originally hidden
		                //and its width cannot be determined.
		                //so we set the width after modal is show

		            $('.chosen-container').each(function(){
		               	$('a:first-child').css('width' , '210px');
		               	$('.chosen-drop').css('width' , '220px');
		               	$('.chosen-search input').css('width' , '210px');
		            }); 
		        });

				$(".chosen-select-advertiser").on('change',function(e){
						console.log("changed!!"+advertiser_campaignmap[$(".chosen-select-advertiser").val()]);
						resetVideoList(advertiser_campaignmap[$(".chosen-select-advertiser").val()]);
				});



				function resetVideoList(advertiser_id){$.getJSON( "db/process_video.php?action=list&advertiser_id="+advertiser_id, function( data ) {
					unassignedData = data;
					$('#select_video').find('option').remove();
					$.each( data, function( obj ) {
						console.log(data[obj].mediaID+data[obj].name);
						$('#select_video').append($('<option>', { 
							value: data[obj].mediaID,
							text : data[obj].name
						}));

				    });

					$(".chosen-select-video").chosen().val(<?php echo $edit->videoid; ?>); 
					$(".chosen-select-video").trigger("chosen:updated");
		                //chosen plugin inside a modal will have a zero width because the select element is originally hidden
		                //and its width cannot be determined.
		                //so we set the width after modal is show
					$('.chosen-container').each(function(){
		               	$('a:first-child').css('width' , '210px');
		               	$('.chosen-drop').css('width' , '220px');
		               	$('.chosen-search input').css('width' , '210px');
		            });
		            
		        });}




				$('#id-input-file-1 , #id-input-file-2').ace_file_input({
					no_file:'Upload new file ...',
					btn_choose:'Choose',
					btn_change:'Change',
					droppable:true,
					onchange:null,
					thumbnail:false //| true | large
					//whitelist:'gif|png|jpg|jpeg'
					//blacklist:'exe|php'
					//onchange:''
					//
				});

				$('#id-input-file-3').ace_file_input({
					style:'well',
					btn_choose:'Drop files here or click to choose file',
					btn_change:null,
					no_icon:'icon-cloud-upload',
					droppable:true,
					thumbnail:'small'
					//,icon_remove:null//set null, to hide remove/reset button
					/**,before_change:function(files, dropped) {
						//Check an example below
						//or examples/file-upload.html
						return true;
					}*/
					/**,before_remove : function() {
						return true;
					}*/
					,
					preview_error : function(filename, error_code) {
						//name of the file that failed
						//error_code values
						//1 = 'FILE_LOAD_FAILED',
						//2 = 'IMAGE_LOAD_FAILED',
						//3 = 'THUMBNAIL_FAILED'
						//alert(error_code);
					}

				}).on('change', function(){
					//console.log($(this).data('ace_input_files'));
					//console.log($(this).data('ace_input_method'));
				});

				//dynamically change allowed formats by changing before_change callback function
				$('#id-file-format').removeAttr('checked').on('change', function() {
					var before_change
					var btn_choose
					var no_icon
					if(this.checked) {
						btn_choose = "Drop images here or click to choose";
						no_icon = "icon-picture";
						before_change = function(files, dropped) {
							var allowed_files = [];
							for(var i = 0 ; i < files.length; i++) {
								var file = files[i];
								if(typeof file === "string") {
									//IE8 and browsers that don't support File Object
									if(! (/\.(jpe?g|png|gif|bmp)$/i).test(file) ) return false;
								}
								else {
									var type = $.trim(file.type);
									if( ( type.length > 0 && ! (/^image\/(jpe?g|png|gif|bmp)$/i).test(type) )
											|| ( type.length == 0 && ! (/\.(jpe?g|png|gif|bmp)$/i).test(file.name) )//for android's default browser which gives an empty string for file.type
										) continue;//not an image so don't keep this file
								}

							allowed_files.push(file);
						}
						if(allowed_files.length == 0) return false;

						return allowed_files;
					}
				}
				else {
					btn_choose = "Drop files here or click to choose";
					no_icon = "icon-cloud-upload";
					before_change = function(files, dropped) {
						return files;
					}
				}
				var file_input = $('#id-input-file-3');
				file_input.ace_file_input('update_settings', {'before_change':before_change, 'btn_choose': btn_choose, 'no_icon':no_icon})
				file_input.ace_file_input('reset_input');
			});

    $( "#switchfield1" ).ready(function() {
        if ($('input:checkbox[id="switchfield1"]:checked').length > 0) {
	        $("#email_campaign_detail").show();
		}
    });

    $( "#switchfield2" ).ready(function() {
        if ($('input:checkbox[id="switchfield2"]:checked').length > 0) {
	        $("#sms_campaign_detail").show();
		}
    });

    $( "#switchfield3" ).ready(function() {
        if ($('input:checkbox[id="switchfield3"]:checked').length > 0) {
    	    $("#qr_campaign_detail").show();
		}
    });



    $( "#switchfield1" ).change(function() {
	    $("#email_campaign_detail").toggle();
    });

    $( "#switchfield2" ).change(function() {
	    $("#sms_campaign_detail").toggle();
    });

    $( "#switchfield3" ).change(function() {
	    $("#qr_campaign_detail").toggle();
    });

})
</script>

</body>
</html>
