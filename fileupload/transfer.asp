<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<style type="text/css">
#calendar-container {
	float: left;
	margin-right: 1em;
}
#acuweb-search-form {
	margin: 1em;
	float: left;
	text-align: center;
}
</style>
<link rel="stylesheet" type="text/css" href="http://acumen.webapplicationsuk.com/system/calendar/tiger/theme.css" />
<script type="text/javascript" src="http://acumen.webapplicationsuk.com/system/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="http://acumen.webapplicationsuk.com/system/calendar/calendar-en.js"></script>
<script type="text/javascript" src="http://acumen.webapplicationsuk.com/system/calendar/calendar-setup_stripped.js"></script>
<script type="text/javascript" src="http://acumen.webapplicationsuk.com/system/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="http://acumen.webapplicationsuk.com/system/shadowbox/shadowbox-2.0.js"></script>

<script type="text/javascript">
	var minDate, maxDate, minDateYear, maxDateYear, cutoff = new Date();
	
	cutoff = new Date(cutoff.getFullYear(), cutoff.getMonth(), cutoff.getDate()+0);
	
	
		minDate = new Date(2011,0,01);
		minDateYear = minDate.getFullYear();
	
		maxDate = new Date(2013,11,31);
		maxDateYear = maxDate.getFullYear();
	
	
	Shadowbox.loadSkin('classic', 'http://acumen.webapplicationsuk.com/system/shadowbox');

	$(document).ready(function() {

		$(".acuweb-submit").attr("disabled", false);
		
		Shadowbox.init({animSequence: "sync"});
		
		if ($("#calendar-date").val() != "") {
			var data = $("#calendar-date").val().split("-",3)
			caldate = new Date(data[0],data[1]-1,data[2]);
		} else {
			while (window.dateCheck && dateCheck(cutoff))
				cutoff.setDate(cutoff.getDate()+1);
			caldate = cutoff;
			$("#calendar-date").val( cutoff.getFullYear() + "-" + (cutoff.getMonth()+1) + "-" + cutoff.getDate() );
		}
		
		if ($("#calendar-container").length == 1) {
			Calendar.setup({
				flat : "calendar-container",
				onSelect : dateChanged,
				ifFormat : "%Y-%m-%d",
				dateStatusFunc : dateStatus,
				date : caldate,
				range : [minDateYear, maxDateYear]
			});
		}
	});

	function dateChanged(calendar, date) {
		$("#calendar-date").val( dateStatus(calendar.date) ? "" : date);
	};

	function validate() {
		if ($("#calendar-date").val() == "") {
			alert("Please select a date from the calendar");
			return false;
		} else
			$(".acuweb-submit").attr("disabled", true);
	};

	function dateStatus(date) {
		return date<cutoff || date<minDate || date>maxDate ? true : window.dateCheck && dateCheck(date) ? true : false;
	};
	
</script>

</head>
<body>
<div id="acuweb-search-panel" class="acuweb-panel">
<h4 id="acuweb-search-header" class="acuweb-header"><table cellspacing=0 cellpadding=0 style="margin:0px" width="100%"><tr><td>Step 1: Search for Your Holiday</td></tr></table><hr></h4>
<span id="acuweb-search-text" class="acuweb-text">Please select your requested arrival date from the calendar below, review the number of people and nights and press 'Search'.</span>
<form id="acuweb-search-form" class="acuweb-form" name="acuwebsearchform" action="http://acumen.webapplicationsuk.com/kellertravel/results.asp?sessionid={EBE86791-9EAB-46D1-99FB-789D5396E8A2}" onsubmit="return validate();" method="post">
	<input id="calendar-date" type="hidden" name="Date">
	<div id="calendar-container"></div>
	<br/>
	<table border=0>
		<tr>
			<td>Choose Campsites :</td>
			<td>
				<select class="accuweb-select" id="acuweb-holiday-select" name="Area">
					<option selected="selected" value="">Any Campsites</option>
					<option value="BCO01">Dol De Bretagne</option>
					<option value="BGM01">La Grande Metairie</option>
					<option value="BPG01">La Pointe St Gilles</option>
					<option value="CAMON">Castell Montgri</option>
					<option value="CLAPL">Clarys Plage</option>
					<option value="CYPSL">Cypsela</option>
					<option value="IAL01">Parc Albatros</option>
					<option value="LABEN">Sylvamar</option>
					<option value="LHP01">Club L'Hippocampe</option>
					<option value="LSR01">La Sirene</option>
					<option value="PACHA">La Pachacaid</option>
					<option value="PCP01">La Pinede</option>
					<option value="PCS01">Sequoia Parc</option>
					<option value="PRU01">Le Ruisseau</option>
					<option value="VDU01">La Dune Des Sables</option>
					<option value="VGE01">Les Genets</option>
					<option value="VLO01">La Loubine</option>
					<option value="VLT01">Le Littoral</option>
					<option value="ING01">Norcenni Girasole</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Number of Nights :</td>
			<td>
				<select id="accuweb-nights-select" class="accuweb-select" name="Nights">
				<option selected>7</option><option>8</option><option>9</option><option>10</option><option>11</option>
				<option>12</option><option>13</option><option>14</option><option>15</option><option>16</option>
				<option>17</option><option>18</option><option>19</option><option>20</option><option>21</option>
				<option>22</option><option>23</option><option>24</option><option>25</option><option>26</option>
				<option>27</option><option>28</option></select>
			</td>
		</tr>
		<tr>
			<td>Number of People :</td>
			<td>
				<select id="accuweb-passengers-select" class="accuweb-select" name="Passengers">
				<option>1</option><option selected>2</option><option>3</option><option>4</option><option>5</option>
				<option>6</option><option>7</option><option>8</option><option>9</option><option>10</option></select>
			</td>
		</tr>
			<td>
				<input id="acuweb-search-submit" class="acuweb-submit" type="submit" name="next" value="Perform Search" />
			</td>
		</tr>
	</table>
</form>
<br clear="all" />
<a href="http://acumen.webapplicationsuk.com/kellertravel/propertylist.asp?sessionid={EBE86791-9EAB-46D1-99FB-789D5396E8A2}">Click here to view a list of our accommodation</a>
<br /><font size=-2>
Powered by <a target="_new" href="http://www.WebApplicationsUk.com">Web Applications UK</a>
</font>
</div>
</body>
</html>
