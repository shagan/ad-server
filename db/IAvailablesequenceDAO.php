<?php

class IAvailablesequenceDAO {

	public function __construct($connection) {
		$this->connection = $connection;
	}

	public function getSequencesNotinSegment($segment_id){
		$sql = "SELECT isequence.id AS sequence_id, iadvertiser.name AS advertiser_name, icampaign.name AS campaign_name,isequence.name AS sequence_name FROM iadvertiser, icampaign, isequence, ilkadvertisercampaign, ilkcampaignsequence WHERE (iadvertiser.id = ilkadvertisercampaign.advertiser_id AND icampaign.id = ilkadvertisercampaign.campaign_id AND icampaign.id = ilkcampaignsequence.campaign_id AND isequence.id = ilkcampaignsequence.sequence_id AND isequence.type =  (select datatype from iregion where id in ( select region_id from ilkregionsegment where segment_id=?)));";
		$ps = new PreparedStatement($sql);
		$ps->setInt($segment_id);
		$rows = array();
		$rs = $this->connection->executeQuery($ps);
		while ($arr = $this->connection->fetchArray($rs)) {
			$rows[] = $arr;
		}
		$this->connection->freeResult($rs);
		return $rows;
	}

	public function getSequencesinSegment($segment_id){
		$sql = "SELECT isequence.id AS sequence_id, isequence.name AS sequence_name FROM isequence, ilksegmentsequenceorder WHERE isequence.id in ( select sequence_id from irun where segment_id = ?) and isequence.id=ilksegmentsequenceorder.sequence_id and ilksegmentsequenceorder.segment_id=? order by ilksegmentsequenceorder.displayorder";
		$ps = new PreparedStatement($sql);
		$ps->setInt($segment_id);
		$ps->setInt($segment_id);
		$rows = array();

		error_log("before:".time());
		$rs = $this->connection->executeQuery($ps);
		while ($arr = $this->connection->fetchArray($rs)) {
			$rows[] = $arr;
		}
		error_log("after:".time());
		$this->connection->freeResult($rs);
		return $rows;
	}


	public function deleteSequenceForSegment($segment_id, $sequence_id){
		$sql = "DELETE FROM irun where segment_id=? AND sequence_id=?";
		error_log($sql.$segment_id.":".$sequence_id);
		$ps = new PreparedStatement($sql);
		$ps->setInt($segment_id);
		$ps->setInt($sequence_id);
		$this->connection->executeQuery($ps);
		
	}
}
