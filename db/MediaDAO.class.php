<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/MediaDAOAbstract.class.php';
class MediaDAO extends MediaDAOAbstract {

	public function findVideoByAdvertiser_Id($id){
		$sql = "SELECT * FROM media where mediaID in ( select media_id from `ilkadvertisermedia` WHERE advertiser_id=?) AND type='video'";
		$ps = new PreparedStatement($sql);
		$ps->setInt($id);
		return parent::findWithPreparedStatement($ps);
	}
}
