<?php
// DDL.class.php
// Copyright (c) 2011 Ronald B. Cemer
// All rights reserved.
// This software is released under the BSD license.
// Please see the accompanying LICENSE.txt for details.
if (!class_exists('PreparedStatement', false)) include dirname(__FILE__).'/PreparedStatement.class.php';
if (!class_exists('Spyc', false)) include dirname(__FILE__).'/spyc/spyc.php';

// The DDL class represents an entire Data Definition Language (DDL) schema.
class DDL {
	// Array of DDLTable, DDLIndex, DDLInsert instances.
	public $topLevelEntities;

	public function DDL($topLevelEntities = array()) {
		$this->topLevelEntities = $topLevelEntities;
	}

	public function getAllTableNames() {
		$allTableNames = array();
		foreach ($this->topLevelEntities as $tle) {
			if ((get_class($tle) == 'DDLTable') &&
				(!in_array($tle->tableName, $allTableNames))) {
				$allTableNames[] = $tle->tableName;
			}
		}
		return $allTableNames;
	}

	// Given another DDL instance, find table names which exist in both this DDL instance and
	// the other DDL instance.
	// Returns an array of common table names (strings).
	public function getCommonTableNames($otherDDL) {
		$commonTableNames = array();
		foreach ($this->topLevelEntities as $tle) {
			if (get_class($tle) == 'DDLTable') {
				foreach ($otherDDL->topLevelEntities as $otle) {
					if ((get_class($otle) == 'DDLTable') && ($otle->tableName == $tle->tableName)) {
						if (!in_array($tle->tableName, $commonTableNames)) {
							$commonTableNames[] = $tle->tableName;
						}
						break;
					}
				}
			}
		}
		return $commonTableNames;
	}

	// Returns the index, or false if not found.
	public function getTableIdxInTopLevelEntities($tableName) {
		for ($i = 0, $n = count($this->topLevelEntities); $i < $n; $i++) {
			if (get_class($this->topLevelEntities[$i]) == 'DDLTable') {
				if ($this->topLevelEntities[$i]->tableName == $tableName) {
					unset($tle);
					return $i;
				}
			}
		}
		unset($tle);
		return false;
	}


	// Given a table name, find and return the DDLTable instance, an array of DDLIndex instances,
	// and an array of DDLForeignKey instances for that table.
	// Returns an object with the following attributes:
	//     tbl: The DDLTable instance, or false if the table was not found in this DDL instance.
	//     idxs: An associative array of index names to DDLIndex instances for the specified table.
	//     fks: An associative array of foreign key names to DDLForeignKey instances for the
	//     specified table.
	public function getTableIndexesAndForeignKeys($tableName) {
		$tbl = false;
		$idxs = array();
		$fks = array();
		foreach ($this->topLevelEntities as $tle) {
			if (($tbl === false) &&
				(get_class($tle) == 'DDLTable') &&
				($tle->tableName == $tableName)) {
				$tbl = $tle;
				continue;
			}
			if ((get_class($tle) == 'DDLIndex') && ($tle->tableName == $tableName)) {
				$idxs[$tle->indexName] = $tle;
			}
			if ((get_class($tle) == 'DDLForeignKey') && ($tle->localTableName == $tableName)) {
				$fks[$tle->foreignKeyName] = $tle;
				continue;
			}
		}
		$result = new stdClass();
		$result->tbl = $tbl;
		$result->idxs = $idxs;
		$result->fks = $fks;
		return $result;
	}
}

// The DDLTable class represents a complete table definition including columns and optional primary
// key, but NOT including any additional indexes on the table (which are represented by individual
// DDLIndex instances).
class DDLTable {
	// The table name (string).
	public $tableName;
	// Array of DDLTableColumn instances.
	public $columns;
	// A single DDLPrimaryKey instance, or false if none.
	public $primaryKey;

	public function DDLTable($tableName, $columns = array(), $primaryKey = false) {
		$this->tableName = $tableName;
		$this->columns = $columns;
		$this->primaryKey = $primaryKey;
	}

	public function getColumnIdx($name) {
		for ($i = 0, $n = count($this->columns); $i < $n; $i++) {
			if ($this->columns[$i]->name == $name) {
				return $i;
			}
		}
		return -1;
	}
}

class DDLForeignKey {
	// The foreign key name (string).
	public $foreignKeyName;
	// The local (referring) table name (string).
	public $localTableName;
	// The foreign (referenced) table name (string).
	public $foreignTableName;
	// Array of DDLForeignKeyColumn instances.
	public $columns;

	public function DDLForeignKey($foreignKeyName, $localTableName, $foreignTableName, $columns = array()) {
		$this->foreignKeyName= $foreignKeyName;
		$this->localTableName = $localTableName;
		$this->foreignTableName = $foreignTableName;
		$this->columns = $columns;
	}

	// Returns the index in $columns for the column whose local name equals $localColumnName,
	// or -1 if not found.
	public function getLocalColumnIdx($localColumnName) {
		for ($i = 0, $n = count($this->columns); $i < $n; $i++) {
			if ($this->columns[$i]->localName == $localColumnName) {
				return $i;
			}
		}
		return -1;
	}

	// Returns the index in $columns for the column whose foreign name equals $foreignColumnName,
	// or -1 if not found.
	public function getForeignColumnIdx($foreignColumnName) {
		for ($i = 0, $n = count($this->columns); $i < $n; $i++) {
			if ($this->columns[$i]->foreignName == $foreignColumnName) {
				return $i;
			}
		}
		return -1;
	}
}

// The DDLIndex class represents a single index (but not a primary key) on a table.
class DDLIndex {
	// The index name (string).
	public $indexName;
	// The table name (string).
	public $tableName;
	// Whether this is a unique index (boolean).
	public $unique;
	// Array of DDLKeyColumn instances.
	public $columns;

	public function DDLIndex($indexName, $tableName, $unique, $columns = array()) {
		$this->indexName = $indexName;
		$this->tableName = $tableName;
		$this->unique = $unique;
		$this->columns = $columns;
	}

	// Returns the index in $columns for the column whose name equals $columnName,
	// or -1 if not found.
	public function getColumnIdx($columnName) {
		for ($i = 0, $n = count($this->columns); $i < $n; $i++) {
			if ($this->columns[$i]->name == $columnName) {
				return $i;
			}
		}
		return -1;
	}
}

// The DDLInsert class represents a single row to be inserted into a table.
class DDLInsert {
	// The table name (string).
	public $tableName;
	// Array of DDLInsertColumn instances.
	public $columns;
	// Optional array of column names referencing columns in this insert which
	// uniquely identify this insert, so that it can be omitted or changed into
	// an update if a row already exists in the table with these column values.
	public $keyColumnNames;
	// true to update the row if it already exists, or false to simply omit the insert.
	// Requires that keyColumnNames be specified.
	public $updateIfExists;

	public function DDLInsert($tableName, $columns = array(), $keyColumnNames = array(), $updateIfExists = false) {
		$this->tableName = $tableName;
		$this->columns = $columns;
		$this->keyColumnNames = $keyColumnNames;
		$this->updateIfExists = $updateIfExists;
	}

	// Returns the index in $columns for the column whose name equals $columnName,
	// or -1 if not found.
	public function getColumnIdx($columnName) {
		for ($i = 0, $n = count($this->columns); $i < $n; $i++) {
			if ($this->columns[$i]->name == $columnName) {
				return $i;
			}
		}
		return -1;
	}
}

// The DDLTableColumn class represents a single column within a DDLTable instance.
// DDLTableColumn instances are members of the $columns array member variable of the
// DDLTable class.
class DDLTableColumn {
	// The column name (string).
	public $name;
	// The column type (string) -- see ddl.dtd for allowed types.
	public $type;
	// The column size (int), or 0 if not applicable.
	public $size;
	// The column scale (int), or 0 if not applicable.
	public $scale;
	// Whether to allow null values (boolean).
	public $allowNull;
	// Default value (string), or false if none.
	public $defaultValue;
	// System variable name for default value (string), or false if none.
	public $sysVarDefault;
	// Whether to auto-increment this column (boolean).
	public $autoIncrement;
	// Whether to use current connection's time zone; only applies to datetime type (boolean).
	public $useTimeZone;

	public function DDLTableColumn(
		$name,
		$type,
		$size,
		$scale,
		$allowNull,
		$defaultValue = false,
		$sysVarDefault = false,
		$autoIncrement = false,
		$useTimeZone = false) {

		$this->name = $name;
		$this->type = $type;
		$this->size = $size;
		$this->scale = $scale;
		$this->allowNull = $allowNull;
		$this->defaultValue = $defaultValue;
		$this->sysVarDefault = $sysVarDefault;
		$this->autoIncrement = $autoIncrement;
		$this->useTimeZone = $useTimeZone;
	}
}

// The DDLPrimaryKey class represents the primary key within a DDLTable instance.
// The $primaryKey member variable of the DDLTable class can be either a DDLPrimaryKey instance,
// or false if there is no primary key on the table.
class DDLPrimaryKey {
	// Array of DDLKeyColumn instances.
	public $columns;

	public function DDLPrimaryKey($columns = array()) {
		$this->columns = $columns;
	}

	// Returns the index in $columns for the column whose name equals $columnName,
	// or -1 if not found.
	public function getColumnIdx($columnName) {
		for ($i = 0, $n = count($this->columns); $i < $n; $i++) {
			if ($this->columns[$i]->name == $columnName) {
				return $i;
			}
		}
		return -1;
	}
}

// The DDLForeignKeyColumn represents a single column within a DDLForeignKey instance.
// DDLForeignKeyColumn instances are members of the $columns array member variable of the
// DDLForeignKey class.
class DDLForeignKeyColumn {
	// The name of the referring column in the local table.
	public $localName;
	// The name of the referenced column in the foreign table.
	public $foreignName;

	public function DDLForeignKeyColumn($localName, $foreignName) {
		$this->localName = $localName;
		$this->foreignName = $foreignName;
	}
}

// The DDLKeyColumn represents a single column within a DDLIndex or a DDLPrimaryKey instance.
// DDLKeyColumn instances are members of the $columns array member variable of the
// DDLIndex or DDLPrimaryKey class.
class DDLKeyColumn {
	// The name of the column.
	public $name;

	public function DDLKeyColumn($name) {
		$this->name = $name;
	}
}

// The DDLInsertColumn represents a single column and its value within a DDLInsert instance.
// DDLInsertColumn instances are members of the $columns array member variable of the
// DDLInsert class.
class DDLInsertColumn {
	// The name of the column.
	public $name;
	// The value (string), or false if none.
	public $value;
	// The system variable name for the value (string), or false if none.
	public $sysVarValue;
	// Whether to quote value; only applies when value is set and sysVarValue is false (boolean).
	public $quoted;

	public function DDLInsertColumn($name, $value, $sysVarValue = false, $quoted = false) {
		$this->name = $name;
		$this->value = $value;
		$this->sysVarValue = $sysVarValue;
		$this->quoted = $quoted;
	}
}

// The ConnectionDDLLoader class loads DDL from a database, and returns a DDL instance which
// represents the DDL of the qualifying tables.
class ConnectionDDLLoader {
	// Load a DDL instance from a Connection, using the given database dialect.
	// Note: This function uses the class of $connection to determine the SQL dialect.
	// Currently only MySQLConnection (mysql dialect) is supported.
	// All other connection types will throw an exception.
	// Parameters:
	// $d: The open database Connection instance.
	// $generateInserts: true to generate DDLInsert instances to populate the tables with the data
	// which is currently in the tables; false to exclude DDLInsert instances.
	// Optional.  Defaults to false.
	// $allowedTableNames: An array of table names which are allowed to be processed, or an empty
	// array to allow all tables.  Optional.  Defaults to an empty array.
	// Returns: A DDL instance.
	public function loadDDL(
		$db,
		$generateInserts = false,
		$allowedTableNames = array()) {

		if ($db instanceof MySQLConnection) {
			$dialect = 'mysql';
		} else if ($db instanceof PostgreSQLConnection) {
			$dialect = 'pgsql';
		} else {
			throw new Exception(sprintf("Unsupported Connection type: %s\n", get_class($db)));
		}
		$ddl = new DDL();

		if ($dialect == 'mysql') {
			// Pre-fetch all foreign keys for this database, because this query is slow.
			$__allMySQLFKRows = $db->fetchAllObjects(
				$db->executeQuery(new PreparedStatement(<<<EOF
select
  b.constraint_name as foreignKeyName,
  b.table_name as localTableName,
  b.referenced_table_name as foreignTableName,
  b.column_name as col_localName,
  b.referenced_column_name as col_foreignName
from information_schema.table_constraints a
inner join information_schema.key_column_usage b
  on a.constraint_name = b.constraint_name
    and b.table_schema = database()
where a.table_schema = database()
  and b.table_schema = database()
  and a.constraint_type = 'FOREIGN KEY'
order by b.table_name, b.constraint_name, ordinal_position
EOF
				)),
				true
			);
			$mysqlFKs = array();
			$__fk = false;
			foreach ($__allMySQLFKRows as $__fkrow) {
				if (($__fk === false) ||
					($__fkrow->foreignKeyName != $__fk->foreignKeyName) ||
					($__fkrow->localTableName != $__fk->localTableName) ||
					($__fkrow->foreignTableName != $__fk->foreignTableName)) {
					if ($__fk !== false) $mysqlFKs[] = $__fk;
					$__fk = new DDLForeignKey(
						$__fkrow->foreignKeyName,
						$__fkrow->localTableName,
						$__fkrow->foreignTableName
					);
				}
				$__fk->columns[] = new DDLForeignKeyColumn(
					$__fkrow->col_localName,
					$__fkrow->col_foreignName
				);
			}
			if ($__fk !== false) $mysqlFKs[] = $__fk;
			unset($__fk);
			unset($__allMySQLFKRows);
			unset($__fkrow);
		}

		// Get table names.
		$tableNames = array();
		switch ($dialect) {
		case 'mysql':
			$ps = new PreparedStatement('show tables');
			$tableRows = $db->fetchAllObjects($db->executeQuery($ps), true);
			if (!empty($tableRows)) {
				$colname = '';
				foreach (array_keys((array)($tableRows[0])) as $cn) {
					if (strncasecmp($cn, 'tables_in_', 10) == 0) {
						$colname = $cn;
						break;
					}
				}
				if ($colname == '') {
					throw new Exception
						("Cannot decipher result set of MySQL \"show tables\" command.");
				}
				foreach ($tableRows as $tableRow) {
					$tableNames[] = $tableRow->$colname;
				}
				unset($tableRow);
				unset($colname);
			}
			unset($tableRows);
			break;
		case 'pgsql':
			$ps = new PreparedStatement("select table_name from information_schema.tables where table_type = 'BASE TABLE' and table_schema not in ('pg_catalog', 'information_schema') and table_catalog = current_database() order by table_name");
			$tableRows = $db->fetchAllObjects($db->executeQuery($ps), true);
			foreach ($tableRows as $tableRow) {
				$tableNames[] = $tableRow->table_name;
			}
			unset($tableRow);
			unset($tableRows);
			break;
		}	// switch ($dialect)
		if (!empty($allowedTableNames)) {
			$ntns = array();
			foreach ($allowedTableNames as $tn) {
				if (in_array($tn, $tableNames)) {
					$ntns[] = $tn;
				}
			}
			$tableNames = $ntns;
			unset($ntns);
			unset($tn);
		}
		$tableNames = array_unique($tableNames);

		// Process all tables which qualify.
		foreach ($tableNames as $tableName) {
			$table = new DDLTable($tableName);
			$tableIndexes = array();
			switch ($dialect) {
			case 'mysql':
				$ps = new PreparedStatement('desc '.$tableName);
				$rs = $db->executeQuery($ps);
				while (($tc = $db->fetchObject($rs)) !== false) {
					$name = $tc->Field;
					$size = 0;
					$scale = 0;
					$allowNull = (strcasecmp($tc->Null, 'yes') == 0) ? true : false;
					$defaultValue = false;
					$sysVarDefault = false;
					$autoIncrement = false;
					$useTimeZone = false;

					$tp = strtolower($tc->Type);
					if (strncmp($tp, 'int', 3) == 0) {
						$type = 'integer';
					} else if ((strncmp($tp, 'tinyint', 7) == 0) ||
								(strncmp($tp, 'smallint', 8) == 0)) {
						$type = 'smallint';
					} else if ((strncmp($tp, 'mediumint', 9) == 0) ||
								(strncmp($tp, 'bigint', 6) == 0)) {
						$type = 'bigint';
					} else if ((strncmp($tp, 'dec', 3) == 0) ||
								(strncmp($tp, 'numeric', 7) == 0) ||
								(strncmp($tp, 'float', 5) == 0) ||
								(strncmp($tp, 'double', 6) == 0) ||
								(strncmp($tp, 'real', 4) == 0)) {
						$type = 'decimal';
						$size = $this->__mysql_parseSize($tp);
						$scale = $this->__mysql_parseScale($tp);
					} else if (strncmp($tp, 'char', 4) == 0) {
						$type = 'char';
						$size = $this->__mysql_parseSize($tp);
					} else if (strncmp($tp, 'varchar', 7) == 0) {
						$type = 'varchar';
						$size = $this->__mysql_parseSize($tp);
					} else if (strncmp($tp, 'binary', 6) == 0) {
						$type = 'binary';
						$size = $this->__mysql_parseSize($tp);
					} else if (strncmp($tp, 'varbinary', 9) == 0) {
						$type = 'varbinary';
						$size = $this->__mysql_parseSize($tp);
					} else if ((strncmp($tp, 'text', 4) == 0) ||
								(strncasecmp($tp, 'tinytext', 8) == 0) ||
								(strncasecmp($tp, 'mediumtext', 10) == 0) ||
								(strncasecmp($tp, 'longtext', 8) == 0)) {
						$type = 'text';
					} else if ((strncmp($tp, 'blob', 4) == 0) ||
								(strncmp($tp, 'tinyblob', 8) == 0) ||
								(strncmp($tp, 'mediumblob', 10) == 0) ||
								(strncmp($tp, 'longblob', 8) == 0)) {
						$type = 'blob';
					} else if ($tp == 'date') {
						$type = 'date';
					} else if ($tp == 'time') {
						$type = 'time';
					} else if ($tp == 'datetime') {
						$type = 'datetime';
					} else if ($tp == 'timestamp') {
						$type = 'datetime';
						$useTimeZone = true;
					}

					if (stripos($tc->Extra, 'auto_increment') !== false) {
						$autoIncrement = true;
					} else if ($tc->Default === null) {
						if (!$allowNull) {
							switch ($type) {
							case 'integer':
							case 'smallint':
							case 'bigint':
								$defaultValue = 0;
								break;
							case 'decimal':
								$defaultValue = 0.0;
								break;
							default:
								$defaultValue = '';
								break;
							}
						}
					} else if ($tc->Default == 'CURRENT_TIMESTAMP') {
						$sysVarDefault = 'CURRENT_TIMESTAMP';
					} else {
						$defaultValue = $tc->Default;
						// For date, datetime and time defaults which are all zeros,
						// take the default down to an empty string.
						if ((($type == 'date') && ($defaultValue == '0000-00-00')) ||
							(($type == 'date') && ($defaultValue == '0001-01-01')) ||
							(($type == 'datetime') && ($defaultValue == '0000-00-00 00:00:00')) ||
							(($type == 'datetime') && ($defaultValue == '0001-01-01 00:00:00')) ||
							(($type == 'time') && ($defaultValue == '00:00:00'))) {
							$defaultValue = '';
						}
					}

					$table->columns[] = new DDLTableColumn(
						$name,
						$type,
						$size,
						$scale,
						$allowNull,
						$defaultValue,
						$sysVarDefault,
						$autoIncrement,
						$useTimeZone
					);
				}	// while (($tc = $db->fetchObject($rs)) !== false)
				$db->freeResult($rs);
				unset($tc);

				$ps = new PreparedStatement('show indexes from '.$tableName);
				$irs = $db->fetchAllObjects($db->executeQuery($ps), true);
				$idxs = array();
				foreach ($irs as $ir) {
					// For mysql, omit indexes which have the same name as a foreign key.
					$isFKIndex = false;
					foreach ($mysqlFKs as $fk) {
						if (($fk->localTableName == $tableName) &&
							($fk->foreignKeyName == $ir->Key_name)) {
							$isFKIndex = true;
							break;
						}
					}
					if ($isFKIndex) {
						// This index corresponds to a foreign key.  Skip it.
						continue;
					}
					$idxname = $ir->Key_name;
					$colname = $ir->Column_name;
					if (strcasecmp($idxname, 'primary') == 0) {
						$found = false;
						for ($i = 0, $n = count($idxs); $i < $n; $i++) {
							if (get_class($idxs[$i]) == 'DDLPrimaryKey') {
								$idxs[$i]->columns[] = new DDLKeyColumn($colname);
								$found = true;
								break;
							}
						}
						if (!$found) {
							$idxs[] = new DDLPrimaryKey(array(new DDLKeyColumn($colname)));
						}
					} else {
						$found = false;
						for ($i = 0, $n = count($idxs); $i < $n; $i++) {
							if ((get_class($idxs[$i]) == 'DDLIndex') &&
								($idxs[$i]->indexName == $idxname)) {
								$idxs[$i]->columns[] = new DDLKeyColumn($colname);
								$found = true;
								break;
							}
						}
						if (!$found) {
							$idxs[] = new DDLIndex(
								$idxname,
								$tableName,
								(((int)$ir->Non_unique) == 0) ? true : false,
								array(new DDLKeyColumn($colname))
							);
						}
					}
				}
				unset($irs);
				unset($ir);
				foreach ($idxs as $idx) {
					switch (get_class($idx)) {
					case 'DDLPrimaryKey':
						$table->primaryKey = $idx;
						break;
					case 'DDLIndex':
						$tableIndexes[] = $idx;
						break;
					}
				}
				unset($idx);
				unset($idxs);
				break;

			case 'pgsql':
				$ps = new PreparedStatement("select * from information_schema.columns where table_catalog = current_database() and table_name = '$tableName' order by ordinal_position");
				$rs = $db->executeQuery($ps);
				while (($tc = $db->fetchObject($rs)) !== false) {
					$name = $tc->column_name;
					$size = 0;
					$scale = 0;
					$allowNull = (strcasecmp($tc->is_nullable, 'yes') == 0) ? true : false;
					$defaultValue = false;
					$sysVarDefault = false;
					$autoIncrement = false;
					$useTimeZone = false;

					$tp = strtolower($tc->data_type);
					if (($tp == 'char') || ($tp == 'character')) {
						$type = 'char';
						$size = (int)$tc->character_maximum_length;
					} else if (($tp == 'character varying') || ($tp == 'varchar')) {
						$type = 'varchar';
						$size = (int)$tc->character_maximum_length;
					} else if ($tp == 'text') {
						$type = 'text';
					} else if ($tp == 'bigint') {
						$type = 'bigint';
					} else if (($tp == 'int') || ($tp == 'integer')) {
						$type = 'integer';
					} else if ($tp == 'smallint') {
						$type = 'smallint';
					} else if ($tp == 'serial') {
						$type = 'integer';
						$autoIncrement = true;
					} else if ($tp == 'bigserial') {
						$type = 'bigint';
						$autoIncrement = true;
					} else if (($tp == 'numeric') || ($tp == 'decimal')) {
						$type = 'decimal';
						$size = (int)$tc->numeric_precision;
						$scale = (int)$tc->numeric_scale;
					} else if ($tp == 'date') {
						$type = 'date';
					} else if ($tp == 'time') {
						$type = 'time';
					} else if ($tp == 'timestamp') {
						$type = 'datetime';
					} else if ($tp == 'timestamp with time zone') {
						$type = 'datetime';
						$useTimeZone = true;
					} else if ($tp == 'bytea') {
						$type = 'blob';
					} else {
						throw new Exception("Unsupported PostgreSQL type ($tp) in table table $tableName");
					}

					$dv = $tc->column_default;
					if ($dv === null) {
						if (!$allowNull) {
							switch ($type) {
							case 'integer':
							case 'smallint':
							case 'bigint':
								$defaultValue = 0;
								break;
							case 'decimal':
								$defaultValue = 0.0;
								break;
							default:
								$defaultValue = '';
								break;
							}
						}
					} else {	// if ($dv === null)
						// Some column defaults are suffixed with '::' followed by the data type.
						// Strip those suffixes off.
						$testsuffix = '::'.$tc->data_type;
						if ((($suffixpos = strrpos($dv, $testsuffix)) !== false) &&
							(($suffixpos+strlen($testsuffix)) == strlen($dv))) {
							$dv = substr($dv, 0, $suffixpos);
						}
						if (stripos($dv, 'nextval') === 0) {
							$autoIncrement = true;
						} else if (($dv == 'now()') || (strcasecmp($dv, 'CURRENT_TIMESTAMP') == 0)) {
							$sysVarDefault = 'CURRENT_TIMESTAMP';
						} else {
							if ((strlen($dv) >= 2) && ($dv[0] == '\'') && ($dv[strlen($dv)-1] == '\'')) {
								// Strip leading/trailing quotes; replace all instances of '' with '.
								$dv = str_replace("''", "'", substr($dv, 1, strlen($dv)-2));
							}
							$defaultValue = $dv;
							// For date, datetime and time defaults which are all zeros,
							// take the default down to an empty string.
							if ((($type == 'date') && ($defaultValue == '0000-00-00')) ||
								(($type == 'date') && ($defaultValue == '0001-01-01')) ||
								(($type == 'datetime') && ($defaultValue == '0000-00-00 00:00:00')) ||
								(($type == 'datetime') && ($defaultValue == '0001-01-01 00:00:00')) ||
								(($type == 'time') && ($defaultValue == '00:00:00'))) {
								$defaultValue = '';
							}
						}
					}	// if ($dv === null) ... else

					$table->columns[] = new DDLTableColumn(
						$name,
						$type,
						$size,
						$scale,
						$allowNull,
						$defaultValue,
						$sysVarDefault,
						$autoIncrement,
						$useTimeZone
					);
				}	// while (($tc = $db->fetchObject($rs)) !== false)
				$db->freeResult($rs);
				unset($tc);

				$ps = new PreparedStatement("select pg_class.relname, pg_index.indkey, pg_index.indisunique, pg_index.indisprimary from pg_class, pg_index where pg_class.oid = pg_index.indexrelid and pg_class.oid in (select indexrelid from pg_index, pg_class where pg_class.relname = '$tableName' and pg_class.oid = pg_index.indrelid)");
				$irs = $db->fetchAllObjects($db->executeQuery($ps), true);
				foreach ($irs as $ir) {
					$idxname = $ir->relname;
					if ($ir->indisprimary == 't') {
						$idx = new DDLPrimaryKey();
					} else {
						$idx = new DDLIndex(
							$idxname,
							$tableName,
							($ir->indisunique == 't') ? true : false
						);
					}

					foreach (explode(' ', $ir->indkey) as $indkey) {
						$ps = new PreparedStatement("select a.attname from pg_index c left join pg_class t on c.indrelid  = t.oid left join pg_attribute a on a.attrelid = t.oid and a.attnum = any(indkey) where t.relname = '$tableName' and a.attnum = $indkey");
						$crs = $db->fetchAllObjects($db->executeQuery($ps), true);
						foreach ($crs as $cr) {
							$idx->columns[] = new DDLKeyColumn($cr->attname);
						}
					}

					if ($ir->indisprimary == 't') {
						$table->primaryKey = $idx;
					} else {
						$tableIndexes[] = $idx;
					}
				}
				break;
			}	// switch ($dialect)
			$ddl->topLevelEntities[] = $table;
			if (!empty($tableIndexes)) {
				$ddl->topLevelEntities = array_merge($ddl->topLevelEntities, $tableIndexes);
			}

			// Get foreign keys for this table.
			switch ($dialect) {
			case 'mysql':
				foreach ($mysqlFKs as $fk) {
					if ($fk->localTableName == $tableName) {
						$ddl->topLevelEntities[] = $fk;
					}
				}
				break;
			case 'pgsql':
				$ps = new PreparedStatement("select constraint_name from information_schema.table_constraints where table_name = '$tableName' and constraint_type = 'FOREIGN KEY'");
				$fkrs = $db->fetchAllObjects($db->executeQuery($ps), true);
				foreach ($fkrs as $fkr) {
					$fkname = $fkr->constraint_name;
					$ps = new PreparedStatement(<<<EOF
select tc.constraint_name,
tc.constraint_type,
tc.table_name,
kcu.column_name,
ccu.table_name as references_table,
ccu.column_name as references_field
from information_schema.table_constraints tc
left join information_schema.key_column_usage kcu
on tc.constraint_catalog = kcu.constraint_catalog
and tc.constraint_schema = kcu.constraint_schema
and tc.constraint_name = kcu.constraint_name
left join information_schema.referential_constraints rc
on tc.constraint_catalog = rc.constraint_catalog
and tc.constraint_schema = rc.constraint_schema
and tc.constraint_name = rc.constraint_name
left join information_schema.constraint_column_usage ccu
on rc.constraint_catalog = ccu.constraint_catalog
and rc.constraint_schema = ccu.constraint_schema
and rc.constraint_name = ccu.constraint_name
EOF
.
		" where tc.table_name = '$tableName' and tc.constraint_name = '$fkname'"
					);
					$fkcs = $db->fetchAllObjects($db->executeQuery($ps), true);
					$fk = false;
					foreach ($fkcs as $fkc) {
						if ($fk === false) {
							$fk = new DDLForeignKey(
								$fkname,
								$tableName,
								$fkc->references_table
							);
						}
						$fk->columns[] = new DDLForeignKeyColumn($fkc->column_name, $fkc->references_field);
					}
					if ($fk !== false) $ddl->topLevelEntities[] = $fk;
				}
				break;
			}	// switch ($dialect)

			if ($generateInserts) {
				$sql = 'select * from '.$tableName;
				if ($table->primaryKey !== false) {
					$sql .= ' order by ';
					$sep = '';
					foreach ($table->primaryKey->columns as $col) {
						$sql .= $sep.$col->name;
						if ($sep == '') $sep = ', ';
					}
				}
				$ps = new PreparedStatement($sql);
				$rs = $db->executeQuery($ps);
				while (($row = $db->fetchObject($rs)) !== false) {
					$insert = new DDLInsert($tableName);
					foreach ($row as $key=>$val) {
						$quoted = true;
						$colidx = $table->getColumnIdx($key);
						if ($colidx >= 0) {
							switch ($table->columns[$colidx]->type) {
							case 'integer':
							case 'smallint':
							case 'bigint':
							case 'decimal':
								$quoted = false;
								break;
							default:
								$quoted = true;
								break;
							}
						}
						$inscol = new DDLInsertColumn(
							$key,
							$val,
							false,
							$quoted
						);
						$insert->columns[] = $inscol;
					}
					unset($key);
					unset($val);

					$ddl->topLevelEntities[] = $insert;
				}
				$db->freeResult($rs);
			}

			unset($table);	// release reference to array element
		}	// foreach ($tableNames as $tableName)

		return $ddl;
	}

	protected function __mysql_parseSize($type) {
		if (($idx1 = strpos($type, '(')) === false) return 0;
		$idx1++;
		if (($idx2 = strpos($type, ')', $idx1)) === false) return 0;
		$pieces = explode(',', substr($type, $idx1, $idx2-$idx1));
		if (!empty($pieces)) return (int)trim($pieces[0]);
		return 0;
	}

	protected function __mysql_parseScale($type) {
		if (($idx1 = strpos($type, '(')) === false) return 0;
		$idx1++;
		if (($idx2 = strpos($type, ')', $idx1)) === false) return 0;
		$pieces = explode(',', substr($type, $idx1, $idx2-$idx1));
		if (isset($pieces[1])) return (int)trim($pieces[1]);
		return 0;
	}
}

// The XMLDDLParser class parses an XML document which conforms to ddl.dtd, and returns a DDL
// instance which represents the DDL described in the XML document.
class XMLDDLParser {
	protected $ddl, $ddlTable, $ddlIndex, $ddlForeignKey, $ddlInsert;
	protected $allowedTableNames, $elnames, $autoIncColName;

	// Parse and return a DDL instance from the text of an XML document.
	// Parameters:
	// $allowedTableNames: An array of table names which are allowed to be processed, or an empty
	// array to allow all tables.  Optional.  Defaults to an empty array.
	// Returns: A DDL instance.
	public function parseFromXML($xml, $allowedTableNames = array()) {
		$this->ddl = new DDL();
		$this->ddlTable = false;
		$this->ddlIndex = false;
		$this->ddlForeignKey = false;
		$this->ddlInsert = false;
		$this->elnames = array();
		$this->autoIncColName = false;
		$this->allowedTableNames = $allowedTableNames;

		$xmlParser = false;
		$exc = false;
		try {
			$xmlParser = xml_parser_create();
			xml_set_object($xmlParser, $this);
			xml_set_element_handler($xmlParser, '__startElement', '__endElement');
			xml_set_character_data_handler($xmlParser, '__characterData');
			xml_parser_set_option($xmlParser, XML_OPTION_CASE_FOLDING, false);
			xml_parser_set_option($xmlParser, XML_OPTION_SKIP_WHITE, true);
			if (!xml_parse($xmlParser, $xml, true)) {
				$exc = new Exception(xml_error_string(xml_get_error_code()));
			}
		} catch (Exception $ex) {
			$exc = $ex;
		}
		if ($xmlParser !== false) {
			xml_parser_free($xmlParser);
		}
		if ($exc !== false) {
			throw $exc;
		}
		if ($this->ddl === false) {
			throw new Exception("Unexpected error during DDL XML parsing.");
		}
		return $this->ddl;
	}

	protected function __startElement($xmlParser, $name, $attrs) {
		$this->elnames[] = $name;
		if (count($this->elnames) >= 2) {
			switch (count($this->elnames)) {
			case 1:
				switch ($this->elnames[0]) {
				case 'ddl':
					break;
				default:
					$this->__unexpectedTagHierarchy($this->elnames);
					break;
				}
				break;
			case 2:
				switch ($this->elnames[1]) {
				case 'table':
					$this->__allowOnlyAttrs($this->elnames[1], $attrs, array('name'));
					$this->__requireAttrs($this->elnames[1], $attrs, array('name'));
					$this->ddlTable = new DDLTable($attrs['name']);
					$this->autoIncColName = '';
					break;
				case 'index':
					$this->__allowOnlyAttrs
						($this->elnames[1], $attrs, array('name', 'table', 'unique'));
					$this->__requireAttrs($this->elnames[1], $attrs, array('name', 'table'));
/// TODO: Verify that the index name hasn't been already created.
					// Make sure index name begins with table name and underscore.
					$idxname = $attrs['name'];
					if (strpos($idxname, $attrs['table'].'_') !== 0) {
						$idxname = $attrs['table'].'_'.$idxname;
					}
					$this->ddlIndex = new DDLIndex(
						$idxname,
						$attrs['table'],
						(isset($attrs['unique']) && ($attrs['unique'] == 'true')) ?
							true : false
					);
					break;
				case 'foreign-key':
					$this->__allowOnlyAttrs
						($this->elnames[1], $attrs, array('name', 'local-table', 'foreign-table'));
					$this->__requireAttrs($this->elnames[1], $attrs, array('name', 'local-table', 'foreign-table'));
/// TODO: Verify that the foreign key name hasn't been already created.
					$this->ddlForeignKey = new DDLForeignKey(
						$attrs['name'],
						$attrs['local-table'],
						$attrs['foreign-table']
					);
					break;
				case 'insert':
					$this->__allowOnlyAttrs($this->elnames[1], $attrs, array('table', 'key-column-names', 'update-if-exists'));
					$this->__requireAttrs($this->elnames[1], $attrs, array('table'));

					$keyColumnNames = isset($attrs['key-column-names']) ?
						trim($attrs['key-column-names']) : '';
					if ($keyColumnNames != '') {
						$keyColumnNames = explode(',', $keyColumnNames);
						for ($kcni = 0; $kcni < count($keyColumnNames);) {
							if (($keyColumnNames[$kcni] = trim($keyColumnNames[$kcni])) == '') {
								unset($keyColumnNames[$kcni]);
								$keyColumnNames = array_slice($keyColumnNames, 0);
							} else {
								$kcni++;
							}
						}
					} else {
						$keyColumnNames = array();
					}
					$this->ddlInsert = new DDLInsert(
						$attrs['table'],
						array(),
						$keyColumnNames,
						((count($keyColumnNames) > 0) &&
							isset($attrs['update-if-exists']) &&
							($attrs['update-if-exists'] == 'true')) ?
								true : false
					);
					break;
				default:
					$this->__unexpectedTagHierarchy($this->elnames);
					break;
				}
				break;
			case 3:
				switch ($this->elnames[1]) {
				case 'table':
					switch ($this->elnames[2]) {
					case 'col':
						$this->__allowOnlyAttrs($this->elnames[2], $attrs, array('name', 'type', 'size', 'scale', 'null', 'default', 'sysvar-default', 'auto-increment', 'use-time-zone'));
						$this->__requireAttrs($this->elnames[2], $attrs, array('name', 'type'));
						switch ($attrs['type']) {
						case 'integer':
						case 'smallint':
						case 'bigint':
							$this->__allowOnlyAttrs($this->elnames[2], $attrs, array('name', 'type', 'null', 'default', 'auto-increment'));
							break;
						case 'decimal':
							$this->__allowOnlyAttrs($this->elnames[2], $attrs, array('name', 'type', 'size', 'scale', 'null', 'default'));
							$this->__requireAttrs($this->elnames[2], $attrs, array('size', 'scale'));
							break;
						case 'char':
						case 'varchar':
						case 'binary':
						case 'varbinary':
							$this->__allowOnlyAttrs($this->elnames[2], $attrs, array('name', 'type', 'size', 'null', 'default'));
							$this->__requireAttrs($this->elnames[2], $attrs, array('size'));
							break;
						case 'text':
						case 'blob':
							$this->__allowOnlyAttrs($this->elnames[2], $attrs, array('name', 'type', 'null', 'default'));
							break;
						case 'date':
							$this->__allowOnlyAttrs($this->elnames[2], $attrs, array('name', 'type', 'null', 'default', 'sysvar-default'));
							break;
						case 'time':
							$this->__allowOnlyAttrs($this->elnames[2], $attrs, array('name', 'type', 'null', 'default', 'sysvar-default'));
							break;
						case 'datetime':
							$this->__allowOnlyAttrs($this->elnames[2], $attrs, array('name', 'type', 'null', 'default', 'sysvar-default', 'use-time-zone'));
							break;
						default:
							throw new Exception(sprintf("Invalid column type: %s", $attrs['type']));
							break;
						}
						if (isset($attrs['auto-increment'])) {
							if (isset($attrs['sysvar-default'])) {
								throw new Exception(
									"Cannot have both auto-increment and sysvar-default".
										" attributes on a column."
								);
							}
							if (isset($attrs['default'])) {
								throw new Exception(
									"Cannot have both auto-increment and default".
										" attributes on a column."
								);
							}
							if ($this->autoIncColName != '') {
								throw new Exception(sprintf(
									"Cannot have more than one auto-increment column per table".
										" in table name \"%s\"",
									$tableName
								));
							}
							$this->autoIncColName = $attrs['name'];
						} else if (isset($attrs['sysvar-default'])) {
							if (isset($attrs['default'])) {
								throw new Exception(
									"Cannot have both sysvar-default and default".
										" attributes on a column."
								);
							}
						}

						$name = $attrs['name'];
						$type = $attrs['type'];
						$defaultValue = isset($attrs['default']) ? $attrs['default'] : false;
						// For date, datetime and time defaults which are all zeros,
						// take the default down to an empty string.
						if ((($type == 'date') && ($defaultValue == '0000-00-00')) ||
							(($type == 'date') && ($defaultValue == '0001-01-01')) ||
							(($type == 'datetime') && ($defaultValue == '0000-00-00 00:00:00')) ||
							(($type == 'datetime') && ($defaultValue == '0001-01-01 00:00:00')) ||
							(($type == 'time') && ($defaultValue == '00:00:00'))) {
							$defaultValue = '';
						}

						// Only save tables which contain one or more columns.
						// Save a table when we encounter the first column definition for
						// that table.
						// Tables with no columns can be used for things like inserts within
						// schema files other than those in which the tables are defined.
						if (empty($ddlTable->columns)) {
							if ((empty($this->allowedTableNames)) ||
								(in_array($this->ddlTable->tableName, $this->allowedTableNames))) {
								$this->ddl->topLevelEntities[] = $this->ddlTable;
							}
						}

						$this->ddlTable->columns[] = new DDLTableColumn(
							$name,
							$type,
							isset($attrs['size']) ? (int)$attrs['size'] : 0,
							isset($attrs['scale']) ? (int)$attrs['scale'] : 0,
							(isset($attrs['null']) && ($attrs['null'] != 'true')) ? false : true,
							$defaultValue,
							isset($attrs['sysvar-default']) ? $attrs['sysvar-default'] : false,
							(isset($attrs['auto-increment']) &&
								($attrs['auto-increment'] == 'true')) ? true : false,
							(isset($attrs['use-time-zone']) &&
								($attrs['use-time-zone'] == 'true')) ? true : false
						);
						break;
					case 'primary-key':
						$this->__allowOnlyAttrs($this->elnames[2], $attrs, array());
						if ($this->ddlTable->primaryKey !== false) {
							throw new Exception(sprintf(
								"Duplicate \"primary-key\" element for table name \"%s\"",
								$this->ddlTable->tableName
							));
						}
						$this->ddlTable->primaryKey = new DDLPrimaryKey();
						break;
					default:
						$this->__unexpectedTagHierarchy($this->elnames);
						break;
					}
					break;
				case 'index':
					switch ($this->elnames[2]) {
					case 'keycol':
						$this->__allowOnlyAttrs($this->elnames[2], $attrs, array('name'));
						$this->__requireAttrs($this->elnames[2], $attrs, array('name'));
/// TODO: Verify that the column exists in the table.
/// TODO: Verify that the column is not repeated in the index.
						$this->ddlIndex->columns[] = new DDLKeyColumn($attrs['name']);
						break;
					default:
						$this->__unexpectedTagHierarchy($this->elnames);
						break;
					}
					break;
				case 'foreign-key':
					switch ($this->elnames[2]) {
					case 'foreign-keycol':
						$this->__allowOnlyAttrs($this->elnames[2], $attrs, array('local-name', 'foreign-name'));
						$this->__requireAttrs($this->elnames[2], $attrs, array('local-name', 'foreign-name'));
/// TODO: Verify that the columns exist in the tables.
/// TODO: Verify that the columns are not repeated in the foreign key.
						$this->ddlForeignKey->columns[] = new DDLForeignKeyColumn($attrs['local-name'], $attrs['foreign-name']);
						break;
					default:
						$this->__unexpectedTagHierarchy($this->elnames);
						break;
					}

					break;
				case 'insert':
					switch ($this->elnames[2]) {
					case 'inscol':
						$this->__allowOnlyAttrs($this->elnames[2], $attrs, array('name', 'value', 'sysvar-value', 'quoted'));
						$this->__requireAttrs($this->elnames[2], $attrs, array('name'));
						$this->__requireAtLeastOneAttr($this->elnames[2], $attrs, array('value', 'sysvar-value'));
						if (isset($attrs['sysvar-value'])) {
							if (isset($attrs['value'])) {
								throw new Exception(sprintf(
									"Cannot have both sysvar-value and value in \"%s\" element.",
									$this->elnames[2]
								));
							}
							if (isset($attrs['quoted'])) {
								throw new Exception(sprintf(
									"Cannot have both sysvar-value and quoted in \"%s\" element.",
									$this->elnames[2]
								));
							}
							$this->ddlInsert->columns[] = new DDLInsertColumn(
								$attrs['name'],
								false,
								$attrs['sysvar-value'],
								false
							);
						} else {
							$this->ddlInsert->columns[] = new DDLInsertColumn(
								$attrs['name'],
								$attrs['value'],
								false,
								(isset($attrs['quoted']) && ($attrs['quoted'] == true)) ?
									true : false
							);
						}
						break;
					default:
						$this->__unexpectedTagHierarchy($this->elnames);
						break;
					}
					break;
				}
				break;
			case 4:
				switch ($this->elnames[1]) {
				case 'table':
					switch ($this->elnames[2]) {
					case 'primary-key':
						switch ($this->elnames[3]) {
						case 'keycol':
							$this->__allowOnlyAttrs($this->elnames[3], $attrs, array('name'));
							$this->__requireAttrs($this->elnames[3], $attrs, array('name'));
/// TODO: Verify that the column exists in the table.
/// TODO: Verify that the column is not repeated in the primary key.
							$this->ddlTable->primaryKey->columns[] =
								new DDLKeyColumn($attrs['name']);
							break;
						default:
							$this->__unexpectedTagHierarchy($this->elnames);
							break;
						}
						break;
					}
				}
				break;
			default:
				$this->__unexpectedTagHierarchy($this->elnames);
				break;
			}
		}
	}

	protected function __endElement($xmlParser, $name) {
		if ( (count($this->elnames) >= 2) && ($this->elnames[0] == 'ddl') ) {
			switch (count($this->elnames)) {
			case 2:
				switch ($this->elnames[1]) {
				case 'table':
					if ($this->autoIncColName != '') {
						if (($this->ddlTable->primaryKey === false) || 
							((count($this->ddlTable->primaryKey->columns) != 1) ||
							($this->ddlTable->primaryKey->columns[0]->name !=
								$this->autoIncColName))) {
							throw new Exception(sprintf(
								"Use of auto-increment requires the primary key to be comprised".
									" of only the auto-increment column in table \"%s\".",
								$this->ddlTable->tableName
							));
						}
					}
					$this->ddlTable = false;
					break;
				case 'index':
					if ((empty($this->allowedTableNames)) ||
						(in_array($this->ddlIndex->tableName, $this->allowedTableNames))) {
						$this->ddl->topLevelEntities[] = $this->ddlIndex;
					}
					$this->ddlIndex = false;;
					break;
				case 'foreign-key':
					if ((empty($this->allowedTableNames)) ||
						(in_array($this->ddlForeignKey->localTableName, $this->allowedTableNames))) {
						$this->ddl->topLevelEntities[] = $this->ddlForeignKey;
					}
					$this->ddlForeignKey = false;;
					break;
				case 'insert':
					if ((empty($this->allowedTableNames)) ||
						(in_array($this->ddlInsert->tableName, $this->allowedTableNames))) {
						$this->ddl->topLevelEntities[] = $this->ddlInsert;
					}
					$this->ddlInsert = false;;
					break;
				}
				break;
			}
		}
		array_pop($this->elnames);
	}

	protected function __characterData($xmlParser, $characterData) { }

	protected function __allowOnlyAttrs($tagName, $attrs, $attrNames) {
		foreach (array_keys($attrs) as $attrName) {
			if (!in_array($attrName, $attrNames)) {
				throw new Exception(sprintf(
					"Unexpected attribute \"%s\" in \"%s\" element.",
					$attrName,
					$tagName
				));
			}
		}
	}

	protected function __requireAttrs($tagName, $attrs, $attrNames) {
		foreach ($attrNames as $attrName) {
			if (!isset($attrs[$attrName])) {
				throw new Exception
					(sprintf("Missing attribute \"%s\" in \"%s\" element.", $attrName, $tagName));
			}
		}
	}

	protected function __requireAtLeastOneAttr($tagName, $attrs, $attrNames) {
		foreach ($attrNames as $attrName) {
			if (isset($attrs[$attrName])) {
				return;
			}
		}
		throw new Exception(sprintf(
			"Must specify one of [%s] attributes in \"%s\" element.",
			implode(', ', $attrNames),
			$tagName
		));
	}

	protected function __unexpectedTagHierarchy($tagNames) {
		throw new Exception(sprintf("Unexpected tag hierarchy: %s.", implode(',', $tagNames)));
	}
}


// The YAMLDDLParser class parses a YAML document, and returns a DDL
// instance which represents the DDL described in the YAML document.
class YAMLDDLParser {
	// Parse and return a DDL instance from the text of a YAML document.
	// Parameters:
	// $allowedTableNames: An array of table names which are allowed to be processed, or an empty
	// array to allow all tables.  Optional.  Defaults to an empty array.
	// Returns: A DDL instance.
	public function parseFromYAML($yaml, $allowedTableNames = array()) {
		$ddl = new DDL();
		$allowedTableNames = $allowedTableNames;

		$cfg = @Spyc::YAMLLoadString($yaml);
		if (!is_array($cfg)) $cfg = array();

		if (isset($cfg['tables']) && is_array($cfg['tables'])) {
			foreach ($cfg['tables'] as $tableName=>$tblAttrs) {
				$tblSection = "tables => $tableName";
				$this->__allowOnlyAttrs($tblSection, $tblAttrs, array('columns', 'primaryKey', 'indexes', 'foreignKeys', 'inserts'));
				if ((!empty($allowedTableNames)) && (!in_array($tableName, $allowedTableNames))) {
					continue;
				}
				$autoIncColName = '';
				unset($ddlTable);
				$ddlTable = new DDLTable($tableName);
				if (isset($tblAttrs['columns']) && is_array($tblAttrs['columns'])) {
					$colSection = "tables => $tableName => columns";
					foreach ($tblAttrs['columns'] as $columnName=>$colAttrs) {
						$this->__allowOnlyAttrs($colSection, $colAttrs, array('type', 'size', 'scale', 'null', 'default', 'sysVarDefault', 'autoIncrement', 'useTimeZone'));
						$this->__requireAttrs($colSection, $colAttrs, array('type'));
						switch ($colAttrs['type']) {
						case 'integer':
						case 'smallint':
						case 'bigint':
							$this->__allowOnlyAttrs($colSection, $colAttrs, array('type', 'null', 'default', 'autoIncrement'));
							break;
						case 'decimal':
							$this->__allowOnlyAttrs($colSection, $colAttrs, array('type', 'size', 'scale', 'null', 'default'));
							$this->__requireAttrs($colSection, $colAttrs, array('size', 'scale'));
							break;
						case 'char':
						case 'varchar':
						case 'binary':
						case 'varbinary':
							$this->__allowOnlyAttrs($colSection, $colAttrs, array('type', 'size', 'null', 'default'));
							$this->__requireAttrs($colSection, $colAttrs, array('size'));
							break;
						case 'text':
						case 'blob':
							$this->__allowOnlyAttrs($colSection, $colAttrs, array('type', 'null', 'default'));
							break;
						case 'date':
							$this->__allowOnlyAttrs($colSection, $colAttrs, array('type', 'null', 'default', 'sysVarDefault'));
							break;
						case 'time':
							$this->__allowOnlyAttrs($colSection, $colAttrs, array('type', 'null', 'default', 'sysVarDefault'));
							break;
						case 'datetime':
							$this->__allowOnlyAttrs($colSection, $colAttrs, array('type', 'null', 'default', 'sysVarDefault', 'useTimeZone'));
							break;
						default:
							throw new Exception(sprintf("Invalid column type: %s", $colAttrs['type']));
							break;
						}
						if (isset($colAttrs['autoIncrement'])) {
							if (isset($colAttrs['sysVarDefault'])) {
								throw new Exception(
									"Cannot have both autoIncrement and sysVarDefault".
										" attributes on a column."
								);
							}
							if (isset($colAttrs['default'])) {
								throw new Exception(
									"Cannot have both autoIncrement and default".
										" attributes on a column."
								);
							}
							if ($autoIncColName != '') {
								throw new Exception(sprintf(
									"Cannot have more than one autoIncrement column per table".
										" in table name \"%s\"",
									$tableName
								));
							}
							$autoIncColName = $columnName;
						} else if (isset($colAttrs['sysVarDefault'])) {
							if (isset($colAttrs['default'])) {
								throw new Exception(
									"Cannot have both sysVarDefault and default".
										" attributes on a column."
								);
							}
						}

						$type = $colAttrs['type'];
						$defaultValue = isset($colAttrs['default']) ? $colAttrs['default'] : false;
						// For date, datetime and time defaults which are all zeros,
						// take the default down to an empty string.
						if ((($type == 'date') && ($defaultValue == '0000-00-00')) ||
							(($type == 'date') && ($defaultValue == '0001-01-01')) ||
							(($type == 'datetime') && ($defaultValue == '0000-00-00 00:00:00')) ||
							(($type == 'datetime') && ($defaultValue == '0001-01-01 00:00:00')) ||
							(($type == 'time') && ($defaultValue == '00:00:00'))) {
							$defaultValue = '';
						}

						// Only save tables which contain one or more columns.
						// Save a table when we encounter the first column definition for
						// that table.
						// Tables with no columns can be used for things like inserts within
						// schema files other than those in which the tables are defined.
						if (empty($ddlTable->columns)) {
							$ddl->topLevelEntities[] = &$ddlTable;
						}

						$ddlTable->columns[] = new DDLTableColumn(
							$columnName,
							$type,
							isset($colAttrs['size']) ? (int)$colAttrs['size'] : 0,
							isset($colAttrs['scale']) ? (int)$colAttrs['scale'] : 0,
							(isset($colAttrs['null']) && (!$colAttrs['null'])) ? false : true,
							$defaultValue,
							isset($colAttrs['sysVarDefault']) ? $colAttrs['sysVarDefault'] : false,
							(isset($colAttrs['autoIncrement']) && ($colAttrs['autoIncrement'])) ?
								true : false,
							(isset($colAttrs['useTimeZone']) && ($colAttrs['useTimeZone'])) ?
								true : false
						);
					}	// foreach ($tblAttrs['columns'] as $columnName=>$colAttrs)
				}	// if (isset($tblAttrs['columns']) && is_array($tblAttrs['columns']))

				if (isset($tblAttrs['primaryKey']) && is_array($tblAttrs['primaryKey'])) {
					$pkSection = "tables => $tableName => primaryKey";
					$this->__allowOnlyAttrs($colSection, $tblAttrs['primaryKey'], array('columns'));
					$ddlTable->primaryKey = new DDLPrimaryKey();
					if (isset($tblAttrs['primaryKey']['columns']) &&
						is_array($tblAttrs['primaryKey']['columns'])) {
						$pkColsSection = "tables => $tableName => primaryKey => columns";
						foreach ($tblAttrs['primaryKey']['columns'] as $columnName) {
							$ddlTable->primaryKey->columns[] = new DDLKeyColumn($columnName);
						}
					}
				}

				if (isset($tblAttrs['indexes']) && is_array($tblAttrs['indexes'])) {
					foreach ($tblAttrs['indexes'] as $indexName=>$indexAttrs) {
						// Make sure index name begins with table name and underscore.
						if (strpos($indexName, $tableName.'_') !== 0) {
							$indexName = $tableName.'_'.$indexName;
						}

						$indexSection = "tables => $tableName => indexes => $indexName";
						$this->__allowOnlyAttrs($indexSection, $indexAttrs, array('unique', 'columns'));
/// TODO: Verify that the index name hasn't been already created.
						unset($ddlIndex);
						$ddlIndex = new DDLIndex(
							$indexName,
							$tableName,
							(isset($indexAttrs['unique']) && ($indexAttrs['unique'])) ?
								true : false
						);
						$ddl->topLevelEntities[] = &$ddlIndex;
						if (isset($indexAttrs['columns']) && is_array($indexAttrs['columns'])) {
							foreach ($indexAttrs['columns'] as $columnName) {
								$ddlIndex->columns[] = new DDLKeyColumn($columnName);
							}
						}
					}
				}

				if (isset($tblAttrs['foreignKeys']) && is_array($tblAttrs['foreignKeys'])) {
					foreach ($tblAttrs['foreignKeys'] as $fkName=>$fkAttrs) {
						$fkSection = "tables => $tableName => foreignKeys => $fkName";
						$this->__allowOnlyAttrs($fkSection, $fkAttrs, array('foreignTable', 'columns'));
						$this->__requireAttrs($fkSection, $fkAttrs, array('foreignTable'));
/// TODO: Verify that the foreign key name hasn't been already created.
						unset($ddlForeignKey);
						$ddlForeignKey = new DDLForeignKey(
							$fkName,
							$tableName,
							$fkAttrs['foreignTable'],
							(isset($attrs['unique']) && ($attrs['unique'])) ?
								true : false
						);
						$ddl->topLevelEntities[] = &$ddlForeignKey;
						if (isset($fkAttrs['columns']) && is_array($fkAttrs['columns'])) {
							foreach ($fkAttrs['columns'] as $colSec=>$columnAttrs) {
								$columnSection = "tables => $tableName => foreignKeys => $fkName => columns => $colSec";
								$this->__allowOnlyAttrs($columnSection, $columnAttrs, array('local', 'foreign'));
								$this->__requireAttrs($columnSection, $columnAttrs, array('local', 'foreign'));
								$ddlForeignKey->columns[] = new DDLForeignKeyColumn($columnAttrs['local'], $columnAttrs['foreign']);
							}
						}
					}
				}

				if (isset($tblAttrs['inserts']) && is_array($tblAttrs['inserts'])) {
					foreach ($tblAttrs['inserts'] as $insertName=>$insertAttrs) {
						unset($ddlInsert);
						$keyColumnNames =
							(isset($insertAttrs['keyColumnNames']) &&
							is_array($insertAttrs['keyColumnNames'])) ?
							$insertAttrs['keyColumnNames'] : array();
						$ddlInsert = new DDLInsert(
							$tableName,
							array(),
							$keyColumnNames,
							((count($keyColumnNames) > 0) &&
								isset($insertAttrs['updateIfExists']) &&
								($insertAttrs['updateIfExists'])) ?
									true : false
						);
						$ddl->topLevelEntities[] = &$ddlInsert;
						foreach ($insertAttrs as $columnName=>$columnAttrs) {
							if (($columnName == 'keyColumnNames') ||
								($columnName == 'updateIfExists')) {
								continue;
							}

							$columnSection = "tables => $tableName => inserts => $insertName => $columnName";
							$this->__allowOnlyAttrs($columnSection, $columnAttrs, array('value', 'sysVarValue', 'quoted'));
							$this->__requireAtLeastOneAttr($columnSection, $columnAttrs, array('value', 'sysVarValue'));
							if (isset($columnAttrs['sysVarValue'])) {
								if (isset($columnAttrs['value'])) {
									throw new Exception(sprintf(
										"Cannot have both sysVarValue and value in \"%s\" section.",
										$columnSection
									));
								}
								if (isset($columnAttrs['quoted'])) {
									throw new Exception(sprintf(
										"Cannot have both sysVarValue and quoted in \"%s\" section.",
										$columnSection
									));
								}
								$ddlInsert->columns[] = new DDLInsertColumn(
									$columnName,
									false,
									$columnAttrs['sysVarValue'],
									false
								);
							} else {
								$ddlInsert->columns[] = new DDLInsertColumn(
									$columnName,
									$columnAttrs['value'],
									false,
									(isset($columnAttrs['quoted']) && ($columnAttrs['quoted'])) ? true : false
								);
							}
						}
					}
				}

				if ($autoIncColName != '') {
					if (($ddlTable->primaryKey === false) || 
						((count($ddlTable->primaryKey->columns) != 1) ||
						($ddlTable->primaryKey->columns[0]->name !=
							$autoIncColName))) {
						throw new Exception(sprintf(
							"Use of autoIncrement requires the primary key to be comprised".
								" of only the autoIncrement column in table \"%s\".",
							$ddlTable->tableName
						));
					}
				}
			}	// foreach ($cfg['tables'] as $tableName=>$tblAttrs)
		}	// if (isset($cfg['tables']) && is_array($cfg['tables']))

		return $ddl;
	}

	protected function __allowOnlyAttrs($sectionName, $attrs, $attrNames) {
		foreach (array_keys($attrs) as $attrName) {
			if (!in_array($attrName, $attrNames)) {
				throw new Exception(sprintf(
					"Unexpected attribute \"%s\" in \"%s\" section.",
					$attrName,
					$sectionName
				));
			}
		}
	}

	protected function __requireAttrs($sectionName, $attrs, $attrNames) {
		foreach ($attrNames as $attrName) {
			if (!isset($attrs[$attrName])) {
				throw new Exception
					(sprintf("Missing attribute \"%s\" in \"%s\" section.", $attrName, $sectionName));
			}
		}
	}

	protected function __requireAtLeastOneAttr($sectionName, $attrs, $attrNames) {
		foreach ($attrNames as $attrName) {
			if (isset($attrs[$attrName])) {
				return;
			}
		}
		throw new Exception(sprintf(
			"Must specify one of [%s] attributes in \"%s\" section.",
			implode(', ', $attrNames),
			$sectionName
		));
	}
}


// The XMLDDLSerializer class outputs an XML document which conforms to ddl.dtd, given a valid
// DDL instance.
class XMLDDLSerializer {
	// Serialize a DDL object tree, returning XML.
	// Parameters:
	// $ddl: A valid DDL instance.
	// $dtdURI: The URI to ddl.dtd, for DTD validation whenever the XML document is loaded by a
	// validating XML parser.  Optional. Defaults to empty.
	// Returns: The XML document, as a string.
	public function serialize($ddl, $dtdURI = '') {
		$xml = "<"."?xml version=\"1.0\"?".">\n";
		if ($dtdURI != '') {
			$xml .= sprintf("<!DOCTYPE ddl SYSTEM \"%s\">\n", $dtdURI);
		}
		$xml .= "<ddl>\n";
		$anythingOutput = false;
		foreach ($ddl->topLevelEntities as &$tle) {
			switch (get_class($tle)) {
			case 'DDLTable':
				if ($anythingOutput) {
					$xml .= "\n\n";
				}
				$anythingOutput = true;
				$xml .= sprintf(" <table name=\"%s\">\n", htmlspecialchars($tle->tableName));
				foreach ($tle->columns as &$col) {
					$xml .= sprintf(
						'  <col name="%s" type="%s"',
						htmlspecialchars($col->name),
						htmlspecialchars($col->type)
					);
					if ( ($col->useTimeZone) && ($col->type == 'datetime') ) {
						$xml .= ' use-time-zone="true"';
					}
					if (($col->type == 'decimal') ||
						($col->type == 'char') ||
						($col->type == 'varchar') ||
						($col->type == 'binary') ||
						($col->type == 'varbinary')) {
						$xml .= sprintf(' size="%d"', $col->size);
					}
					if ($col->type == 'decimal') {
						$xml .= sprintf(' scale="%d"', $col->scale);
					}
					if (!$col->allowNull) {
						$xml .= ' null="false"';
					}
					if (($col->autoIncrement) &&
						(($col->type == 'integer') ||
						 ($col->type == 'smallint') ||
						 ($col->type == 'bigint'))) {
						$xml .= ' auto-increment="true"';
					} else if ($col->sysVarDefault !== false) {
						$xml .= sprintf(
							' sysvar-default="%s"',
							htmlspecialchars($col->sysVarDefault)
						);
					} else if ($col->defaultValue !== false) {
						$xml .= sprintf(
							' default="%s"',
							htmlspecialchars($col->defaultValue)
						);
					}
					$xml .= "/>\n";
				}
				unset($col);	// release reference to last element
				if ($tle->primaryKey !== false) {
					$xml .= "  <primary-key>\n";
					foreach ($tle->primaryKey->columns as &$col) {
						$xml .= sprintf(
							"   <keycol name=\"%s\"/>\n",
							$col->name
						);
					}
					unset($col);	// release reference to last element
					$xml .= "  </primary-key>\n";
				}
				$xml .= " </table>\n";
				break;
			case 'DDLIndex':
				$anythingOutput = true;
				$xml .= sprintf(
					" <index name=\"%s\" table=\"%s\"",
					htmlspecialchars($tle->indexName),
					htmlspecialchars($tle->tableName)
				);
				if ($tle->unique) {
					$xml .= ' unique="true"';
				}
				$xml .= ">\n";
				foreach ($tle->columns as &$col) {
					$xml .= sprintf(
						"  <keycol name=\"%s\"/>\n",
						$col->name
					);
				}
				unset($col);	// release reference to last element
				$xml .= " </index>\n";
				break;
			case 'DDLForeignKey':
				$anythingOutput = true;
				$xml .= sprintf(
					" <foreign-key name=\"%s\" local-table=\"%s\" foreign-table=\"%s\">\n",
					htmlspecialchars($tle->foreignKeyName),
					htmlspecialchars($tle->localTableName),
					htmlspecialchars($tle->foreignTableName)
				);
				foreach ($tle->columns as &$col) {
					$xml .= sprintf(
						"  <foreign-keycol local-name=\"%s\" foreign-name=\"%s\"/>\n",
						$col->localName,
						$col->foreignName
					);
				}
				unset($col);	// release reference to last element
				$xml .= " </foreign-key>\n";
				break;
			case 'DDLInsert':
				$anythingOutput = true;
				$xml .= sprintf(" <insert table=\"%s\"", $tle->tableName);
				if (!empty($tle->keyColumnNames)) {
					$xml .= sprintf(' key-column-names="%s"', implode(',', $tle->keyColumnNames));
					$xml .= sprintf(' update-if-exists="%s"', $tle->updateIfExists ? 'true' : 'false');
				}
				$xml .= ">\n";
				foreach ($tle->columns as &$col) {
					$xml .= sprintf("  <inscol name=\"%s\"", $col->name);
					if ($col->sysVarValue !== false) {
						$xml .= sprintf(
							" sysvar-value=\"%s\"",
							htmlspecialchars($col->sysVarValue)
						);
					} else {
						$xml .= sprintf(
							" value=\"%s\"",
							htmlspecialchars($col->value)
						);
						if ($col->quoted) {
							$xml .= ' quoted="true"';
						}
					}
					$xml .= "/>\n";
				}
				unset($col);	// release reference to last element
				$xml .= " </insert>\n";
				break;
			}
		}
		unset($tle);	// release reference to last element
		$xml .= "</ddl>\n";
		return $xml;
	}
}

// The SQLDDLSerializer class outputs SQL for a specified SQL dialect, given a valid
// DDL instance.
class SQLDDLSerializer {
	// Array of supported SQL dialects (strings).
	public static $SUPPORTED_DIALECTS = array('mysql', 'pgsql');

	// Serialize a DDL object tree, returning SQL for the requested SQL dialect.
	// Parameters:
	// $ddl: A valid DDL instance.
	// $dialect: The SQL dialect to use.  Must be one of the dialects listed in
	// SQLDDLSerializer::$SUPPORTED_DIALECTS.  Defaults to 'mysql'.
	// Returns an array of strings, where each string is a single SQL statement.
	public function serialize($ddl, $dialect = 'mysql') {
		if (!in_array($dialect, SQLDDLSerializer::$SUPPORTED_DIALECTS)) {
			throw new Exception(sprintf(
				"Requested SQL dialect \"%s\" is not in the list of supported dialects (%s).",
				$dialect,
				implode(', ',  SQLDDLSerializer::$SUPPORTED_DIALECTS)
			));
		}

		$sqlStatements = array();
		foreach ($ddl->topLevelEntities as &$tle) {
			switch (get_class($tle)) {
			case 'DDLTable':
				// Find auto-increment column; create sequence if needed (depends on dialect).
				$autoIncColName = '';
				foreach ($tle->columns as &$col) {
					if ($col->autoIncrement) {
						if ($autoIncColName != '') {
							throw new Exception(sprintf(
								"Cannot have more than one auto-increment column per table".
									" in table name \"%s\"",
								$tle->tableName
							));
						}
						$autoIncColName = $col->name;
					}
				}
				unset($col);	// release reference to last element
				if ($autoIncColName != '') {
					switch ($dialect) {
					case 'pgsql':
						$sqlStatements[] = sprintf("drop sequence if exists %s_autoInc_seq", $tle->tableName);
						$sqlStatements[] = sprintf("create sequence %s_autoInc_seq", $tle->tableName);
						break;
					}
				}

				$sql = sprintf("create table %s (", $tle->tableName);
				$sep = '';
				foreach ($tle->columns as &$col) {
					$sql .= $sep;
					if ($sep == '') $sep = ", ";
					$sql .= ' '.$this->serializeTableColumn($col, $tle->tableName, $dialect);
				}
				unset($col);	// release reference to last element
				if ($tle->primaryKey !== false) {
					$sql .= $sep.' primary key (';
					$sep2 = '';
					foreach ($tle->primaryKey->columns as &$col) {
						$sql .= $sep2.$col->name;
						if ($sep2 == '') $sep2 = ', ';
					}
					unset($col);	// release reference to last element
					$sql .= ")";
				}
				switch ($dialect) {
				case 'mysql':
					$sql .= ") engine=InnoDB";
					break;
				default:
					$sql .= ")";
					break;
				}
				$sqlStatements[] = $sql;
				unset($sql);
				break;
			case 'DDLIndex':
				$sql = sprintf(
					"create%s index %s on %s (",
					$tle->unique ? ' unique' : '',
					$tle->indexName,
					$tle->tableName
				);
				$sep = '';
				foreach ($tle->columns as &$col) {
					$sql .= $sep.$col->name;
					if ($sep == '') $sep = ', ';
				}
				unset($col);	// release reference to last element
				$sql .= ")";
				$sqlStatements[] = $sql;
				unset($sl);
				break;
			case 'DDLForeignKey':
				$localCols = array();
				$foreignCols = array();
				foreach ($tle->columns as $fkcol) {
					$localCols[] = $fkcol->localName;
					$foreignCols[] = $fkcol->foreignName;
				}
				$sqlStatements[] = sprintf(
					"alter table %s add constraint %s foreign key (%s) references %s (%s)",
					$tle->localTableName,
					$tle->foreignKeyName,
					implode(', ', $localCols),
					$tle->foreignTableName,
					implode(', ', $foreignCols)
				);
				break;
			case 'DDLInsert':
				$sqlStatements = array_merge($sqlStatements, $this->serializeInsert($tle, $dialect));
				break;
			}
		}
		unset($tle);	// release reference to last element
		return $sqlStatements;
	}

	public function serializeTableColumn(
		$col,
		$tableName,
		$dialect = 'mysql',
		$includeColumnName = true,
		$includeColumnType = true,
		$includeColumnNull = true,
		$includeColumnDefault = true,
		$includeColumnAutoIncrement = true) {

		$sql = '';
		if ($includeColumnName) {
			$sql .= $col->name.' ';
		}

		if ($includeColumnType) {
			switch ($col->type) {
			case 'integer':
			case 'smallint':
			case 'bigint':
				$sql .= $col->type;
				break;
			case 'decimal':
				$sql .= sprintf('decimal(%d, %d)', $col->size, $col->scale);
				break;
			case 'char':
			case 'varchar':
			case 'binary':
			case 'varbinary':
				switch ($dialect) {
				case 'mysql':
					$sql .= sprintf('%s(%d)', $col->type, $col->size);
					break;
				case 'pgsql':
					if (($col->type == 'binary') || ($col->type == 'varbinary') || ($col->type == 'blob')) {
						$sql .= 'bytea';
					} else {
						$sql .= sprintf('%s(%d)', $col->type, $col->size);
					}
					break;
				}
				break;
			case 'text':
				switch ($dialect) {
				case 'mysql':
					$sql .= 'longtext';
					break;
				case 'pgsql':
					$sql .= 'text';
					break;
				}
				break;
			case 'blob':
				switch ($dialect) {
				case 'mysql':
					$sql .= 'longblob';
					break;
				case 'pgsql':
					$sql .= 'bytea';
					break;
				}
				break;
			case 'date':
			case 'time':
				$sql .= $col->type;
				break;
			case 'datetime':
				switch ($dialect) {
				case 'mysql':
					if ($col->useTimeZone) {
						$sql .= 'timestamp';
					} else {
						$sql .= 'datetime';
					}
					break;
				case 'pgsql':
					if ($col->useTimeZone) {
						$sql .= 'timestamp with time zone';
					} else {
						$sql .= 'timestamp without time zone';
					}
					break;
				}
				break;
			default:
				throw new Exception("Invalid column type: %s", $col->type);
				break;
			}	// switch ($col->type])
		}	// if ($includeColumnType)

		if ($includeColumnNull) {
			if (!$col->allowNull) {
				$sql .= ' not null';
			}
		}

		if ($col->autoIncrement) {
			if ($includeColumnAutoIncrement) {
				switch ($dialect) {
				case 'mysql':
					$sql .= ' auto_increment';
					break;
				case 'pgsql':
					$sql .= " default nextval('".$tableName."_autoInc_seq')";
					break;
				}
			}
		} else if ($col->sysVarDefault !== false) {
			if ($includeColumnDefault) {
				$sql .= " default ".$this->__convertSysVar($col->sysVarDefault, $dialect);
			}
		} else if ($col->defaultValue !== false) {
			if ($includeColumnDefault) {
				switch ($col->type) {
				case 'integer':
				case 'smallint':
				case 'bigint':
					$sql .= sprintf(" default %s", (string)((int)$col->defaultValue));
					break;
				case 'decimal':
					$sql .= sprintf(" default %s", (string)((double)$col->defaultValue));
					break;
				case 'date':
					if ($col->defaultValue == '') {
						// For date types which default to empty, change the default to 0000-00-00 or 0001-01-01, depending on which one the database will accept.
						switch ($dialect) {
						case 'mysql':
							$sql .= " default '0000-00-00'";
							break;
						case 'pgsql':
							$sql .= " default '0001-01-01'";
							break;
						}
					} else {
						$sql .= sprintf(
							" default '%s'",
							$this->__escape($col->defaultValue, $dialect)
						);
					}
					break;
				case 'time':
					if ($col->defaultValue == '') {
						// For time types which default to empty, change the default to 00:00:00.
						switch ($dialect) {
						case 'mysql':
						case 'pgsql':
							$sql .= " default '00:00:00'";
							break;
						}
					} else {
						$sql .= sprintf(
							" default '%s'",
							$this->__escape($col->defaultValue, $dialect)
						);
					}
					break;
				case 'datetime':
					if ($col->defaultValue == '') {
						// For datetime types which default to empty, change the default to 0000-00-00 00:00:00 or 0001-01-01 00:00:00, depending on which one the database will accept.
						switch ($dialect) {
						case 'mysql':
							$sql .= " default '0000-00-00 00:00:00'";
							break;
						case 'pgsql':
							$sql .= " default '0001-01-01 00:00:00'";
							break;
						}
					} else {
						$sql .= sprintf(
							" default '%s'",
							$this->__escape($col->defaultValue, $dialect)
						);
					}
					break;
				default:
					$sql .= sprintf(
						" default '%s'",
						$this->__escape($col->defaultValue, $dialect)
					);
					break;
				}
			}	// if ($includeColumnDefault)
		}
		return $sql;
	}

	public function serializeInsert($insert, $dialect) {
		$sqlStatements = array();

		$sql = sprintf("insert into %s (", $insert->tableName);
		$sep = '';
		foreach ($insert->columns as &$col) {
			$sql .= $sep.$col->name;
			if ($sep == '') $sep = ', ';
		}
		unset($col);	// release reference to last element
		if (!empty($insert->keyColumnNames)) {
			$sql .= ') select ';
		} else {
			$sql .= ') values (';
		}
		$sep = '';
		foreach ($insert->columns as &$col) {
			$sql .= $sep;
			if ($col->sysVarValue !== false) {
				$sql .= $this->__convertSysVar($col->sysVarValue, $dialect);
			} else {
				if ($col->quoted) {
					$sql .= sprintf("'%s'", $this->__escape($col->value, $dialect));
				} else {
					$sql .= $col->value;
				}
			}
			if ($sep == '') $sep = ', ';
		}
		unset($col);	// release reference to last element
		if (!empty($insert->keyColumnNames)) {
			if ($dialect != 'pgsql') {
				$sql .= ' from dual';
			}
			$sql .=
				' where not exists(select '.
				implode(', ', $insert->keyColumnNames).
				' from '.$insert->tableName.
				' where ';
			$sep = '';
			foreach ($insert->keyColumnNames as $kcn) {
				$kcidx = $insert->getColumnIdx($kcn);
				if ($kcidx < 0) continue;
				$col = $insert->columns[$kcidx];
				if (($col->sysVarValue !== false) || ($col->value === false)) continue;
				$sql .= $sep.$kcn.' = ';
				if ($col->quoted) {
					$sql .= sprintf("'%s'", $this->__escape($col->value, $dialect));
				} else {
					$sql .= $col->value;
				}
				if ($sep == '') $sep = ' and ';
			}
			$sql .= ")";
		} else {
			$sql .= ")";
		}
		$sqlStatements[] = $sql;

		if ((!empty($insert->keyColumnNames)) && ($insert->updateIfExists)) {
			$sql = sprintf("update %s set ", $insert->tableName);
			$sep = '';
			foreach ($insert->columns as &$col) {
				$sql .= $sep.$col->name.' = ';
				if ($col->sysVarValue !== false) {
					$sql .= $this->__convertSysVar($col->sysVarValue, $dialect);
				} else {
					if ($col->quoted) {
						$sql .= sprintf("'%s'", $this->__escape($col->value, $dialect));
					} else {
						$sql .= $col->value;
					}
				}
				if ($sep == '') $sep = ', ';
			}
			unset($col);	// release reference to last element
			$sql .= ' where ';
			$sep = '';
			foreach ($insert->keyColumnNames as $kcn) {
				$kcidx = $insert->getColumnIdx($kcn);
				if ($kcidx < 0) continue;
				$col = $insert->columns[$kcidx];
				if (($col->sysVarValue !== false) || ($col->value === false)) continue;
				$sql .= $sep.$kcn.' = ';
				if ($col->quoted) {
					$sql .= sprintf("'%s'", $this->__escape($col->value, $dialect));
				} else {
					$sql .= $col->value;
				}
				if ($sep == '') $sep = ' and ';
			}
			$sqlStatements[] = $sql;
		}

		return $sqlStatements;
	}

	protected function __convertSysVar($sysVar, $dialect) {
		switch (strtolower($sysVar)) {
		case 'current_date':
			switch ($dialect) {
			case 'mysql':
			case 'pgsql':
			default:
				return 'CURRENT_DATE';
				break;
			}
			break;
		case 'current_time':
			switch ($dialect) {
			case 'mysql':
			case 'pgsql':
			default:
				return 'CURRENT_TIME';
				break;
			}
			break;
		case 'current_timestamp':
			switch ($dialect) {
			case 'mysql':
			case 'pgsql':
			default:
				return 'CURRENT_TIMESTAMP';
				break;
			}
			break;
		default:
			throw new Exception(sprintf("Invalid system variable reference \"%s\".", $sysVar));
			break;
		}
	}

	protected function __escape($s, $dialect) {
		switch ($dialect) {
		case 'mysql':
			$s2 = '';
			$len = strlen($s);
			for ($i = 0; $i < $len; $i++) {
				switch ($s[$i]) {
				case "\x00":
				case "\n":
				case "\r":
				case "\\":
				case "'":
				case "\"":
				case "\x1a":
					$s2 .= "\\";
					// no break here.
				default:
					$s2 .= $s[$i];
				}
			}
			$s = $s2;
			unset($s2);
			break;
		case 'pgsql':
			$s = str_replace("'", "''", $s);
			break;
		}
		return $s;
	}
}

class YAMLDDLSerializer {
	// Serialize a DDL object tree, returning YAML.
	// Parameters:
	// $ddl: A valid DDL instance.
	// Returns: The YAML document, as a string.
	public function serialize($ddl) {
		$yaml = "tables:\n";
		$anythingOutput = false;
		foreach ($ddl->topLevelEntities as &$tle) {
			if (get_class($tle) != 'DDLTable') continue;
			if ($anythingOutput) $yaml .= "\n\n";
			$anythingOutput = true;
			$yaml .= "  {$tle->tableName}:\n";
			if (!empty($tle->columns)) {
				$yaml .= "    columns:\n";
				foreach ($tle->columns as &$col) {
					$yaml .= "      {$col->name}: ".'{'." type: {$col->type}";
					if ( ($col->useTimeZone) && ($col->type == 'datetime') ) {
						$yaml .= ', useTimeZone: Yes';
					}
					if (($col->type == 'decimal') ||
						($col->type == 'char') ||
						($col->type == 'varchar') ||
						($col->type == 'binary') ||
						($col->type == 'varbinary')) {
						$yaml .= sprintf(', size: %d', $col->size);
					}
					if ($col->type == 'decimal') {
						$yaml .= sprintf(', scale: %d', $col->scale);
					}
					if (!$col->allowNull) {
						$yaml .= ', null: No';
					}
					if (($col->autoIncrement) &&
						(($col->type == 'integer') ||
					 	($col->type == 'smallint') ||
					 	($col->type == 'bigint'))) {
						$yaml .= ', autoIncrement: Yes';
					} else if ($col->sysVarDefault !== false) {
						$yaml .= sprintf(
							', sysVarDefault: %s',
							$col->sysVarDefault
						);
					} else if ($col->defaultValue !== false) {
/// TODO: Figure out how to quote and properly escape this.
						$yaml .= sprintf(
							', default: "%s"',
							$col->defaultValue
						);
					}
					$yaml .= " }\n";
				}
				unset($col);	// release reference to last element
			}
			if ($tle->primaryKey !== false) {
				$yaml .= "    primaryKey:\n";
				if (empty($tle->primaryKey->columns)) {
					$yaml .= "      columns: ~\n";
				} else {
					$yaml .= "      columns: [";
					$sep = '';
					foreach ($tle->primaryKey->columns as &$col) {
						$yaml .= sprintf("%s %s", $sep, $col->name);
						if ($sep == '') $sep = ',';
					}
					unset($col);	// release reference to last element
					$yaml .= " ]\n";
				}
			}

			// Output indexes for this table.
			$anyIndexesOutput = false;
			foreach ($ddl->topLevelEntities as &$itle) {
				if (get_class($itle) != 'DDLIndex') continue;
				if ($itle->tableName != $tle->tableName) continue;
				if (!$anyIndexesOutput) {
					$anyIndexesOutput = true;
					$yaml .= "    indexes:\n";
				}
				$yaml .= sprintf("      %s:\n", $itle->indexName);
				if ($itle->unique) {
					$yaml .= "        unique: Yes\n";
				}
				if (empty($itle->columns)) {
					$yaml .= "        columns: ~\n";
				} else {
					$yaml .= "        columns: [";
					$sep = '';
					foreach ($itle->columns as &$col) {
						$yaml .= sprintf("%s %s", $sep, $col->name);
						if ($sep == '') $sep = ',';
					}
					unset($col);	// release reference to last element
					$yaml .= " ]\n";
				}
			}
			unset($itle);	// release reference to last element
			if (!$anyIndexesOutput) $yaml .= "    indexes: ~\n";


			// Output foreign keys for this table.
			$anyForeignKeysOutput = false;
			foreach ($ddl->topLevelEntities as &$fktle) {
				if (get_class($fktle) != 'DDLForeignKey') continue;
				if ($fktle->localTableName != $tle->tableName) continue;
				if (!$anyForeignKeysOutput) {
					$anyForeignKeysOutput = true;
					$yaml .= "    foreignKeys:\n";
				}
				$yaml .= sprintf(
					"      %s:\n        foreignTable: %s\n",
					$fktle->foreignKeyName,
					$fktle->foreignTableName
				);
				if (empty($fktle->columns)) {
					$yaml .= "        columns: ~\n";
				} else {
					$yaml .= "        columns:\n";
					foreach ($fktle->columns as &$col) {
						$yaml .= sprintf(
							"          %s: { local: %s, foreign: %s }\n",
							$col->localName,
							$col->localName,
							$col->foreignName
						);
					}
					unset($col);	// release reference to last element
				}
			}
			unset($fktle);	// release reference to last element
			if (!$anyForeignKeysOutput) $yaml .= "    foreignKeys: ~\n";


			// Output inserts for this table.
			$anyInsertsOutput = false;
			$useColNameForInsertIdx = '';
			if ((($tle->primaryKey !== false) && (count($tle->primaryKey->columns) == 1)) &&
				(($cidx = $tle->getColumnIdx($tle->primaryKey->columns[0]->name)) >= 0) &&
				in_array($tle->columns[$cidx]->type, array('integer', 'smallint', 'bigint'))) {
				$useColNameForInsertIdx = $tle->primaryKey->columns[0]->name;
			}
			$insertIdx = 0;
			foreach ($ddl->topLevelEntities as &$itle) {
				if (get_class($itle) != 'DDLInsert') continue;
				if ($itle->tableName != $tle->tableName) continue;
				if (!$anyInsertsOutput) {
					$anyInsertsOutput = true;
					$yaml .= "    inserts:\n";
				}

				// If possible, use the primary key value as the index for this entry;
				// otherwise, use an auto-incrementing value.
				$found = false;
				if ($useColNameForInsertIdx != '') {
					for ($ci = 0; $ci < count($itle->columns); $ci++) {
						if (($itle->columns[$ci]->name == $useColNameForInsertIdx) &&
							isset($itle->columns[$ci]->value) &&
							((!isset($itle->columns[$ci]->sysVarValue) ||
							 ($itle->columns[$ci]->sysVarValue == '')))) {
							$found = true;
							$insertId = (int)$itle->columns[$ci]->value;
							break;
						}
					}
				}
				$insertIdx++;
				if (!$found) {
					$insertId = sprintf('auto_%d', $insertIdx);
				}
				if (empty($itle->columns)) {
					$yaml .= sprintf("      %s: ~\n", $insertId);
				} else {
					$yaml .= sprintf("      %s:\n", $insertId);
					if (!empty($itle->keyColumnNames)) {
						$yaml .= sprintf(
							"        keyColumnNames: [ %s ]\n",
							implode(', ', $tle->keyColumnNames)
						);
						$yaml .= sprintf(
							"        updateIfExists: %s\n",
							$tle->updateIfExists ? 'true' : 'false'
						);
					}
					foreach ($itle->columns as &$col) {
						$yaml .= sprintf("        %s: {", $col->name);
						if ($col->sysVarValue !== false) {
							$yaml .= sprintf(" sysVarValue: %s", $col->sysVarValue);
						} else {
/// TODO: Figure out how to quote and properly escape this.
							$yaml .= sprintf(' value: "%s"', $col->value);
							if ($col->quoted) $yaml .= ', quoted: Yes';
						}
						$yaml .= " }\n";
					}
					unset($col);	// release reference to last element
				}
			}
			unset($itle);	// release reference to last element
		}
		unset($tle);	// release reference to last element
		return $yaml;
	}
}

// The SQLDDLUpdater class outputs DDL update SQL for a specified SQL dialect,
// given a pair of valid DDL instances.  The outputted SQL will update the
// tables and indexes (DDL) from one DDL to another.
class SQLDDLUpdater {
	// Array of supported SQL dialects (strings).
	public static $SUPPORTED_DIALECTS = array('mysql', 'pgsql');

	// Given two DDL object trees (one old and one new), generate SQL in the selected
	// database dialect to update the schema from the old to the new DDL structure.
	// Parameters:
	// $oldDDL: A valid DDL instance which represents the current (old) state of
	// the database schema. Typically this would be retrieved from a database
	// server which needs to be updated to reflect a new schema.
	// $newDDL: A valid DDL instance which represents the desired final (new) state
	// of the database schema.
	// $allowDropTable: true to allow dropping of tables which are in $oldDDL but
	// not in $newDDL; false to not drop them.  Defaults to false.
	// $allowDropColumn: true to allow dropping of columns which are in $oldDDL
	// but not in $newDDL; false to not drop them.  Defaults to false.
	// $allowDropIndex: true to allow dropping of indexes and foreign keys which are in $oldDDL
	// but not in $newDDL; false to not drop them.  Defaults to false.
	// $dialect: The SQL dialect to use.  Must be one of the dialects listed in
	// SQLDDLUpdater::$SUPPORTED_DIALECTS.  Defaults to 'mysql'.
	// Returns the SQL statements required to transform the schema from $oldDDL
	// to match $newDDL, as an array of strings, where each string is a single SQL statement.
	public function generateSQLUpdates(
		$oldDDL,
		$newDDL,
		$allowDropTable = false,
		$allowDropColumn = false,
		$allowDropIndex = false,
		$dialect = 'mysql') {

		if (!in_array($dialect, SQLDDLUpdater::$SUPPORTED_DIALECTS)) {
			throw new Exception(sprintf(
				"Requested SQL dialect \"%s\" is not in the list of supported dialects (%s).",
				$dialect,
				implode(', ',  SQLDDLUpdater::$SUPPORTED_DIALECTS)
			));
		}

		$serializer = new SQLDDLSerializer();

		$sqlStatements = array();

		// Determine which table names are common among all tables.
		$commonTableNames = $oldDDL->getCommonTableNames($newDDL);
		$oldTableNames = $oldDDL->getAllTableNames();
		$newTableNames = $newDDL->getAllTableNames();

		$idxsDroppedByTableName = array();
		$fksDroppedByTableName = array();

		if ($allowDropIndex) {
			// For tables which exist in both old and new schemas, drop all indexes and
			// foreign keys which exist in $oldDDL but don't exist in $newDDL,
			// or which exist in both but are different between the two.
			foreach ($commonTableNames as $tableName) {
				$tmp = $oldDDL->getTableIndexesAndForeignKeys($tableName);
				$oidxs = $tmp->idxs;
				$ofks = $tmp->fks;
				unset($tmp);

				$tmp = $newDDL->getTableIndexesAndForeignKeys($tableName);
				$nidxs = $tmp->idxs;
				$nfks = $tmp->fks;
				unset($tmp);

				// Indexes
				foreach (array_keys($oidxs) as $indexName) {
					if ((!isset($nidxs[$indexName])) ||
						($oidxs[$indexName] != $nidxs[$indexName])) {
						switch ($dialect) {
						case 'mysql':
							// MySQL also creates an index with the same name as the foreign key.
							// This means we only want to drop the index if it's not part of
							// a foreign key in the new schema, or if it's part of a foreign key
							// which does not match between the old and new schemas.
							if ((!isset($nfks[$indexName])) ||
								(isset($ofks[$indexName]) && ($ofks[$indexName] != $nfks[$indexName]))) {
								$sqlStatements[] = "drop index $indexName on $tableName";
							}
							break;
						case 'pgsql':
							$sqlStatements[] = "drop index $indexName";
							break;
						}
						if (!isset($idxsDroppedByTableName[$tableName])) {
							$idxsDroppedByTableName[$tableName] = array();
						}
						if (!in_array($indexName, $idxsDroppedByTableName[$tableName])) {
							$idxsDroppedByTableName[$tableName][] = $indexName;
						}
					}
				}

				// Foreign keys
				foreach (array_keys($ofks) as $foreignKeyName) {
					if ((!isset($nfks[$foreignKeyName])) ||
						($ofks[$foreignKeyName] != $nfks[$foreignKeyName])) {
						switch ($dialect) {
						case 'mysql':
							$sqlStatements[] = "alter table $tableName drop foreign key $foreignKeyName";
							$sqlStatements[] = "alter table $tableName drop index $foreignKeyName";
							break;
						case 'pgsql':
							$sqlStatements[] = "alter table $tableName drop constraint $foreignKeyName";
							break;
						}
						if (!isset($fksDroppedByTableName[$tableName])) {
							$fksDroppedByTableName[$tableName] = array();
						}
						if (!in_array($foreignKeyName, $fksDroppedByTableName[$tableName])) {
							$fksDroppedByTableName[$tableName][] = $foreignKeyName;
						}
					}
				}
			}
		}	// if ($allowDropIndex)

		if ($allowDropTable) {
			// Drop all tables which exist in $oldDDL but don't exist in $newDDL.
			foreach ($oldTableNames as $tn) {
				if (!in_array($tn, $newTableNames)) {
					$sqlStatements[] = 'drop table '.$tn;
					if ($dialect == 'pgsql') {
						$sqlStatements[] = sprintf("drop sequence if exists %s_autoInc_seq", $tn);
					}
				}
			}
		}	// if ($allowDropTable)

		// Create all tables and their indexes, which exist in $newDDL but don't exist in $oldDDL.
		// Perform all inserts for newly created tables.
		foreach ($newDDL->topLevelEntities as $ntle) {
			if ((get_class($ntle) == 'DDLTable') &&
				(!in_array($ntle->tableName, $commonTableNames))) {
				$tles = array($ntle);
				for ($i = 0, $ni = count($newDDL->topLevelEntities); $i < $ni; $i++) {
					$tle = $newDDL->topLevelEntities[$i];
					if (((get_class($tle) == 'DDLIndex') || (get_class($tle) == 'DDLInsert')) &&
						($tle->tableName == $ntle->tableName)) {
						$tles[] = $tle;
						continue;
					}
				}
				$tddl = new DDL($tles);
				$sqlStatements = array_merge($sqlStatements, $serializer->serialize($tddl, $dialect));
			}
		}	// foreach ($newDDL->topLevelEntities as $ntle)

		// Process each common table name.
		foreach ($commonTableNames as $tableName) {
			// Find the old table definition, and all indexes and foreign keys for it.
			$tmp = $oldDDL->getTableIndexesAndForeignKeys($tableName);
			$otbl = $tmp->tbl;
			$oidxs = $tmp->idxs;
			$ofks = $tmp->fks;
			unset($tmp);

			// Find the new table definition, and all indexes and foreign keys for it.
			$tmp = $newDDL->getTableIndexesAndForeignKeys($tableName);
			$ntbl = $tmp->tbl;
			$nidxs = $tmp->idxs;
			$nfks = $tmp->fks;
			unset($tmp);

			if ($allowDropColumn) {
				// Drop columns which exist in the old table but not the new.
				$ncols = array();
				foreach ($ntbl->columns as $ncol) $ncols[$ncol->name] = $ncol;
				for ($oci = 0, $onc = count($otbl->columns); $oci < $onc; $oci++) {
					$ocol = $otbl->columns[$oci];
					if (!isset($ncols[$ocol->name])) {
						switch ($dialect) {
						case 'mysql':
							$sqlStatements[] = "alter table $tableName drop column ".$ocol->name;
							break;
						case 'pgsql':
							$sqlStatements[] = "alter table $tableName drop column ".$ocol->name." cascade";
							break;
						}
					}
				}
			}

			// Add columns which exist in the new table but not the old.
			// Update columns which exist in both tables.
			$ocols = array();
			foreach ($otbl->columns as $ocol) $ocols[$ocol->name] = $ocol;
			for ($nci = 0, $nnc = count($ntbl->columns); $nci < $nnc; $nci++) {
				$ncol = $ntbl->columns[$nci];
				if (!isset($ocols[$ncol->name])) {
					// Column exists in new but not old.  Add the column to the table.
					switch ($dialect) {
					case 'mysql':
					case 'pgsql':
						$sqlStatements[] =
							"alter table $tableName add column ".
							$serializer->serializeTableColumn($ncol, $tableName, $dialect).
							(($nci > 0) ? (' after '.$ntbl->columns[$nci-1]->name) : ' first');
						break;
					}
				} else if ($ncol != $ocols[$ncol->name]) {
					// Column exists in both new and old, but is different.  Alter the column.
					$ocol = $ocols[$ncol->name];
					switch ($dialect) {
					case 'mysql':
						$sqlStatements[] =
							"alter table $tableName change column {$ncol->name} ".
							$serializer->serializeTableColumn($ncol, $tableName, $dialect).
							(($nci > 0) ? (' after '.$ntbl->columns[$nci-1]->name) : ' first');
						break;
					case 'pgsql':
						if (($ocol->type != $ncol->type) ||
							(($ncol->type == 'datetime') &&
							 ($ocol->useTimeZone != $ncol->useTimeZone)) ||
							($ocol->size != $ncol->size) ||
							($ocol->scale != $ncol->scale)) {

							// The only binary type PostgreSQL supports is bytea, and it does
							// not have a size parameter.  Therefore, binary, varbinary and
							// blob types all map to bytea in PostgreSQL.  If both the new and
							// old columns are any of these types, then we don't need to alter
							// the column type.
							if ((($ocol->type == 'binary') || ($ocol->type == 'varbinary') || ($ocol->type == 'blob')) &&
								(($ncol->type == 'binary') || ($ncol->type == 'varbinary') || ($ncol->type == 'blob'))) {
								// Nothing to do here.
							} else {
								$sqlStatements[] =
									"alter table $tableName alter column {$ncol->name} type ".
									$serializer->serializeTableColumn(
										$ncol,
										$tableName,
										$dialect,
										false,
										true,
										false,
										false,
										false);
							}
						}
						if ($ocol->allowNull != $ncol->allowNull) {
							if ($ncol->allowNull) {
								$sqlStatements[] = "alter table $tableName alter column {$ncol->name} drop not null";
							} else {
								$sqlStatements[] = "alter table $tableName alter column {$ncol->name} set not null";
							}
						}
						if (($ocol->defaultValue != $ncol->defaultValue) ||
							($ocol->sysVarDefault != $ncol->sysVarDefault) ||
							($ocol->autoIncrement != $ncol->autoIncrement)) {
							$s = $serializer->serializeTableColumn(
								$ncol,
								$tableName,
								$dialect,
								false,
								false,
								false,
								true,
								true);
							if ($s != '') {
								$sqlStatements[] = "alter table $tableName alter column {$ncol->name} set ".ltrim($s, ' ');		// $s includes the "default" keyword.
							} else {
								$sqlStatements[] = "alter table $tableName alter column {$ncol->name} drop default";
							}
						}
						break;
					}
				}
			}	// for ($nci = 0, $nnc = count($ntbl->columns); $nci < $nnc; $nci++)

			// Add, drop or update the primary key.
			if (($otbl->primaryKey === false) && ($ntbl->primaryKey !== false)) {
				// Add the primary key.
				$cns = array();
				foreach ($ntbl->primaryKey->columns as $cl) $cns[] = $cl->name;
				switch ($dialect) {
				case 'mysql':
				case 'pgsql':
					$sqlStatements[] = "alter table $tableName add primary key (".implode(', ', $cns).")";
					break;
				}
			} else if (($otbl->primaryKey !== false) && ($ntbl->primaryKey === false)) {
				// Drop the primary key.
				switch ($dialect) {
				case 'mysql':
					$sqlStatements[] = "alter table $tableName drop primary key";
					break;
				case 'pgsql':
					$sqlStatements[] = "alter table $tableName drop constraint ${tableName}_pkey";
					break;
				}
			} else if (($otbl->primaryKey !== false) &&
						($ntbl->primaryKey !== false) &&
						($otbl->primaryKey != $ntbl->primaryKey)) {
				// Drop and re-add the primary key.
				$cns = array();
				foreach ($ntbl->primaryKey->columns as $cl) $cns[] = $cl->name;
				switch ($dialect) {
				case 'mysql':
					$sqlStatements[] = "alter table $tableName drop primary key";
					$sqlStatements[] = "alter table $tableName add primary key (".implode(', ', $cns).")";
					break;
				case 'pgsql':
					$sqlStatements[] = "alter table $tableName drop constraint ${tableName}_pkey";
					$sqlStatements[] = "alter table $tableName add primary key (".implode(', ', $cns).")";
					break;
				}
			}

			// Create any indexes which are missing or different between the old and new.
			// Drop and re-create any indexes which don't match.
			foreach (array_keys($nidxs) as $indexName) {
				if ((!isset($oidxs[$indexName])) || ($oidxs[$indexName] != $nidxs[$indexName])) {
					if (isset($oidxs[$indexName])) {
						// Drop the index (will re-create it below).
						switch ($dialect) {
						case 'mysql':
							$sqlStatements[] = "drop index $indexName on $tableName";
							break;
						case 'pgsql':
							$sqlStatements[] = "drop index $indexName";
							break;
						}
					}
					// (Re-)create the index.
					$tddl = new DDL(array($nidxs[$indexName]));
					$sqlStatements = array_merge($sqlStatements, $serializer->serialize($tddl, $dialect));
				}
			}
		}	// foreach ($commonTableNames as $tableName)

		// For all tables which exist in $newDDL but don't exist in $oldDDL,
		// create their foriegn keys.
		foreach ($newDDL->topLevelEntities as $ntle) {
			if ((get_class($ntle) == 'DDLTable') &&
				(!in_array($ntle->tableName, $commonTableNames))) {
				$tles = array();
				for ($i = 0, $ni = count($newDDL->topLevelEntities); $i < $ni; $i++) {
					$tle = $newDDL->topLevelEntities[$i];
					if ((get_class($tle) == 'DDLForeignKey') &&
						($tle->localTableName == $ntle->tableName)) {
						$tles[] = $tle;
						continue;
					}
				}
				if (!empty($tles)) {
					$tddl = new DDL($tles);
					$sqlStatements = array_merge($sqlStatements, $serializer->serialize($tddl, $dialect));
				}
			}
		}

		// Create any foreign keys which are missing or different between the old and new.
		// Drop and re-create any foreign keys which don't match.
		foreach ($commonTableNames as $tableName) {
			// Find the old table definition, and all foreign keys for it.
			$tmp = $oldDDL->getTableIndexesAndForeignKeys($tableName);
			$ofks = $tmp->fks;
			unset($tmp);

			// Find the new table definition, and all indexes and foreign keys for it.
			$tmp = $newDDL->getTableIndexesAndForeignKeys($tableName);
			$nfks = $tmp->fks;
			unset($tmp);

			foreach (array_keys($nfks) as $foreignKeyName) {
				if ((!isset($ofks[$foreignKeyName])) ||
					($ofks[$foreignKeyName] != $nfks[$foreignKeyName])) {
					if (isset($ofks[$foreignKeyName])) {
						// Drop the foreign key (will re-create it below).
						if (!in_array($foreignKeyName, $fksDroppedByTableName[$tableName])) {
							switch ($dialect) {
							case 'mysql':
								$sqlStatements[] = "alter table $tableName drop foreign key $foreignKeyName";
								$sqlStatements[] = "alter table $tableName drop index $foreignKeyName";
								break;
							case 'pgsql':
								$sqlStatements[] = "alter table $tableName drop constraint $foreignKeyName";
								break;
							}
						}
					}
					// (Re-)create the foreign key.
					$tddl = new DDL(array($nfks[$foreignKeyName]));
					$sqlStatements = array_merge($sqlStatements, $serializer->serialize($tddl, $dialect));
				}
			}
		}

		return $sqlStatements;
	}

	private static function __indexNameSortComparator($a, $b) {
		return strcmp($a->indexName, $b->indexName);
	}
}

class DAOClassGenerator {
	public static $GENERATED_HEADER =
		"// Generated automatically by phpdaogen.\n// Do NOT edit this file.\n// Any changes made to this file will be overwritten the next time it is generated.\n\n";
	public static $STUB_DATA_HEADER =
		"// This file can be edited (within reason) to extend the functionality\n// of the generated (abstract) data class.\n\n";
	public static $STUB_DAO_HEADER =
		"// This file can be edited (within reason) to extend the functionality\n// of the generated (abstract) DAO class.\n\n";


	// Returns PHP code for the data class.
	// Returns false if the table was not found in the DDL.
	public function generateDataClass($ddl, $tableName, $isAbstract = false) {
		if (($tableIdx = $ddl->getTableIdxInTopLevelEntities($tableName)) === false) {
			return false;
		}
		$table = &$ddl->topLevelEntities[$tableIdx];

		$concreteTableClassName = ucfirst($tableName);
		$tableClassName = $concreteTableClassName.($isAbstract ? 'Abstract' : '');

		// Open the class.
		$code = "<?php\n";
		$code .= DAOClassGenerator::$GENERATED_HEADER;
		$code .= ($isAbstract ? 'abstract ' : '')."class $tableClassName {\n";

		// Emit member variables.
		foreach ($table->columns as $column) {
			$code .= "\tpublic \${$column->name};\n";
		}
		$code .= "\n";

		// createDefault static factory function
		$code .= "\tpublic static function createDefault() {\n";
		$code .= "\t\t\$v = new $concreteTableClassName();\n";
		$code .= "\t\t\$v->defaultAllFields();\n";
		$code .= "\t\treturn \$v;\n";
		$code .= "\t}\n\n";


		// defaultAllFields function
		$code .= "\tpublic function defaultAllFields() {\n";
		foreach ($table->columns as $column) {
			$code .= "\t\t".'$this->'.$column->name.' = '.
				$this->getPHPEncodedDefaultValue($column).";\n";
		}
		$code .= "\t\treturn \$this;\n";
		$code .= "\t}\n\n";

		// loadFromArray function
		$code .= "\tpublic function loadFromArray(\$arr) {\n";
		foreach ($table->columns as $column) {
			$phpDataType = $this->getPHPDataType($column);
			$code .= "\t\t".'$this->'.$column->name.' = isset($arr[\''.$column->name.'\']) ? ('.
				$phpDataType.')$arr[\''.$column->name.'\'] : '.
				$this->getPHPEncodedDefaultValue($column).';'."\n";
		}
		$code .= "\t\treturn \$this;\n";
		$code .= "\t}\n";

		// Close the class.
		$code .= "}\n";

		unset($table);

		return $code;
	}

	public function generateStubDataClass($ddl, $tableName) {
		if (($tableIdx = $ddl->getTableIdxInTopLevelEntities($tableName)) === false) {
			return false;
		}
		$table = &$ddl->topLevelEntities[$tableIdx];

		$tableClassName = ucfirst($tableName);

		// Open the class.
		$code = "<?php\n";
		$code .= DAOClassGenerator::$STUB_DATA_HEADER;
		$code .= "include dirname(__FILE__).'/abstract/{$tableClassName}Abstract.class.php';\n";
		$code .= "class $tableClassName extends {$tableClassName}Abstract {\n";
		$code .= "}\n";
		return $code;
	}

	// Returns PHP code for the DAO class.
	// Returns false if the table was not found in the DDL.
	public function generateDAOClass($ddl, $tableName, $isAbstract = false) {
		if (($tableIdx = $ddl->getTableIdxInTopLevelEntities($tableName)) === false) {
			return false;
		}
		$table = &$ddl->topLevelEntities[$tableIdx];

		$concreteTableClassName = ucfirst($tableName);
		$tableClassName = $concreteTableClassName.($isAbstract ? 'Abstract' : '');
		$daoClassName = $concreteTableClassName.($isAbstract ? 'DAOAbstract' : 'DAO');

		// Open the class.
		$code = "<?php\n";
		$code .= DAOClassGenerator::$GENERATED_HEADER;

		// Include data class.
		if ($isAbstract) {
			$code .= "if (!class_exists('$concreteTableClassName', false)) include dirname(dirname(__FILE__)).'/$concreteTableClassName.class.php';\n\n";
		} else {
			$code .= "if (!class_exists('$concreteTableClassName', false)) include dirname(__FILE__).'/$concreteTableClassName.class.php';\n\n";
		}

		$code .= ($isAbstract ? 'abstract ' : '')."class $daoClassName {\n";
		$code .= "\tpublic static \$ALLOWED_QUERY_OPERATORS = array('=', '<>', '<', '<=', '>', '>=', 'beginsWith', 'contains', 'endsWith');\n";
		$code .= "\tpublic static \$ALLOWED_NUMERIC_QUERY_OPERATORS = array('=', '<>', '<', '<=', '>', '>=');\n";
		$code .= "\tpublic static \$ALLOWED_STRING_QUERY_OPERATORS = array('=', '<>', '<', '<=', '>', '>=', 'beginsWith', 'contains', 'endsWith');\n";
		$code .= "\tpublic static \$ALLOWED_BINARY_QUERY_OPERATORS = array('=', '<>');\n";

		// Emit member variables.
		$code .= "\tprotected \$connection;\n";
		$code .= "\tprotected \$cache = null;\n\n";

		$code .= "\tpublic function __construct(".'$connection, $cache = null'.") {\n";
		$code .= "\t\t".'$this->connection = $connection;'."\n";
		$code .= "\t\t".'$this->cache = $cache;'."\n";
		$code .= "\t}\n\n";

		$code .= "\tpublic function getCache() {\n";
		$code .= "\t\t".'return $this->cache;'."\n";
		$code .= "\t}\n\n";

		$code .= "\tpublic function setCache(".'$cache'.") {\n";
		$code .= "\t\t".'$this->cache = $cache;'."\n";
		$code .= "\t}\n\n";

		// insert function
		$code .= "\tpublic function insert(&\$$tableName) {\n";
		$code .= "\t\t".'$ps = new PreparedStatement("insert into '.$tableName.' (';
		$sep = '';
		foreach ($table->columns as $column) {
			if (!$column->autoIncrement) {
				$code .= $sep.$column->name;
				if ($sep == '') $sep = ', ';
			}
		}
		$code .= ') values (';
		$sep = '';
		foreach ($table->columns as $column) {
			if (!$column->autoIncrement) {
				$code .= $sep.'?';
				if ($sep == '') $sep = ', ';
			}
		}
		$code .= ")\");\n";
		foreach ($table->columns as $column) {
			if (!$column->autoIncrement) {
				$code .= "\t\t".'$ps->set'.ucwords($this->getPSDataType($column)).'($'.$tableName.'->'.$column->name.");\n";
			}
		}
		$code .= "\t\t".'$result = $this->connection->executeUpdate($ps);'."\n";
		foreach ($table->columns as $column) {
			if ($column->autoIncrement) {
				$code .= "\t\t".'$'.$tableName.'->'.$column->name.' = $this->connection->getLastInsertId();'."\n";
			}
		}
		$code .= "\t\treturn \$result;\n";
		$code .= "\t}\n\n";

		// update function
		$code .= "\tpublic function update(\$$tableName) {\n";

		$anyColumnsNotInPrimaryKey = false;
		foreach ($table->columns as $column) {
			if (($table->primaryKey === false) ||
				($table->primaryKey->getColumnIdx($column->name) < 0)) {
				$anyColumnsNotInPrimaryKey = true;
				break;
			}
		}

		if ($anyColumnsNotInPrimaryKey) {
			$code .= "\t\t".'$ps = new PreparedStatement("update '.$tableName.' set ';
			$sep = '';
			foreach ($table->columns as $column) {
				if (($table->primaryKey === false) ||
					($table->primaryKey->getColumnIdx($column->name) < 0)) {
					$code .= $sep.$column->name.' = ?';
					if ($sep == '') $sep = ', ';
				}
			}
			if ($table->primaryKey !== false) {
				$whereAnd = ' where ';
				foreach ($table->primaryKey->columns as $pkcol) {
					$column = $table->columns[$table->getColumnIdx($pkcol->name)];
					$code .= $whereAnd.$column->name.' = ?';
					$whereAnd = ' and ';
				}
			}
			$code .= "\");\n";
			foreach ($table->columns as $column) {
				if (($table->primaryKey === false) ||
					($table->primaryKey->getColumnIdx($column->name) < 0)) {
					$code .= "\t\t".'$ps->set'.ucwords($this->getPSDataType($column)).'($'.$tableName.'->'.$column->name.");\n";
				}
			}
			if ($table->primaryKey !== false) {
				foreach ($table->primaryKey->columns as $pkcol) {
					$column = $table->columns[$table->getColumnIdx($pkcol->name)];
					$code .= "\t\t".'$ps->set'.ucwords($this->getPSDataType($column)).'($'.$tableName.'->'.$column->name.");\n";
				}
			}
			$code .= "\t\t".'return $this->connection->executeUpdate($ps);'."\n";
		} else {	// if ($anyColumnsNotInPrimaryKey)
			$code .= "\t\treturn true;\n";
		}	// if ($anyColumnsNotInPrimaryKey) ... else

		$code .= "\t}\n\n";

		// delete function
		$code .= "\tpublic function delete(";
		$sep = '';
		if ($table->primaryKey !== false) {
			foreach ($table->primaryKey->columns as $pkcol) {
				$code .= $sep.'$'.$pkcol->name;
				if ($sep == '') $sep = ', ';
			}
		}
		$code .= ") {\n";
		if ($table->primaryKey !== false) {
			$code .= "\t\t".'$ps = new PreparedStatement("delete from '.$tableName;
			$whereAnd = ' where ';
			foreach ($table->primaryKey->columns as $pkcol) {
				$code .= $whereAnd.$pkcol->name.' = ?';
				$whereAnd = ' and ';
			}
			$code .= "\");\n";
			foreach ($table->primaryKey->columns as $pkcol) {
				$column = $table->columns[$table->getColumnIdx($pkcol->name)];
				$code .= "\t\t".'$ps->set'.ucwords($this->getPSDataType($column)).'($'.$column->name.");\n";
			}
			$code .= "\t\t".'return $this->connection->executeUpdate($ps);'."\n";
		} else {
			$code .= "\t\treturn true;\n";
		}
		$code .= "\t}\n\n";

		// load function
		$code .= "\tpublic function load(";
		$sep = '';
		if ($table->primaryKey !== false) {
			foreach ($table->primaryKey->columns as $pkcol) {
				$code .= $sep.'$'.$pkcol->name;
				if ($sep == '') $sep = ', ';
			}
		}
		$code .= ") {\n";
		if ($table->primaryKey !== false) {
			$code .= "\t\t".'$ps = new PreparedStatement("select * from '.$tableName;
			$whereAnd = ' where ';
			foreach ($table->primaryKey->columns as $pkcol) {
				$code .= $whereAnd.$pkcol->name.' = ?';
				$whereAnd = ' and ';
			}
			$code .= "\", 0, 1);\n";
			foreach ($table->primaryKey->columns as $pkcol) {
				$column = $table->columns[$table->getColumnIdx($pkcol->name)];
				$code .= "\t\t".'$ps->set'.ucwords($this->getPSDataType($column)).'($'.$pkcol->name.");\n";
			}
		}
		$code .= "\t\t".'$rows = $this->findWithPreparedStatement($ps);'."\n";
		$code .= "\t\t".'if (count($rows) > 0) return $rows[0];'."\n";
		$code .= "\t\treturn false;\n";
		$code .= "\t}\n\n";


		// finder functions for each column
		for ($finderi = 0; $finderi < count($table->columns); $finderi++) {
			$finderColumn = $table->columns[$finderi];
			$phpDataType = $this->getPHPDataType($finderColumn);
			$psDataType = $this->getPSDataType($finderColumn);

			// findBy<name>PS function.
			$code .= "\tpublic function findBy".ucwords($finderColumn->name).
				'PS($'.$finderColumn->name.", \$queryOperator = '=', \$orderBy = null, \$offset = 0, \$limit = 0) {\n";
			if ($psDataType == 'binary') {
				$oplist = 'self::$ALLOWED_BINARY_QUERY_OPERATORS';
				$sqlqoname = '$queryOperator';
			} else if ($phpDataType == 'string') {
				$oplist = 'self::$ALLOWED_STRING_QUERY_OPERATORS';
				$sqlqoname = '$sqlQueryOperator';
			} else {
				$oplist = 'self::$ALLOWED_NUMERIC_QUERY_OPERATORS';
				$sqlqoname = '$queryOperator';
			}
			$code .= "\t\tif (!in_array(\$queryOperator, $oplist)) \$queryOperator = $oplist"."[0];\n";
			if ($psDataType == 'string') {
				$code .=
					"\t\tif ((\$queryOperator == 'beginsWith') || (\$queryOperator == 'endsWith') || (\$queryOperator == 'contains')) {\n".
					"\t\t\t\$sqlQueryOperator = \$this->connection->likeOperator;\n".
					"\t\t\t\$needLower = (!\$this->connection->hasCaseInsensitiveLike);\n".
					"\t\t} else {\n".
					"\t\t\t\$sqlQueryOperator = \$queryOperator;\n".
					"\t\t\t\$needLower = false;\n".
					"\t\t}\n";
			}

			$code .= "\t\t\$ps = new PreparedStatement(\"select * from $tableName where ";
			if ($psDataType == 'string') {
				$code .=
					'".($needLower ? \'lower('.$finderColumn->name.')\' : \''.$finderColumn->name.'\')'.
					".' '.$sqlqoname.' '.".
					"(\$needLower ? 'lower(?)' : '?').";
			} else {
				$code .= $finderColumn->name.' '.$sqlqoname.' ?".';
			}
			$code .=
				"(((\$orderBy!==null)&&(\$orderBy!='')) ? (' order by '.\$orderBy) : ''), ".
				"\$offset, \$limit);\n";
			if ($psDataType == 'string') {
				$code .= "\t\tif (\$queryOperator == 'beginsWith') {\n";
				$code .= "\t\t\t".'$ps->set'.ucwords($psDataType).'($'.$finderColumn->name.".'%');\n";
				$code .= "\t\t} else if (\$queryOperator == 'endsWith') {\n";
				$code .= "\t\t\t".'$ps->set'.ucwords($psDataType).'(\'%\'.$'.$finderColumn->name.");\n";
				$code .= "\t\t} else if (\$queryOperator == 'contains') {\n";
				$code .= "\t\t\t".'$ps->set'.ucwords($psDataType).'(\'%\'.$'.$finderColumn->name.".'%');\n";
				$code .= "\t\t} else {\n";
				$code .= "\t\t\t".'$ps->set'.ucwords($psDataType).'($'.$finderColumn->name.");\n";
				$code .= "\t\t}\n";
			} else {
				$code .= "\t\t".'$ps->set'.ucwords($psDataType).'($'.$finderColumn->name.");\n";
			}
			$code .= "\t\treturn \$ps;\n";
			$code .= "\t}\n\n";

			// findBy<name> function.
			$code .= "\tpublic function findBy".ucwords($finderColumn->name).
				'($'.$finderColumn->name.", \$queryOperator = '=', \$orderBy = null, \$offset = 0, \$limit = 0) {\n";
			$code .= "\t\treturn \$this->findWithPreparedStatement(\$this->findBy".
				ucwords($finderColumn->name).'PS($'.$finderColumn->name.
				", \$queryOperator, \$orderBy, \$offset, \$limit));\n";
			$code .= "\t}\n\n";
		}

		// findAllPS function.
		$code .= "\tpublic function findAllPS".
			"(\$orderBy = null, \$offset = 0, \$limit = 0) {\n";
		$code .= "\t\t".'$ps = new PreparedStatement("select * from '.
			$tableName.
			"\".(((\$orderBy!==null)&&(\$orderBy!='')) ? (' order by '.\$orderBy) : ''), ".
			"\$offset, \$limit);\n";
		$code .= "\t\treturn \$ps;\n";
		$code .= "\t}\n\n";

		// findAll function.
		$code .= "\tpublic function findAll".
			"(\$orderBy = null, \$offset = 0, \$limit = 0) {\n";
			$code .= "\t\treturn \$this->findWithPreparedStatement(\$this->findAllPS".
				"(\$orderBy, \$offset, \$limit));\n";
			$code .= "\t}\n\n";

		$code .= "\tpublic function findWithPreparedStatement(\$ps) {\n";
		$code .= "\t\t\$cacheKey = null;\n";
		$code .= "\t\tif (\$this->cache !== null) {\n";
		$code .= "\t\t\t\$cacheKey = \$ps->toSQL(\$this->connection);\n";
		$code .= "\t\t\tif ((\$rows = \$this->cache->get(\$cacheKey)) !== false) {\n";
		$code .= "\t\t\t\treturn \$rows;\n";
		$code .= "\t\t\t}\n";
		$code .= "\t\t}\n";
		$code .= "\t\t\$rows = array();\n";
		$code .= "\t\t\$rs = \$this->connection->executeQuery(\$ps);\n";
		$code .= "\t\twhile (\$arr = \$this->connection->fetchArray(\$rs)) {\n";
		$code .= "\t\t\t\$row = new $concreteTableClassName();\n";
		$code .= "\t\t\t\$row->loadFromArray(\$arr);\n";
		$code .= "\t\t\t\$rows[] = \$row;\n";
		$code .= "\t\t}\n";
		$code .= "\t\t\$this->connection->freeResult(\$rs);\n";
		$code .= "\t\tif (\$this->cache !== null) {\n";
		$code .= "\t\t\t\$this->cache->set(\$cacheKey, \$rows);\n";
		$code .= "\t\t}\n";
		$code .= "\t\treturn \$rows;\n";
		$code .= "\t}\n";

		// Close the class.
		$code .= "}\n";

		unset($table);

		return $code;
	}

	public function generateStubDAOClass($ddl, $tableName, $isAbstract = false) {
		if (($tableIdx = $ddl->getTableIdxInTopLevelEntities($tableName)) === false) {
			return false;
		}
		$table = &$ddl->topLevelEntities[$tableIdx];

		$tableClassName = ucfirst($tableName);
		$daoClassName = $tableClassName.'DAO';

		// Open the class.
		$code = "<?php\n";
		$code .= DAOClassGenerator::$STUB_DAO_HEADER;
		$code .= "include dirname(__FILE__).'/abstract/{$daoClassName}Abstract.class.php';\n";
		$code .= "class $daoClassName extends {$daoClassName}Abstract {\n";
		$code .= "}\n";
		return $code;
	}

	// $column must be a DDLTableColumn instance.
	public function getPHPEncodedDefaultValue(&$column) {
		switch ($column->sysVarDefault) {
		case 'CURRENT_TIMESTAMP':
			return "date('Y-m-d H:i:s')";
			break;
		}
		if ( ($column->allowNull) && ($column->defaultValue === false) ) {
			return 'null';
		}
		if (($column->type == 'binary') ||
			($column->type == 'varbinary') ||
			($column->type == 'blob')) {
			return '\'\'';
		}
		if (($column->type == 'integer') ||
			($column->type == 'smallint') ||
			($column->type == 'bigint')) {
			$val = $column->defaultValue;
			if ($val == '') $val = '0';
			return $val;
		}
		if ($column->type == 'decimal') {
			$val = $column->defaultValue;
			if ($val == '') {
				$val = '0';
				if ($column->scale > 0) {
					$val .= '.';
					for ($i = 0; $i < $column->scale; $i++) $val .= '0';
				}
			} else {
				$dotidx = strrpos($val, '.');
				while (strlen($val) < ($dotidx+$column->scale+1)) $val .= '0';
			}
			return $val;
		}
		$val = $column->defaultValue;
		if ($val == '') {
			switch ($column->type) {
			case 'date': $val = '0001-01-01'; break;
			case 'datetime': $val = '0001-01-01 00:00:00'; break;
			case 'time': $val = '00:00:00'; break;
			}
		}
		return '"'.$this->phpencode($val).'"';
	}

	// $column must be a DDLTableColumn instance.
	public function getPHPDataType(&$column) {
		$dataType = '';
		switch ($column->type) {
		case 'integer':
		case 'smallint':
		case 'bigint':
			return 'int';
		case 'decimal':
			return 'double';
		default:
			return 'string';
		}
	}

	// $column must be a DDLTableColumn instance.
	public function getPSDataType(&$column) {
		if (($column->type == 'binary') ||
			($column->type == 'varbinary') ||
			($column->type == 'blob')) {
			return 'binary';
		}
		return $this->getPHPDataType($column);
	}

	// This must ONLY be used with strings which will be enclosed in double-quotes ("...").
	public function phpencode($s) {
		$ss = '';
		for ($i = 0; $i < strlen($s); $i++) {
			$c = $s[$i];
			$co = ord($c);
			if (($co >= 0x20) && ($co <= 0x7e)) {
				$ss .= $c;
				continue;
			}
			switch ($c) {
			case "\n": $ss .= "\\n"; break;
			case "\r": $ss .= "\\r"; break;
			case "\t": $ss .= "\\t"; break;
			case "\v": $ss .= "\\v"; break;
			case "\f": $ss .= "\\f"; break;
			case "\\": $ss .= "\\\\"; break;
			case "\$": $ss .= "\\\$"; break;
			case "\"": $ss .= "\\\""; break;
			default:
				$hex = dechex($co);
				while (strlen($hex) < 2) $hex = '0'.$hex;
				$ss .= "\\x".$hex;
			}
		}
		return $ss;
	}
}

/*
///
$parser = new XMLDDLParser();
$ddl = $parser->parseFromXML(file_get_contents('/home/rcemer/erp/ddl/modules/gl/gl.ddl.xml'));

///$serializer = new XMLDDLSerializer();
///$xml = $serializer->serialize($ddl, '../../../phpdaogen/ddl.dtd');
///echo $xml;

$serializer = new SQLDDLSerializer();
$sqlStatements = $serializer->serialize($ddl, 'mysql');
foreach ($sqlStatements as $stmt) {
	fprintf(STDOUT, "%s;\n", $stmt);
}
*/
