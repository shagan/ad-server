<?php


//L5:S1
//L5:S1:P1
//L5:S1:P1:C1

function startsWith($haystack, $needle)
{
    return $needle === "" || strpos($haystack, $needle) === 0;
}

$arrS = explode(":","L5:S1");

$arrP = explode(":","L5:S1:P1");

$arrC = explode(":","L5:S1:P1:C123");

$str = $arrC[count($arrC)-1];
if(startsWith($str,'S')){
	echo "Sequence";
}
else if(startsWith($str,'P')){
	echo "Popup";
}
else if(startsWith($str,'C')){
	echo "Campaign";
	echo substr($str, 1);
}






?>