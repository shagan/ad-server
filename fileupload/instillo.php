<?php

// Include specific parameters
include_once("include.params.php");

/////////////////////////////
// Connect to the Database //
/////////////////////////////
/*
mysql_connect("$dbhost","$dbuser","$dbpass") or die ("Could not connect to mysql database");
$db = mysql_select_db("$dbname") or die ("No database");              

// Load details of source files
if ( ! is_dir($fromDir)) {
    exit('Invalid diretory path');
}
$files = array();
		
echo '<h1>Sending files to Instillo ...</h1>';

///////////////////////////////////////////////////////
// Remove the existing items for the relevant region //
///////////////////////////////////////////////////////
$sql = mysql_query("SELECT * FROM lklayoutmedia WHERE layoutID='$layout' AND regionID='$region'") or die (mysql_error());
$mediaCount = mysql_num_rows($sql); // count the media items
if ($mediaCount > 0) {
	while($row = mysql_fetch_array($sql)){ 
        $lkid     = $row["lklayoutmediaID"];
        $mediaID  = $row["mediaID"];
        $layoutID = $row["layoutID"];
        $regionID = $row["regionID"];
	}
}

$sql = mysql_query("DELETE FROM lklayoutmedia WHERE layoutID='$layout' AND regionID='%region'") or die (mysql_error());

// Get the Xml for this Layout from the DB
$sql = sprintf("SELECT xml FROM layout WHERE layoutID = %d ", $layout) or die (mysql_error());
$results = mysql_query($sql);
$row = mysql_fetch_row($results);
$item = $row[0];

// Find the region in question
$xml = new DOMDocument("1.0");
$xml->loadXML($item);

// Get this region from the layout (xpath)
$xpath = new DOMXPath($xml);
		
$regionNodeList = $xpath->query("//region[@id='$region']");
$regionNode = $regionNodeList->item(0);
		
// Remove the media layout link if appropriate
$mediaNodes = $regionNode->getElementsByTagName('media');
$z = $mediaNodes->length;
for ($i = 0; $i < $z; $i++) {
    $mediaNode = $mediaNodes->item(0);
	$id = $mediaNode->getAttribute("id");
	$name = $mediaNode->getAttribute("name");
    $mediaNode->parentNode->removeChild($mediaNode);
}	

////////////////////////
// LOOP THROUGH FILES //
////////////////////////
echo '<table>';
foreach (scandir($fromDir) as $file) {
    if ('.' === $file) continue;
    if ('..' === $file) continue;

    $files[] = $file;
	if (strpos(strtoupper($file), '.JPG') !== false) 
	{

        ////////////////////////////////////////////////////////
        // Insert a record into the media table for each file //
		////////////////////////////////////////////////////////
        $sql = mysql_query("INSERT INTO media (name, type, duration, mediaTags, originalFilename, storedAs, MD5, FileSize, onclick, userID, retired, isEdited, editedMediaID) VALUES ('', 'image', 10, '', '$file', '', '', 0, '', 1, 0, 0, NULL)") or die (mysql_error());
        $pid = mysql_insert_id();
		$storedAs = $pid . '.jpg';
		$from     = $fromDir . $file;
		$to       = $toDir . $storedAs;
		echo '<tr><td>' . $file . '</td><td> -> </td><td>' . $storedAs . '</td></tr>';

        // Calculate the MD5 and the file size
        $md5        = md5_file($from);
        $fileSize   = filesize($from);

        // Update media table with new file name
        $sql = mysql_query("UPDATE media SET name='$pid', storedAs='$storedAs', MD5='$md5', FileSize='$fileSize' WHERE mediaID='$pid'");

        /////////////////////////////////////////
        // Insert a record into the link table // HARDCODED LAYOUT & REGION IDs
        /////////////////////////////////////////
        $sql = mysql_query("INSERT INTO lklayoutmedia (mediaID, layoutID, regionID) VALUES ('$pid', 9, '520379bb8afb2')") or die (mysql_error());
		$lkid = mysql_insert_id();
		
		//////////////////////////////////////////
		// Reconfigure XML Layout for new media //
		//////////////////////////////////////////
     	
		//Import the new media node into this document
		$mediaXmlString = '<media><options><uri>' . $storedAs . '</uri></options></media>';

    	//Get the media's Xml
	    $mediaXml = new DOMDocument("1.0");
		
	    //Load the Media's XML into a SimpleXML object
	    $mediaXml->loadXML($mediaXmlString);
	    $mediaNode = $xml->importNode($mediaXml->documentElement, true);
		$mediaNode->setAttribute('id', $pid);
		$mediaNode->setAttribute('type', 'image');
		$mediaNode->setAttribute('duration', 10);
		$mediaNode->setAttribute('onclick', '');
		$mediaNode->setAttribute('lkid', $lkid);
        $mediaNode->setAttribute('userId', 1);
        $mediaNode->setAttribute('schemaVersion', 1);
		$mediaNode->setAttribute('name', $pid);
		$mediaNode->setAttribute('mediaTags', '');
		$mediaNode->setAttribute('subjectLine', '');
		$mediaNode->setAttribute('smsTextMsg', '');
		$mediaNode->setAttribute('subQRCode', '');
		
	    //Add the new imported node to the regionNode we got from the xpath
	    $regionNode->appendChild($mediaNode);

        ////////////////////////
        // Copy an image file //
		////////////////////////
		copy($from, $to);
	}
}
echo '</table>';

// Convert back to XML
$xml = $xml->saveXML();
$SQL = mysql_query("UPDATE layout SET xml = '$xml' WHERE layoutID = '$layout'") or die (mysql_error());
*/




////////////////////////
// LOOP THROUGH FILES //
////////////////////////
echo '<table>';
foreach (scandir($fromDir) as $file) {
    if ('.' === $file) continue;
    if ('..' === $file) continue;

    $files[] = $file;
	$ext_jpg  = strpos(strtoupper($file), '.JPG');
	$ext_jpeg = strpos(strtoupper($file), '.JPEG');
	$ext_gif  = strpos(strtoupper($file), '.GIF');
	$ext_png  = strpos(strtoupper($file), '.PNG');
	if ($ext_jpg || $ext_jpeg || $ext_gif || $ext_png) 
	{

        // Copy an image file to the Library
        $from = $fromDir . $file;
        $to   = $toDir . $file;

		copy($from, $to);
	}


}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sending files to Library...</title>
<style type="text/css">
body {
	background-color:#D0D0D0 !important;
}
#button {
	-moz-box-shadow:inset 1px 1px 1px 1px #fce2c1;
	-webkit-box-shadow:inset 1px 1px 1px 1px #fce2c1;
	box-shadow:inset 1px 1px 1px 1px #fce2c1;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ffc477), color-stop(1, #fb9e25) );
	background:-moz-linear-gradient( center top, #ffc477 5%, #fb9e25 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffc477', endColorstr='#fb9e25');
	background-color:#ffc477;
	-moz-border-radius:7px;
	-webkit-border-radius:7px;
	border-radius:7px;
	border:1px solid #F60; /*#eeb44f;*/
	display:inline-block;
	color:#FFF;
	font-family:Arial,Tahoma,"Bitstream Vera Sans",sans-serif;
	font-size:16px;
	font-weight:bold;
	padding:2px 16px;
	text-decoration:none;
	text-shadow:1px 1px 0px #cc9f52;
}#button:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #fb9e25), color-stop(1, #ffc477) );
	background:-moz-linear-gradient( center top, #fb9e25 5%, #ffc477 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#fb9e25', endColorstr='#ffc477');
	background-color:#fb9e25;
}#button:active {
	position:relative;
	top:1px;
}
</style>

</head>

<body>
<br/>
<form>
<input type="button" id="button" value="Done" onclick="CloseWin()" />
</form>
<script>
function CloseWin()
{
myWindow=window.close();
}
</script>

</body>
</html>