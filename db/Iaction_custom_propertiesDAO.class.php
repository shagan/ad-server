<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/Iaction_custom_propertiesDAOAbstract.class.php';
class Iaction_custom_propertiesDAO extends Iaction_custom_propertiesDAOAbstract {

	public function updatevalue($iaction_custom_properties) {
	    $ps=new PreparedStatement("INSERT INTO iaction_custom_properties (id, property, value) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE value=?");
	    $ps->setInt($iaction_custom_properties->id);
	    $ps->setString($iaction_custom_properties->property);
	    $ps->setString($iaction_custom_properties->value);
	    $ps->setString($iaction_custom_properties->value);
	    return $this->connection->executeUpdate($ps);
	}

}
