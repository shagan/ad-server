<?php
// THIS FILE IS PART OF THE phpdaogen PACKAGE.  DO NOT EDIT.
// THIS FILE GETS RE-WRITTEN EACH TIME THE DAO GENERATOR IS EXECUTED.
// ANY MANUAL EDITS WILL BE LOST.

// PreparedStatement.class.php
// Copyright (c) 2010-2011 Ronald B. Cemer
// All rights reserved.
// This software is released under the BSD license.
// Please see the accompanying LICENSE.txt for details.

class PreparedStatement {
	private $sqlPieces;
	private $params = array();
	private $paramsAreBinary = array();
	private $paramIdx = 0;
	public $selectOffset = 0, $selectLimit = 0;

	public function PreparedStatement($sql, $selectOffset = 0, $selectLimit = 0, $isRawSQL = false) {
		$sql = trim($sql, " \t\n\r\x00\x0b;");
		if ($isRawSQL) {
			$this->sqlPieces = array($sql);
		} else {
			$this->sqlPieces = explode('?', $sql);
		}
		$this->selectOffset = max(0, (int)$selectOffset);
		$this->selectLimit = max(0, (int)$selectLimit);
	}

	public function clearParams() {
		$this->params = array();
		$this->paramsAreBinary = array();
		$this->paramIdx = 0;
	}

	public function setBoolean($val) {
		if ($val === null) $this->params[$this->paramIdx] = null;
		$this->params[$this->paramIdx] = (boolean)$val;
		$this->paramsAreBinary[$this->paramIdx] = false;
		$this->paramIdx++;
	}

	public function setInt($val) {
		if ($val === null) $this->params[$this->paramIdx] = null;
		$this->params[$this->paramIdx] = (int)$val;
		$this->paramsAreBinary[$this->paramIdx] = false;
		$this->paramIdx++;
	}

	public function setFloat($val) {
		if ($val === null) $this->params[$this->paramIdx] = null;
		$this->params[$this->paramIdx] = (float)$val;
		$this->paramsAreBinary[$this->paramIdx] = false;
		$this->paramIdx++;
	}

	public function setDouble($val) {
		if ($val === null) $this->params[$this->paramIdx] = null;
		$this->params[$this->paramIdx] = (double)$val;
		$this->paramsAreBinary[$this->paramIdx] = false;
		$this->paramIdx++;
	}

	public function setString($val) {
		if ($val === null) $this->params[$this->paramIdx] = null;
		$this->params[$this->paramIdx] = (string)$val;
		$this->paramsAreBinary[$this->paramIdx] = false;
		$this->paramIdx++;
	}

	public function setBinary($val) {
		if ($val === null) $this->params[$this->paramIdx] = null;
		$this->params[$this->paramIdx] = (string)$val;
		$this->paramsAreBinary[$this->paramIdx] = true;
		$this->paramIdx++;
	}

	public function toSQL($connection) {
		if (count($this->params) != (count($this->sqlPieces)-1)) {
			throw new Exception(sprintf(
				'PreparedStatement contains %d placeholder(s) but %d parameter(s)',
				count($this->sqlPieces)-1,
				count($this->params)
			));
		}
		$sql = $this->sqlPieces[0];
		for ($i = 1, $j = 0, $n = count($this->sqlPieces); $i < $n; $i++, $j++) {
			$sql .=
				$connection->encode($this->params[$j], $this->paramsAreBinary[$j]) .
				$this->sqlPieces[$i];
		}
		return $sql;
	}
}
