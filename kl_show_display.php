    <?php include("kl_inc_header.php"); ?>

<?php 
    include 'db/dbheader.php';
    include 'db/Display.class.php';
    include 'db/DisplayDAO.class.php';
    include 'db/Display_properties.class.php';
    include 'db/Display_propertiesDAO.class.php';
?>

    <body>

        <?php include("kl_inc_navbar.php"); ?>
<?php
    
    $display=NULL;
    if(isset($_GET["id"])) {
        $displayDAO=new DisplayDAO($con);
        $displayArr=$displayDAO->findByDisplayid($_GET["id"]);
        $display=$displayArr[0];
        $displayProp = new Display_propertiesDAO($con);
       
    }
?>


        <?php include("kl_inc_sidebar.php"); ?>

        <div class="main-content">
            <div class="breadcrumbs" id="breadcrumbs">
             <script type="text/javascript">
             try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
             </script>


              <ul class="breadcrumb">
                        <li>
                            <i class="icon-home home-icon"></i>
                            <a href="#">Home</a>

                            <span class="divider">
                                <i class="icon-angle-right arrow-icon"></i>
                                <a href="kl_groups.php">Display Groups</a>
                            </span>
                        <span class="divider">
                                <i class="icon-angle-right arrow-icon"></i>
                                <?php echo $display->display ?>
                        </span></li>
                    </ul>
            </div>

             <div class="page-content">


                <div class="row-fluid">
                    <div class="span12">
                        <!--PAGE CONTENT BEGINS-->
                      <div class="hr dotted"></div>


                        <div id="user-profile-2" class="user-profile row-fluid">
                            <div class="tabbable" >
                                <ul class="nav nav-tabs padding-18">
                                    <li  class="active">
                                        <a data-toggle="tab" href="#home">
                                            <i class="green icon-user bigger-120"></i>
                                            Profile
                                        </a>
                                    </li>

                                    <li>
                                        <a data-toggle="tab" href="#logs">
                                            <i class="orange icon-rss bigger-120"></i>
                                            Logs
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#properties">
                                            <i class="blue icon-group bigger-120"></i>
                                            Properties
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#events">
                                            <i class="blue icon-group bigger-120"></i>
                                            Events
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#subscriptions">
                                            <i class="blue icon-group bigger-120"></i>
                                            Subscriptions
                                        </a>
                                    </li>

                                    <li>
                                        <a data-toggle="tab" href="#screenshots">
                                            <i class="pink icon-picture bigger-120"></i>
                                            Screenshots
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content no-border padding-24">
                                    <div id="home" class="tab-pane in active">
                                        <div class="row-fluid">
                                            <div class="span3 center">
                                                <span class="profile-picture">
                                                    <img class="editable" alt="Alex's Avatar" id="avatar2" src="http://access.jabbers.ie/screenshots/<?php echo $display->display ?>/latest" />
                                                </span>

                                                <div class="space space-4"></div>

                                                <a href="#" class="btn btn-small btn-block btn-inverse">
                                                    <i class="icon-lock"></i>
                                                    Lock Screen
                                                </a>

                                                <a href="#" class="btn btn-small btn-block btn-danger">
                                                    <i class="icon-refresh"></i>
                                                    Restart Tablet
                                                </a>
                                            </div><!--/span-->
                                            <?php //echo json_encode($display)?>
                                            <div class="span9">
                                                <h4 class="blue">
                                                    <span class="middle"><?php echo $display->display ?></span>

                                                   
                                                        <?php if($display->loggedin==1) {?>
                                                         <span class="label label-purple arrowed-in-right">
                                                            <i class="icon-circle smaller-80"></i>
                                                            online
                                                         </span>
                                                        
                                                        <?php } else { ?>
                                                         <span class="label label-important arrowed-in-right">
                                                            <i class="icon-circle smaller-80"></i>
                                                            offline
                                                         </span>
                                                         
                                                        <?php } ?>
                                                   
                                                </h4>

                                                <div class="profile-user-info">
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name"> name </div>

                                                        <div class="profile-info-value">
                                                            <span><?php echo $display->display ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name"> hardware key </div>

                                                        <div class="profile-info-value">
                                                            <span><?php echo $display->display ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name"> Location </div>

                                                        <div class="profile-info-value">
                                                            <i class="icon-map-marker light-orange bigger-110"></i>
                                                            <span>Unknown</span>

                                                        </div>
                                                    </div>

                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name"> Synchronized </div>

                                                        <div class="profile-info-value">
                                                           <?php if ($display->MediaInventoryStatus==1) { ?>
                                                               <span class="label label-success arrowed">Yes</span>
                                                            <?php } else { ?>
                                                               <span class="label label-warning arrowed">No</span>
                                                            <?php } ?>
                                                                
                                                           
                                                        </div>
                                                    </div>



                                                     <div class="profile-info-row">
                                                        <div class="profile-info-name"> AppDownloader Version </div>

                                                        <div class="profile-info-value">
                                                            <span><?php echo $displayProp->load($display->displayid,'prop.appversion.com.instillo.appdownloader')->value ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name"> Signage Version </div>

                                                        <div class="profile-info-value">
                                                            <span><?php echo $displayProp->load($display->displayid,'prop.appversion.ie.jabbers.prototype.home')->value ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name"> Watchdog Version </div>

                                                        <div class="profile-info-value">
                                                            <span><?php echo $displayProp->load($display->displayid,'prop.appversion.com.instillo.watchdog.home')->value ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name"> RMS Version </div>

                                                        <div class="profile-info-value">
                                                            <span><?php echo $displayProp->load($display->displayid,'prop.appversion.com.updater')->value ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name"> Logger Version </div>

                                                        <div class="profile-info-value">
                                                            <span><?php echo $displayProp->load($display->displayid,'prop.appversion.com.instillo.datalogger')->value ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name"> RebootApp Version </div>

                                                        <div class="profile-info-value">
                                                            <span><?php echo $displayProp->load($display->displayid,'prop.appversion.com.instillo.kungfureboot')->value ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name"> Keyboard Version </div>

                                                        <div class="profile-info-value">
                                                            <span><?php echo $displayProp->load($display->displayid,'prop.appversion.com.instillo.softkeyboard')->value ?></span>
                                                        </div>
                                                    </div>



                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name"> MAC Address</div>

                                                        <div class="profile-info-value">
                                                            <span><?php echo $displayProp->load($display->displayid,'prop.mac.wifi')->value ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name"> IP Address</div>

                                                        <div class="profile-info-value">
                                                            <span><?php echo $displayProp->load($display->displayid,'dhcp.wlan0.ipaddress')->value ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name"> Registered under</div>

                                                        <div class="profile-info-value">
                                                            <span><?php echo $displayProp->load($display->displayid,'prop.email')->value ?></span>
                                                        </div>
                                                    </div>

                                                     <div class="profile-info-row">
                                                        <div class="profile-info-name"> Sync Interval</div>

                                                        <div class="profile-info-value">
                                                            <span><?php echo $displayProp->load($display->displayid,'prop.pref.etxtSync')->value ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name"> Last Accessed </div>

                                                        <div class="profile-info-value">
                                                            <span>5 mins ago</span>
                                                        </div>
                                                    </div>

                                                      <div class="profile-info-row">
                                                        <div class="profile-info-name"> Client Address </div>

                                                        <div class="profile-info-value">
                                                            <span><?php echo $display->ClientAddress ?></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="hr hr-8 dotted"></div>

                                                <div class="profile-user-info">
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name"> Website </div>

                                                        <div class="profile-info-value">
                                                            <a href="#" target="_blank">www.alexdoe.com</a>
                                                        </div>
                                                    </div>

                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">
                                                            <i class="middle icon-facebook-sign bigger-150 blue"></i>
                                                        </div>

                                                        <div class="profile-info-value">
                                                            <a href="#">Find me on Facebook</a>
                                                        </div>
                                                    </div>

                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">
                                                            <i class="middle icon-twitter-sign bigger-150 light-blue"></i>
                                                        </div>

                                                        <div class="profile-info-value">
                                                            <a href="#">Follow me on Twitter</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!--/span-->
                                        </div><!--/row-fluid-->

                                        <div class="space-20"></div>

                                        <div class="row-fluid">
                                            <div class="span6">
                                                <div class="widget-box transparent">
                                                    <div class="widget-header widget-header-small">
                                                        <h4 class="smaller">
                                                            <i class="orange icon-rss bigger-80"></i>
                                                            Activity Feed
                                                        </h4>
                                                    </div>

                                                    <div class="profile-activity clearfix">
                                                        <div>
                                                            <span class="label label-success arrowed">up</span>
                                                            Display back online
                                                            <div class="time">
                                                                <i class="icon-time bigger-110"></i>
                                                                4 hours ago
                                                            </div>
                                                        </div>

                                                        
                                                    </div>

                                                    <div class="profile-activity clearfix">
                                                        <div>
                                                            <span class="label label-important arrowed">down</span>
                                                            Display went down
                                                            <div class="time">
                                                                <i class="icon-time bigger-110"></i>
                                                                5 hours ago
                                                            </div>
                                                        </div>

                                                        
                                                    </div>
                                                    <div class="profile-activity clearfix">
                                                        <div>
                                                            <span class="label label-success arrowed">up</span>
                                                            Display back online
                                                            <div class="time">
                                                                <i class="icon-time bigger-110"></i>
                                                                2 days ago
                                                            </div>
                                                        </div>

                                                        
                                                    </div>

                                                    <div class="profile-activity clearfix">
                                                        <div>
                                                            <span class="label label-important arrowed">down</span>
                                                            Display went down
                                                            <div class="time">
                                                                <i class="icon-time bigger-110"></i>
                                                                2 days ago
                                                            </div>
                                                        </div>

                                                        
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="span6">
                                                <div class="widget-box transparent">
                                                    <div class="widget-header widget-header-small header-color-blue2">
                                                        <h4 class="smaller">
                                                            <i class="icon-lightbulb bigger-120"></i>
                                                            My Skills
                                                        </h4>
                                                    </div>

                                                    <div class="widget-body">
                                                        <div class="widget-main padding-16">
                                                            <div class="row-fluid">
                                                                <div class="grid3 center">
                                                                    <div class="easy-pie-chart percentage" data-percent="45" data-color="#CA5952">
                                                                        <span class="percent">45</span>
                                                                        %
                                                                    </div>

                                                                    <div class="space-2"></div>
                                                                    Graphic Design
                                                                </div>

                                                                <div class="grid3 center">
                                                                    <div class="center easy-pie-chart percentage" data-percent="90" data-color="#59A84B">
                                                                        <span class="percent">90</span>
                                                                        %
                                                                    </div>

                                                                    <div class="space-2"></div>
                                                                    HTML5 & CSS3
                                                                </div>

                                                                <div class="grid3 center">
                                                                    <div class="center easy-pie-chart percentage" data-percent="80" data-color="#9585BF">
                                                                        <span class="percent">80</span>
                                                                        %
                                                                    </div>

                                                                    <div class="space-2"></div>
                                                                    Javascript/jQuery
                                                                </div>
                                                            </div>

                                                            <div class="hr hr-16"></div>

                                                            <div class="profile-skills">
                                                                <div class="progress">
                                                                    <div class="bar" style="width:80%">
                                                                        <span class="pull-left">HTML5 & CSS3</span>
                                                                        <span class="pull-right">80%</span>
                                                                    </div>
                                                                </div>

                                                                <div class="progress progress-success">
                                                                    <div class="bar" style="width:72%">
                                                                        <span class="pull-left">Javascript & jQuery</span>

                                                                        <span class="pull-right">72%</span>
                                                                    </div>
                                                                </div>

                                                                <div class="progress progress-purple">
                                                                    <div class="bar" style="width:70%">
                                                                        <span class="pull-left">PHP & MySQL</span>

                                                                        <span class="pull-right">70%</span>
                                                                    </div>
                                                                </div>

                                                                <div class="progress progress-warning">
                                                                    <div class="bar" style="width:50%">
                                                                        <span class="pull-left">Wordpress</span>

                                                                        <span class="pull-right">50%</span>
                                                                    </div>
                                                                </div>

                                                                <div class="progress progress-danger">
                                                                    <div class="bar" style="width:35%">
                                                                        <span class="pull-left">Photoshop</span>

                                                                        <span class="pull-right">35%</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--#home-->

                                    <div id="logs" class="tab-pane">
                                        <div class="row-fluid">


                                            <table id="logs_table" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Date</th>
                                                        <th>Message</th>
                                                        <th>Client Address</th>
                                                        <th>Display ID</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    
                                                </tbody>
                                            </table>
                                        </div>

                                    </div><!--/#feed-->
                                    <div id="properties" class="tab-pane">
                                        <div class="row-fluid">


                                            <table id="properties_table" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>

                                                        <th>Parameter</th>
                                                        <th>Value</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <tr> <td>loading..</td><td>loading..</td></tr>



                                                </tbody>
                                            </table>
                                        </div>

                                    </div><!--/#properties-->
                                    <div id="events" class="tab-pane">
                                        <div class="row-fluid">

                                            <table id="events_table" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                     <tr>
                                                        <th>ID</th>
                                                        <th>Date</th>
                                                        <th>Level</th>
                                                        <th>Code</th>
                                                        <th>Message</th>
                                                       <th>DisplayID</th>
                                                     </tr>
                                                </thead>

                                                <tbody>
                                                    
                                                </tbody>
                                            </table>                                        
                                        </div>

                                    </div><!--/#properties-->

                                    <div id="subscriptions" class="tab-pane">
                                        <table id="subscriptions_table" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Date</th>
                                                        <th>Type</th>
                                                        <th>Value</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <tr>  <td>loading..</td><td>loading..</td><td>loading..</td></tr>



                                                </tbody>
                                            </table>
                                    </div><!--/#friends-->

                                    <div id="screenshots" class="tab-pane">
                                        <ul class="ace-thumbnails">
                                            <?php 
                                            $url = 'http://access.jabbers.ie/images.html?phoneid='.$display->display;
                                            $sshot_arr=json_decode(file_get_contents($url));
                                            
                                            for ($i= (sizeof($sshot_arr)-2); $i > (sizeof($sshot_arr)-51) ; $i--) { 
                                                 
                                            ?>
                                            <li>
                                                <a href="http://access.jabbers.ie/screenshots/<?php echo $display->display ?>/<?php echo $sshot_arr[$i] ?>" data-rel="colorbox">
                                                    <img width="150" height="150" alt="150x150" src="http://access.jabbers.ie/screenshots/<?php echo $display->display ?>/<?php echo $sshot_arr[$i] ?>" />
                                                    <div class="text">
                                                        <div class="inner"><?php echo $sshot_arr[$i] ?></div>
                                                    </div>
                                                </a>
                                                <!--
                                                <div class="tools tools-bottom">
                                                    <a href="#">
                                                        <i class="icon-link"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="icon-paper-clip"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="icon-pencil"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="icon-remove red"></i>
                                                    </a>
                                                </div>
                                            -->
                                            </li>
                                            <?php } ?>
                                            
                                        </ul>
                                    </div><!--/#pictures-->
                                </div>
                            </div>
                        </div>

                        

                        <!--PAGE CONTENT ENDS-->
                    </div><!--/.span-->
                </div><!--/.row-fluid-->
            </div><!--/.page-content-->

            <?php include("kl_inc_ace_settings.php"); ?>




        </div><!--/.main-content-->
    </div><!--/.main-container-->

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
       <i class="icon-double-angle-up icon-only bigger-110"></i>
   </a>

   <!--basic scripts-->

   <!--[if !IE]>-->

   <script type="text/javascript">
   window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
   </script>

   <!--<![endif]-->

    		<!--[if IE]>
    <script type="text/javascript">
     window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
    </script>
    <![endif]-->

    <script type="text/javascript">
    if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!--page specific plugin scripts-->
    <script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="assets/js/jquery.ui.touch-punch.min.js"></script>
    <script src="assets/js/jquery.gritter.min.js"></script>
    <script src="assets/js/bootbox.min.js"></script>
    <script src="assets/js/jquery.slimscroll.min.js"></script>
    <script src="assets/js/jquery.easy-pie-chart.min.js"></script>
    <script src="assets/js/jquery.hotkeys.min.js"></script>
    <script src="assets/js/bootstrap-wysiwyg.min.js"></script>
    <script src="assets/js/select2.min.js"></script>
    <script src="assets/js/date-time/bootstrap-datepicker.min.js"></script>
    <script src="assets/js/fuelux/fuelux.spinner.min.js"></script>
    <script src="assets/js/x-editable/bootstrap-editable.min.js"></script>
    <script src="assets/js/x-editable/ace-editable.min.js"></script>
    <script src="assets/js/jquery.maskedinput.min.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
        <script src="assets/js/jquery.dataTables.bootstrap.js"></script>
        <script src="assets/js/jquery.colorbox-min.js"></script>
    <!--ace scripts-->


    <!--inline scripts related to this page-->
    <script type="text/javascript">
    jQuery(function($) {

                //editables on first profile page
                $.fn.editable.defaults.mode = 'inline';
                $.fn.editableform.loading = "<div class='editableform-loading'><i class='light-blue icon-2x icon-spinner icon-spin'></i></div>";
                $.fn.editableform.buttons = '<button type="submit" class="btn btn-info editable-submit"><i class="icon-ok icon-white"></i></button>'+
                '<button type="button" class="btn editable-cancel"><i class="icon-remove"></i></button>';    
                
                //editables 
                $('#username').editable({
                    type: 'text',
                    name: 'username'
                });


                var countries = [];
                $.each({ "CA": "Canada", "IN": "India", "NL": "Netherlands", "TR": "Turkey", "US": "United States"}, function(k, v) {
                    countries.push({id: k, text: v});
                });

                var cities = [];
                cities["CA"] = [];
                $.each(["Toronto", "Ottawa", "Calgary", "Vancouver"] , function(k, v){
                    cities["CA"].push({id: v, text: v});
                });
                cities["IN"] = [];
                $.each(["Delhi", "Mumbai", "Bangalore"] , function(k, v){
                    cities["IN"].push({id: v, text: v});
                });
                cities["NL"] = [];
                $.each(["Amsterdam", "Rotterdam", "The Hague"] , function(k, v){
                    cities["NL"].push({id: v, text: v});
                });
                cities["TR"] = [];
                $.each(["Ankara", "Istanbul", "Izmir"] , function(k, v){
                    cities["TR"].push({id: v, text: v});
                });
                cities["US"] = [];
                $.each(["New York", "Miami", "Los Angeles", "Chicago", "Wysconsin"] , function(k, v){
                    cities["US"].push({id: v, text: v});
                });
                
                var currentValue = "NL";
                $('#country').editable({
                    type: 'select2',
                    value : 'NL',
                    source: countries,
                    success: function(response, newValue) {
                        if(currentValue == newValue) return;
                        currentValue = newValue;
                        
                        var new_source = (!newValue || newValue == "") ? [] : cities[newValue];
                        
                        //the destroy method is causing errors in x-editable v1.4.6
                        //it worked fine in v1.4.5
                        /**         
                        $('#city').editable('destroy').editable({
                            type: 'select2',
                            source: new_source
                        }).editable('setValue', null);
                */

                        //so we remove it altogether and create a new element
                        var city = $('#city').removeAttr('id').get(0);
                        $(city).clone().attr('id', 'city').text('Select City').editable({
                            type: 'select2',
                            value : null,
                            source: new_source
                        }).insertAfter(city);//insert it after previous instance
                        $(city).remove();//remove previous instance
                        
                    }
                });

    $('#city').editable({
        type: 'select2',
        value : 'Amsterdam',
        source: cities[currentValue]
    });



    $('#signup').editable({
        type: 'date',
        format: 'yyyy-mm-dd',
        viewformat: 'dd/mm/yyyy',
        datepicker: {
            weekStart: 1
        }
    });

    $('#age').editable({
        type: 'spinner',
        name : 'age',
        spinner : {
            min : 16, max:99, step:1
        }
    });

                //var $range = document.createElement("INPUT");
                //$range.type = 'range';
                $('#login').editable({
                    type: 'slider',//$range.type == 'range' ? 'range' : 'slider',
                    name : 'login',
                    slider : {
                        min : 1, max:50, width:100
                    },
                    success: function(response, newValue) {
                        if(parseInt(newValue) == 1)
                            $(this).html(newValue + " hour ago");
                        else $(this).html(newValue + " hours ago");
                    }
                });

                $('#about').editable({
                    mode: 'inline',
                    type: 'wysiwyg',
                    name : 'about',
                    wysiwyg : {
                        //css : {'max-width':'300px'}
                    },
                    success: function(response, newValue) {
                    }
                });
                
                
                
                // *** editable avatar *** //
                try {//ie8 throws some harmless exception, so let's catch it

                    //it seems that editable plugin calls appendChild, and as Image doesn't have it, it causes errors on IE at unpredicted points
                    //so let's have a fake appendChild for it!
                    if( /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ) Image.prototype.appendChild = function(el){}

                        var last_gritter
                    $('#avatar').editable({
                        type: 'image',
                        name: 'avatar',
                        value: null,
                        image: {
                            //specify ace file input plugin's options here
                            btn_choose: 'Change Avatar',
                            droppable: true,
                            /**
                            //this will override the default before_change that only accepts image files
                            before_change: function(files, dropped) {
                                return true;
                            },
                            */

                            //and a few extra ones here
                            name: 'avatar',//put the field name here as well, will be used inside the custom plugin
                            max_size: 110000,//~100Kb
                            on_error : function(code) {//on_error function will be called when the selected file has a problem
                                if(last_gritter) $.gritter.remove(last_gritter);
                                if(code == 1) {//file format error
                                    last_gritter = $.gritter.add({
                                        title: 'File is not an image!',
                                        text: 'Please choose a jpg|gif|png image!',
                                        class_name: 'gritter-error gritter-center'
                                    });
                                } else if(code == 2) {//file size rror
                                    last_gritter = $.gritter.add({
                                        title: 'File too big!',
                                        text: 'Image size should not exceed 100Kb!',
                                        class_name: 'gritter-error gritter-center'
                                    });
                                }
                                else {//other error
                                }
                            },
                            on_success : function() {
                                $.gritter.removeAll();
                            }
                        },
                        url: function(params) {
                            // ***UPDATE AVATAR HERE*** //
                            //You can replace the contents of this function with examples/profile-avatar-update.js for actual upload


                            var deferred = new $.Deferred

                            //if value is empty, means no valid files were selected
                            //but it may still be submitted by the plugin, because "" (empty string) is different from previous non-empty value whatever it was
                            //so we return just here to prevent problems
                            var value = $('#avatar').next().find('input[type=hidden]:eq(0)').val();
                            if(!value || value.length == 0) {
                                deferred.resolve();
                                return deferred.promise();
                            }


                            //dummy upload
                            setTimeout(function(){
                                if("FileReader" in window) {
                                    //for browsers that have a thumbnail of selected image
                                    var thumb = $('#avatar').next().find('img').data('thumb');
                                    if(thumb) $('#avatar').get(0).src = thumb;
                                }
                                
                                deferred.resolve({'status':'OK'});

                                if(last_gritter) $.gritter.remove(last_gritter);
                                last_gritter = $.gritter.add({
                                    title: 'Avatar Updated!',
                                    text: 'Uploading to server can be easily implemented. A working example is included with the template.',
                                    class_name: 'gritter-info gritter-center'
                                });
                                
                            } , parseInt(Math.random() * 800 + 800))

    return deferred.promise();
},

success: function(response, newValue) {
}
})
}catch(e) {}



                //another option is using modals
                $('#avatar2').on('click', function(){
                    var modal = 
                    '<div class="modal hide fade">\
                    <div class="modal-header">\
                    <button type="button" class="close" data-dismiss="modal">&times;</button>\
                    <h4 class="blue">Change Avatar</h4>\
                    </div>\
                    \
                    <form class="no-margin">\
                    <div class="modal-body">\
                    <div class="space-4"></div>\
                    <div style="width:75%;margin-left:12%;"><input type="file" name="file-input" /></div>\
                    </div>\
                    \
                    <div class="modal-footer center">\
                    <button type="submit" class="btn btn-small btn-success"><i class="icon-ok"></i> Submit</button>\
                    <button type="button" class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Cancel</button>\
                    </div>\
                    </form>\
                    </div>';
                    
                    
                    var modal = $(modal);
                    modal.modal("show").on("hidden", function(){
                        modal.remove();
                    });

                    var working = false;

                    var form = modal.find('form:eq(0)');
                    var file = form.find('input[type=file]').eq(0);
                    file.ace_file_input({
                        style:'well',
                        btn_choose:'Click to choose new avatar',
                        btn_change:null,
                        no_icon:'icon-picture',
                        thumbnail:'small',
                        before_remove: function() {
                            //don't remove/reset files while being uploaded
                            return !working;
                        },
                        before_change: function(files, dropped) {
                            var file = files[0];
                            if(typeof file === "string") {
                                //file is just a file name here (in browsers that don't support FileReader API)
                                if(! (/\.(jpe?g|png|gif)$/i).test(file) ) return false;
                            }
                            else {//file is a File object
                                var type = $.trim(file.type);
                                if( ( type.length > 0 && ! (/^image\/(jpe?g|png|gif)$/i).test(type) )
                                        || ( type.length == 0 && ! (/\.(jpe?g|png|gif)$/i).test(file.name) )//for android default browser!
                                        ) return false;

                                if( file.size > 110000 ) {//~100Kb
                                    return false;
                                }
                            }

                            return true;
                        }
                    });

    form.on('submit', function(){
        if(!file.data('ace_input_files')) return false;

        file.ace_file_input('disable');
        form.find('button').attr('disabled', 'disabled');
        form.find('.modal-body').append("<div class='center'><i class='icon-spinner icon-spin bigger-150 orange'></i></div>");

        var deferred = new $.Deferred;
        working = true;
        deferred.done(function() {
            form.find('button').removeAttr('disabled');
            form.find('input[type=file]').ace_file_input('enable');
            form.find('.modal-body > :last-child').remove();

            modal.modal("hide");
            
            var thumb = file.next().find('img').data('thumb');
            if(thumb) $('#avatar2').get(0).src = thumb;
            
            working = false;
        });


        setTimeout(function(){
            deferred.resolve();
        } , parseInt(Math.random() * 800 + 800));

        return false;
    });

});



                //////////////////////////////
                $('#profile-feed-1').slimScroll({
                    height: '250px',
                    alwaysVisible : true
                });

                $('.profile-social-links > a').tooltip();

                $('.easy-pie-chart.percentage').each(function(){
                    var barColor = $(this).data('color') || '#555';
                    var trackColor = '#E2E2E2';
                    var size = parseInt($(this).data('size')) || 72;
                    $(this).easyPieChart({
                        barColor: barColor,
                        trackColor: trackColor,
                        scaleColor: false,
                        lineCap: 'butt',
                        lineWidth: parseInt(size/10),
                        animate:false,
                        size: size
                    }).css('color', barColor);
                });

                var colorbox_params = {
                    reposition:true,
                    scalePhotos:true,
                    scrolling:false,
                    previous:'<i class="icon-arrow-left"></i>',
                    next:'<i class="icon-arrow-right"></i>',
                    close:'&times;',
                    current:'{current} of {total}',
                    maxWidth:'100%',
                    maxHeight:'100%',
                    onOpen:function(){
                        document.body.style.overflow = 'hidden';
                    },
                    onClosed:function(){
                        document.body.style.overflow = 'auto';
                    },
                    onComplete:function(){
                        $.colorbox.resize();
                    }
                };

                $('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);

                ///////////////////////////////////////////

                //show the user info on right or left depending on its position
                $('#user-profile-2 .memberdiv').on('mouseenter', function(){
                    var $this = $(this);
                    var $parent = $this.closest('.tab-pane');

                    var off1 = $parent.offset();
                    var w1 = $parent.width();

                    var off2 = $this.offset();
                    var w2 = $this.width();

                    var place = 'left';
                    if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) place = 'right';
                    
                    $this.find('.popover').removeClass('right left').addClass(place);
                }).on('click', function() {
                    return false;
                });

                ////////////////////
                //change profile
                $('[data-toggle="buttons-radio"]').on('click', function(e){
                    var target = $(e.target);
                    var which = parseInt($.trim(target.text()));
                    $('.user-profile').parent().hide();
                    $('#user-profile-'+which).parent().show();
                });

                $( ".nav" ).on( "shown", function( event, ui ) {
                    if(event.target.hash.indexOf("properties")>0){
                        if($('#properties_table tbody tr').length>5)return;
                        $.get( "db/process_dispprop.php?action=list&id=<?php echo $_GET["id"] ?>", function( data ) {
                         $('#properties_table tbody tr').remove();
                          var obj = jQuery.parseJSON(data);
                          for (var i = 0; i <= obj.length - 1; i++) {
                              $('#properties_table > tbody:last').append('<tr><td>'+obj[i].property+'</td><td>'+obj[i].value+'</td></tr>');
                              console.log( "Adding"+obj[i] );
                          };
                      });
                    }
                    else if(event.target.hash.indexOf("subscriptions")>0){
                        if($('#statistics_table tbody tr').length>5)return;
                        $.get( "db/process_subscribers.php?action=list&display_id=<?php echo $_GET["id"] ?>", function( data ) {
                         $('#subscriptions_table tbody tr').remove();
                          var obj = jQuery.parseJSON(data);
                          for (var i = 0; i <= obj.length - 1; i++) {
                              $('#subscriptions_table > tbody:last').append('<tr><td>'+obj[i].subDate+'</td><td>'+obj[i].type+'</td><td>'+obj[i].value+'</td></tr>');
                              console.log( "Adding"+obj[i] );
                          };
                      });
                    }
                    else if(event.target.hash.indexOf("events")>0){
                      if($('#events_table tbody tr').length>5)return;
                      var oTable1 = $('#events_table').dataTable( {
                        "iDisplayLength": 50,
                        "bProcessing": true,
                        "bServerSide": true,
                        "aaSorting": [[ 1, "desc" ]],
                        "fnCreatedRow": function(){
                            setTimeout( "$('[data-rel=tooltip]').tooltip();",1000 );
                        },
                        // "fnServerParams": function ( aoData ) {
                        //     aoData.push( { "name": "sSearch_5", "value": "<?php echo $_GET["id"] ?>" } );
                        // },
                        "aoColumns": [
                              null,{ "sWidth": "120" },
                              null,
                              null,null,null
                            ],
                        "sAjaxSource": "db/datatable_events.php?id=<?php echo $_GET["id"] ?>" } );
                    }
                    else if(event.target.hash.indexOf("logs")>0){
                      if($('#logs_table tbody tr').length>5)return;
                      var oTable1 = $('#logs_table').dataTable( {
                        "iDisplayLength": 50,
                        "bProcessing": true,
                        "bServerSide": true,
                        "aaSorting": [[ 1, "desc" ]],
                        "fnCreatedRow": function(){
                            setTimeout( "$('[data-rel=tooltip]').tooltip();",1000 );
                        },
                        // "fnServerParams": function ( aoData ) {
                        //     aoData.push( { "name": "sSearch_4", "value": "<?php echo $_GET["id"] ?>" } );
                        // },
                        "aoColumns": [
                              null,{ "sWidth": "120" },
                              null,
                              null,null
                            ],
                        "sAjaxSource": "db/datatable_log.php?id=<?php echo $_GET["id"] ?>" } );
                    }
                } );

            });
    </script>
   
</body>
</html>
