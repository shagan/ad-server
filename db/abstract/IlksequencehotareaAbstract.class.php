<?php
// Generated automatically by phpdaogen.
// Do NOT edit this file.
// Any changes made to this file will be overwritten the next time it is generated.

abstract class IlksequencehotareaAbstract {
	public $sequence_id;
	public $hotarea_id;

	public static function createDefault() {
		$v = new Ilksequencehotarea();
		$v->defaultAllFields();
		return $v;
	}

	public function defaultAllFields() {
		$this->sequence_id = 0;
		$this->hotarea_id = 0;
		return $this;
	}

	public function loadFromArray($arr) {
		$this->sequence_id = isset($arr['sequence_id']) ? (int)$arr['sequence_id'] : 0;
		$this->hotarea_id = isset($arr['hotarea_id']) ? (int)$arr['hotarea_id'] : 0;
		return $this;
	}
}
