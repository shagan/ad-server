<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/IlksegmentsequenceDAOAbstract.class.php';
class IlksegmentsequenceDAO extends IlksegmentsequenceDAOAbstract {

	public function deleteSequenceForSegment($segment_id){
		$sql = "DELETE FROM ilksegmentsequence WHERE segment_id =?;";
		$ps = new PreparedStatement($sql);
		$ps->setInt($segment_id);
		$this->connection->executeQuery($ps);		
	}

	public function insertTodaysSequencesForSegment($segment_id){
		$sql = "INSERT INTO ilksegmentsequence( segment_id, sequence_id, displayorder ) SELECT `irun`.segment_id, `irun`.sequence_id, `ilksegmentsequenceorder`.displayorder FROM  `irun`,`ilksegmentsequenceorder` WHERE `irun`.segment_id =? AND `ilksegmentsequenceorder`.segment_id=`irun`.segment_id AND `ilksegmentsequenceorder`.sequence_id=`irun`.sequence_id AND DATE( NOW( ) ) BETWEEN startDate AND endDate order by `ilksegmentsequenceorder`.displayorder;";
		$ps = new PreparedStatement($sql);
		$ps->setInt($segment_id);
		$this->connection->executeQuery($ps);		
	}

	public function sortSegments($segment_id, $order){
		$sql="";
		$i=1;
		foreach ($order as $item) {
			$sql="UPDATE ilksegmentsequence SET displayorder=$i where region_id=$region_id AND segment_id=$item;";
			$i++;
			$ps = new PreparedStatement($sql);
			$this->connection->executeQuery($ps);
		}	
		return;
	}


}
