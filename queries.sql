--QUERIES : ==> 
--Selects id and names from advertiser, campaign and sequence

select 
iadvertiser.id as advertiserid, iadvertiser.name as advertiser_name, 
icampaign.id as campaignid, icampaign.name as campaign_name, 
isequence.id as sequence_id, isequence.name as sequence_name 
from iadvertiser,icampaign,isequence, ilkadvertisercampaign,ilkcampaignsequence 
where ( iadvertiser.id = ilkadvertisercampaign.advertiser_id 
	AND icampaign.id = ilkadvertisercampaign.campaign_id 
	AND icampaign.id = ilkcampaignsequence.campaign_id 
	AND isequence.id = ilkcampaignsequence.sequence_id);

-- ONly of type banner
SELECT  isequence.id AS sequence_id, iadvertiser.name AS advertiser_name, icampaign.name AS campaign_name,
isequence.name AS sequence_name
FROM iadvertiser, icampaign, isequence, ilkadvertisercampaign, ilkcampaignsequence
WHERE (iadvertiser.id = ilkadvertisercampaign.advertiser_id
AND icampaign.id = ilkadvertisercampaign.campaign_id
AND icampaign.id = ilkcampaignsequence.campaign_id
AND isequence.id = ilkcampaignsequence.sequence_id
AND isequence.type =  "com.instillo.layout.model.BannerSequence"
);

-- Only items that are not already in this segment
SELECT  isequence.id AS sequence_id, iadvertiser.name AS advertiser_name, icampaign.name AS campaign_name,
isequence.name AS sequence_name
FROM iadvertiser, icampaign, isequence, ilkadvertisercampaign, ilkcampaignsequence
WHERE (iadvertiser.id = ilkadvertisercampaign.advertiser_id
AND icampaign.id = ilkadvertisercampaign.campaign_id
AND icampaign.id = ilkcampaignsequence.campaign_id
AND isequence.id = ilkcampaignsequence.sequence_id
AND isequence.type =  "com.instillo.layout.model.BannerSequence"
AND isequence.id NOT in ( select sequence_id from ilksegmentsequence where segment_id = 1)
);

-- View for display - displaygroup join table
SELECT display.displayid, display.display, display.MacAddress, display.ClientAddress, display.loggedin, display.lastaccessed, display.licensed, displaygroup.displaygroupid
FROM display, displaygroup, lkdisplaydg
WHERE displaygroup.displaygroupid = lkdisplaydg.displaygroupid
AND display.displayid = lkdisplaydg.displayid
AND displaygroup.isDisplayspecific =0;

create view joindisplaydg as SELECT display.displayid, display.display, display.MacAddress, display.ClientAddress, display.loggedin, display.lastaccessed, display.licensed, displaygroup.displaygroupid
FROM display, displaygroup, lkdisplaydg
WHERE displaygroup.displaygroupid = lkdisplaydg.displaygroupid
AND display.displayid = lkdisplaydg.displayid
AND displaygroup.isDisplayspecific =0;