<?php
// Generated automatically by phpdaogen.
// Do NOT edit this file.
// Any changes made to this file will be overwritten the next time it is generated.

if (!class_exists('Display', false)) include dirname(dirname(__FILE__)).'/Display.class.php';

abstract class DisplayDAOAbstract {
	public static $ALLOWED_QUERY_OPERATORS = array('=', '<>', '<', '<=', '>', '>=', 'beginsWith', 'contains', 'endsWith');
	public static $ALLOWED_NUMERIC_QUERY_OPERATORS = array('=', '<>', '<', '<=', '>', '>=');
	public static $ALLOWED_STRING_QUERY_OPERATORS = array('=', '<>', '<', '<=', '>', '>=', 'beginsWith', 'contains', 'endsWith');
	public static $ALLOWED_BINARY_QUERY_OPERATORS = array('=', '<>');
	protected $connection;
	protected $cache = null;

	public function __construct($connection, $cache = null) {
		$this->connection = $connection;
		$this->cache = $cache;
	}

	public function getCache() {
		return $this->cache;
	}

	public function setCache($cache) {
		$this->cache = $cache;
	}

	public function insert(&$display) {
		$ps = new PreparedStatement("insert into display (isAuditing, display, defaultlayoutid, license, licensed, loggedin, lastaccessed, inc_schedule, email_alert, alert_timeout, ClientAddress, MediaInventoryStatus, MediaInventoryXml, MacAddress, LastChanged, NumberOfMacAddressChanges, LastWakeOnLanCommandSent, WakeOnLan, WakeOnLanTime, BroadCastAddress, SecureOn, Cidr) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		$ps->setInt($display->isAuditing);
		$ps->setString($display->display);
		$ps->setInt($display->defaultlayoutid);
		$ps->setString($display->license);
		$ps->setInt($display->licensed);
		$ps->setInt($display->loggedin);
		$ps->setInt($display->lastaccessed);
		$ps->setInt($display->inc_schedule);
		$ps->setInt($display->email_alert);
		$ps->setInt($display->alert_timeout);
		$ps->setString($display->ClientAddress);
		$ps->setInt($display->MediaInventoryStatus);
		$ps->setString($display->MediaInventoryXml);
		$ps->setString($display->MacAddress);
		$ps->setInt($display->LastChanged);
		$ps->setInt($display->NumberOfMacAddressChanges);
		$ps->setInt($display->LastWakeOnLanCommandSent);
		$ps->setInt($display->WakeOnLan);
		$ps->setString($display->WakeOnLanTime);
		$ps->setString($display->BroadCastAddress);
		$ps->setString($display->SecureOn);
		$ps->setInt($display->Cidr);
		$result = $this->connection->executeUpdate($ps);
		$display->displayid = $this->connection->getLastInsertId();
		return $result;
	}

	public function update($display) {
		$ps = new PreparedStatement("update display set isAuditing = ?, display = ?, defaultlayoutid = ?, license = ?, licensed = ?, loggedin = ?, lastaccessed = ?, inc_schedule = ?, email_alert = ?, alert_timeout = ?, ClientAddress = ?, MediaInventoryStatus = ?, MediaInventoryXml = ?, MacAddress = ?, LastChanged = ?, NumberOfMacAddressChanges = ?, LastWakeOnLanCommandSent = ?, WakeOnLan = ?, WakeOnLanTime = ?, BroadCastAddress = ?, SecureOn = ?, Cidr = ? where displayid = ?");
		$ps->setInt($display->isAuditing);
		$ps->setString($display->display);
		$ps->setInt($display->defaultlayoutid);
		$ps->setString($display->license);
		$ps->setInt($display->licensed);
		$ps->setInt($display->loggedin);
		$ps->setInt($display->lastaccessed);
		$ps->setInt($display->inc_schedule);
		$ps->setInt($display->email_alert);
		$ps->setInt($display->alert_timeout);
		$ps->setString($display->ClientAddress);
		$ps->setInt($display->MediaInventoryStatus);
		$ps->setString($display->MediaInventoryXml);
		$ps->setString($display->MacAddress);
		$ps->setInt($display->LastChanged);
		$ps->setInt($display->NumberOfMacAddressChanges);
		$ps->setInt($display->LastWakeOnLanCommandSent);
		$ps->setInt($display->WakeOnLan);
		$ps->setString($display->WakeOnLanTime);
		$ps->setString($display->BroadCastAddress);
		$ps->setString($display->SecureOn);
		$ps->setInt($display->Cidr);
		$ps->setInt($display->displayid);
		return $this->connection->executeUpdate($ps);
	}

	public function delete($displayid) {
		$ps = new PreparedStatement("delete from display where displayid = ?");
		$ps->setInt($displayid);
		return $this->connection->executeUpdate($ps);
	}

	public function load($displayid) {
		$ps = new PreparedStatement("select * from display where displayid = ?", 0, 1);
		$ps->setInt($displayid);
		$rows = $this->findWithPreparedStatement($ps);
		if (count($rows) > 0) return $rows[0];
		return false;
	}

	public function findByDisplayidPS($displayid, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from display where displayid $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($displayid);
		return $ps;
	}

	public function findByDisplayid($displayid, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByDisplayidPS($displayid, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByIsAuditingPS($isAuditing, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from display where isAuditing $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($isAuditing);
		return $ps;
	}

	public function findByIsAuditing($isAuditing, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByIsAuditingPS($isAuditing, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByDisplayPS($display, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_STRING_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_STRING_QUERY_OPERATORS[0];
		if (($queryOperator == 'beginsWith') || ($queryOperator == 'endsWith') || ($queryOperator == 'contains')) {
			$sqlQueryOperator = $this->connection->likeOperator;
			$needLower = (!$this->connection->hasCaseInsensitiveLike);
		} else {
			$sqlQueryOperator = $queryOperator;
			$needLower = false;
		}
		$ps = new PreparedStatement("select * from display where ".($needLower ? 'lower(display)' : 'display').' '.$sqlQueryOperator.' '.($needLower ? 'lower(?)' : '?').((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		if ($queryOperator == 'beginsWith') {
			$ps->setString($display.'%');
		} else if ($queryOperator == 'endsWith') {
			$ps->setString('%'.$display);
		} else if ($queryOperator == 'contains') {
			$ps->setString('%'.$display.'%');
		} else {
			$ps->setString($display);
		}
		return $ps;
	}

	public function findByDisplay($display, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByDisplayPS($display, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByDefaultlayoutidPS($defaultlayoutid, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from display where defaultlayoutid $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($defaultlayoutid);
		return $ps;
	}

	public function findByDefaultlayoutid($defaultlayoutid, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByDefaultlayoutidPS($defaultlayoutid, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByLicensePS($license, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_STRING_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_STRING_QUERY_OPERATORS[0];
		if (($queryOperator == 'beginsWith') || ($queryOperator == 'endsWith') || ($queryOperator == 'contains')) {
			$sqlQueryOperator = $this->connection->likeOperator;
			$needLower = (!$this->connection->hasCaseInsensitiveLike);
		} else {
			$sqlQueryOperator = $queryOperator;
			$needLower = false;
		}
		$ps = new PreparedStatement("select * from display where ".($needLower ? 'lower(license)' : 'license').' '.$sqlQueryOperator.' '.($needLower ? 'lower(?)' : '?').((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		if ($queryOperator == 'beginsWith') {
			$ps->setString($license.'%');
		} else if ($queryOperator == 'endsWith') {
			$ps->setString('%'.$license);
		} else if ($queryOperator == 'contains') {
			$ps->setString('%'.$license.'%');
		} else {
			$ps->setString($license);
		}
		return $ps;
	}

	public function findByLicense($license, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByLicensePS($license, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByLicensedPS($licensed, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from display where licensed $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($licensed);
		return $ps;
	}

	public function findByLicensed($licensed, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByLicensedPS($licensed, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByLoggedinPS($loggedin, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from display where loggedin $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($loggedin);
		return $ps;
	}

	public function findByLoggedin($loggedin, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByLoggedinPS($loggedin, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByLastaccessedPS($lastaccessed, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from display where lastaccessed $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($lastaccessed);
		return $ps;
	}

	public function findByLastaccessed($lastaccessed, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByLastaccessedPS($lastaccessed, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByInc_schedulePS($inc_schedule, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from display where inc_schedule $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($inc_schedule);
		return $ps;
	}

	public function findByInc_schedule($inc_schedule, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByInc_schedulePS($inc_schedule, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByEmail_alertPS($email_alert, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from display where email_alert $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($email_alert);
		return $ps;
	}

	public function findByEmail_alert($email_alert, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByEmail_alertPS($email_alert, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByAlert_timeoutPS($alert_timeout, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from display where alert_timeout $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($alert_timeout);
		return $ps;
	}

	public function findByAlert_timeout($alert_timeout, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByAlert_timeoutPS($alert_timeout, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByClientAddressPS($ClientAddress, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_STRING_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_STRING_QUERY_OPERATORS[0];
		if (($queryOperator == 'beginsWith') || ($queryOperator == 'endsWith') || ($queryOperator == 'contains')) {
			$sqlQueryOperator = $this->connection->likeOperator;
			$needLower = (!$this->connection->hasCaseInsensitiveLike);
		} else {
			$sqlQueryOperator = $queryOperator;
			$needLower = false;
		}
		$ps = new PreparedStatement("select * from display where ".($needLower ? 'lower(ClientAddress)' : 'ClientAddress').' '.$sqlQueryOperator.' '.($needLower ? 'lower(?)' : '?').((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		if ($queryOperator == 'beginsWith') {
			$ps->setString($ClientAddress.'%');
		} else if ($queryOperator == 'endsWith') {
			$ps->setString('%'.$ClientAddress);
		} else if ($queryOperator == 'contains') {
			$ps->setString('%'.$ClientAddress.'%');
		} else {
			$ps->setString($ClientAddress);
		}
		return $ps;
	}

	public function findByClientAddress($ClientAddress, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByClientAddressPS($ClientAddress, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByMediaInventoryStatusPS($MediaInventoryStatus, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from display where MediaInventoryStatus $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($MediaInventoryStatus);
		return $ps;
	}

	public function findByMediaInventoryStatus($MediaInventoryStatus, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByMediaInventoryStatusPS($MediaInventoryStatus, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByMediaInventoryXmlPS($MediaInventoryXml, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_STRING_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_STRING_QUERY_OPERATORS[0];
		if (($queryOperator == 'beginsWith') || ($queryOperator == 'endsWith') || ($queryOperator == 'contains')) {
			$sqlQueryOperator = $this->connection->likeOperator;
			$needLower = (!$this->connection->hasCaseInsensitiveLike);
		} else {
			$sqlQueryOperator = $queryOperator;
			$needLower = false;
		}
		$ps = new PreparedStatement("select * from display where ".($needLower ? 'lower(MediaInventoryXml)' : 'MediaInventoryXml').' '.$sqlQueryOperator.' '.($needLower ? 'lower(?)' : '?').((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		if ($queryOperator == 'beginsWith') {
			$ps->setString($MediaInventoryXml.'%');
		} else if ($queryOperator == 'endsWith') {
			$ps->setString('%'.$MediaInventoryXml);
		} else if ($queryOperator == 'contains') {
			$ps->setString('%'.$MediaInventoryXml.'%');
		} else {
			$ps->setString($MediaInventoryXml);
		}
		return $ps;
	}

	public function findByMediaInventoryXml($MediaInventoryXml, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByMediaInventoryXmlPS($MediaInventoryXml, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByMacAddressPS($MacAddress, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_STRING_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_STRING_QUERY_OPERATORS[0];
		if (($queryOperator == 'beginsWith') || ($queryOperator == 'endsWith') || ($queryOperator == 'contains')) {
			$sqlQueryOperator = $this->connection->likeOperator;
			$needLower = (!$this->connection->hasCaseInsensitiveLike);
		} else {
			$sqlQueryOperator = $queryOperator;
			$needLower = false;
		}
		$ps = new PreparedStatement("select * from display where ".($needLower ? 'lower(MacAddress)' : 'MacAddress').' '.$sqlQueryOperator.' '.($needLower ? 'lower(?)' : '?').((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		if ($queryOperator == 'beginsWith') {
			$ps->setString($MacAddress.'%');
		} else if ($queryOperator == 'endsWith') {
			$ps->setString('%'.$MacAddress);
		} else if ($queryOperator == 'contains') {
			$ps->setString('%'.$MacAddress.'%');
		} else {
			$ps->setString($MacAddress);
		}
		return $ps;
	}

	public function findByMacAddress($MacAddress, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByMacAddressPS($MacAddress, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByLastChangedPS($LastChanged, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from display where LastChanged $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($LastChanged);
		return $ps;
	}

	public function findByLastChanged($LastChanged, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByLastChangedPS($LastChanged, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByNumberOfMacAddressChangesPS($NumberOfMacAddressChanges, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from display where NumberOfMacAddressChanges $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($NumberOfMacAddressChanges);
		return $ps;
	}

	public function findByNumberOfMacAddressChanges($NumberOfMacAddressChanges, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByNumberOfMacAddressChangesPS($NumberOfMacAddressChanges, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByLastWakeOnLanCommandSentPS($LastWakeOnLanCommandSent, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from display where LastWakeOnLanCommandSent $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($LastWakeOnLanCommandSent);
		return $ps;
	}

	public function findByLastWakeOnLanCommandSent($LastWakeOnLanCommandSent, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByLastWakeOnLanCommandSentPS($LastWakeOnLanCommandSent, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByWakeOnLanPS($WakeOnLan, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from display where WakeOnLan $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($WakeOnLan);
		return $ps;
	}

	public function findByWakeOnLan($WakeOnLan, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByWakeOnLanPS($WakeOnLan, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByWakeOnLanTimePS($WakeOnLanTime, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_STRING_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_STRING_QUERY_OPERATORS[0];
		if (($queryOperator == 'beginsWith') || ($queryOperator == 'endsWith') || ($queryOperator == 'contains')) {
			$sqlQueryOperator = $this->connection->likeOperator;
			$needLower = (!$this->connection->hasCaseInsensitiveLike);
		} else {
			$sqlQueryOperator = $queryOperator;
			$needLower = false;
		}
		$ps = new PreparedStatement("select * from display where ".($needLower ? 'lower(WakeOnLanTime)' : 'WakeOnLanTime').' '.$sqlQueryOperator.' '.($needLower ? 'lower(?)' : '?').((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		if ($queryOperator == 'beginsWith') {
			$ps->setString($WakeOnLanTime.'%');
		} else if ($queryOperator == 'endsWith') {
			$ps->setString('%'.$WakeOnLanTime);
		} else if ($queryOperator == 'contains') {
			$ps->setString('%'.$WakeOnLanTime.'%');
		} else {
			$ps->setString($WakeOnLanTime);
		}
		return $ps;
	}

	public function findByWakeOnLanTime($WakeOnLanTime, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByWakeOnLanTimePS($WakeOnLanTime, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByBroadCastAddressPS($BroadCastAddress, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_STRING_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_STRING_QUERY_OPERATORS[0];
		if (($queryOperator == 'beginsWith') || ($queryOperator == 'endsWith') || ($queryOperator == 'contains')) {
			$sqlQueryOperator = $this->connection->likeOperator;
			$needLower = (!$this->connection->hasCaseInsensitiveLike);
		} else {
			$sqlQueryOperator = $queryOperator;
			$needLower = false;
		}
		$ps = new PreparedStatement("select * from display where ".($needLower ? 'lower(BroadCastAddress)' : 'BroadCastAddress').' '.$sqlQueryOperator.' '.($needLower ? 'lower(?)' : '?').((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		if ($queryOperator == 'beginsWith') {
			$ps->setString($BroadCastAddress.'%');
		} else if ($queryOperator == 'endsWith') {
			$ps->setString('%'.$BroadCastAddress);
		} else if ($queryOperator == 'contains') {
			$ps->setString('%'.$BroadCastAddress.'%');
		} else {
			$ps->setString($BroadCastAddress);
		}
		return $ps;
	}

	public function findByBroadCastAddress($BroadCastAddress, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByBroadCastAddressPS($BroadCastAddress, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findBySecureOnPS($SecureOn, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_STRING_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_STRING_QUERY_OPERATORS[0];
		if (($queryOperator == 'beginsWith') || ($queryOperator == 'endsWith') || ($queryOperator == 'contains')) {
			$sqlQueryOperator = $this->connection->likeOperator;
			$needLower = (!$this->connection->hasCaseInsensitiveLike);
		} else {
			$sqlQueryOperator = $queryOperator;
			$needLower = false;
		}
		$ps = new PreparedStatement("select * from display where ".($needLower ? 'lower(SecureOn)' : 'SecureOn').' '.$sqlQueryOperator.' '.($needLower ? 'lower(?)' : '?').((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		if ($queryOperator == 'beginsWith') {
			$ps->setString($SecureOn.'%');
		} else if ($queryOperator == 'endsWith') {
			$ps->setString('%'.$SecureOn);
		} else if ($queryOperator == 'contains') {
			$ps->setString('%'.$SecureOn.'%');
		} else {
			$ps->setString($SecureOn);
		}
		return $ps;
	}

	public function findBySecureOn($SecureOn, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findBySecureOnPS($SecureOn, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByCidrPS($Cidr, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from display where Cidr $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($Cidr);
		return $ps;
	}

	public function findByCidr($Cidr, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByCidrPS($Cidr, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findAllPS($orderBy = null, $offset = 0, $limit = 0) {
		$ps = new PreparedStatement("select * from display".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		return $ps;
	}

	public function findAll($orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findAllPS($orderBy, $offset, $limit));
	}

	public function findWithPreparedStatement($ps) {
		$cacheKey = null;
		if ($this->cache !== null) {
			$cacheKey = $ps->toSQL($this->connection);
			if (($rows = $this->cache->get($cacheKey)) !== false) {
				return $rows;
			}
		}
		$rows = array();
		$rs = $this->connection->executeQuery($ps);
		while ($arr = $this->connection->fetchArray($rs)) {
			$row = new Display();
			$row->loadFromArray($arr);
			$rows[] = $row;
		}
		$this->connection->freeResult($rs);
		if ($this->cache !== null) {
			$this->cache->set($cacheKey, $rows);
		}
		return $rows;
	}
}
