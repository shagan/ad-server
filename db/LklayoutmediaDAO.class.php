<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/LklayoutmediaDAOAbstract.class.php';
class LklayoutmediaDAO extends LklayoutmediaDAOAbstract {

	public function deleteAllforLayoutId($layout_id){
		$sql = "DELETE FROM lklayoutmedia where layoutID=?";
		$ps = new PreparedStatement($sql);
		$ps->setInt($layout_id);
		$this->connection->executeQuery($ps);
		
	}

}
