<?php include("kl_inc_header.php"); ?>
 <link  href="scheduler/css/scheduler.css?body=1" media="all" rel="stylesheet" />   
   <link  href="scheduler/css/jquery.ui.core.css?body=1" media="all" rel="stylesheet" />
   <link  href="scheduler/css/jquery.ui.theme.css?body=1" media="all" rel="stylesheet" />
   <link  href="scheduler/css/jquery.ui.resizable.css?body=1" media="all" rel="stylesheet" />
   <link  href="scheduler/css/jquery.ui.selectable.css?body=1" media="all" rel="stylesheet" />
   <link  href="scheduler/css/jquery.tipsy.css?body=1" media="all" rel="stylesheet" /> 
    <?php 
  include 'db/dbheader.php';
  include 'db/IAvailablesequenceDAO.php';
  include 'db/IsegmentDAO.class.php';

  $segmentDAO = new IsegmentDAO($con);
  $dataType = $segmentDAO->getType($_GET['id']);
  if($dataType=="com.instillo.layout.model.TickerSequence")
    $slotSize=40;
  else if($dataType=="com.instillo.layout.model.BannerSequence")
    $slotSize=40;
  else if($dataType=="com.instillo.layout.model.MainContentSequence")
    $slotSize=6;
  else if($dataType=="com.instillo.layout.model.SponsorBannerSequence")
    $slotSize=40;


?>
   </head>
<body>
<?php include("kl_inc_navbar.php"); ?>
<?php include("kl_inc_sidebar.php"); ?>
  <div class="main-content">
    <div class="breadcrumbs" id="breadcrumbs"> 
      <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
          </script>
      <?php include("kl_inc_breadcrumbs.php"); ?>
      
      
      <?php include("kl_inc_nav_search.php"); ?></div>
  
      <!--/.page-header-->
      
      <div class="row-fluid">
        
          <!--PAGE CONTENT BEGINS-->
          
          <div class="span12"> 
          <div id="scheduler_buttons">
    <a data-toggle="modal" class="btn blue"  role="button" href="#modal-form"> <i class="icon-plus"></i>existing AD</a>
    <a class="btn blue" href="kl_ad_banner_new.php"> <i class="icon-plus"></i>new AD</a>

</div>
        
   <div id="sch-timeline">
   <div id="sch-entities-info">
      <div id="sch-section">
        <ul id="sch-entity-names">

        </ul>

      </div>
      <div class="clearfix"></div>
   </div>
   <div id="sch-scheduler">
        <div id="sch-sbody">
              <div id="sch-shead">
                    <div id="sch-months"></div>
                    <div class="clearfix"></div>
                    <div id="sch-days">
                        <div class="sch-wrapper"></div>
                        <div class="clearfix"></div>
                     </div>
                     <div id="sch-slot-records">
                         <div class="sch-wrapper"></div>
                         <div class="clearfix"></div>
                     </div>
              </div>
              <div>
                    <div class="clearfix"></div>
                    <div id="sch-entities">
                       <div id="sch-runs-container">
                       </div>
                     </div>
               </div>
          </div>
   </div>
   <div class="clearfix"></div>
</div>
<div class="sch-modal-backdrop sch-hide"></div>
<div id="sch-status"></div>
        
          <!--- beginning modal form -->

                <div id="modal-form" class="modal hide" tabindex="-1">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="blue bigger">Please fill the following form fields</h4>
                </div>

                <div class="modal-body overflow-visible">
                  <div class="row-fluid">
                    
                      <div class="control-group">
                        <label for="form-field-select-3">Select:</label>

                        <div class="controls">
                          <select class="chosen-select" data-placeholder="Choose an ad..." id="select_sequence">
                            <option value="">&nbsp;</option>
                            

                            
                          </select>
                        </div>
                      </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <button class="btn btn-small" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Cancel
                  </button>

                  <button class="btn btn-small btn-primary" id="addSequence" data-dismiss="modal">
                    <i class="icon-ok"></i>
                    Save
                  </button>
                </div>
              </div>


<!-- ending modal form code -->
          
          <!--PAGE CONTENT ENDS--> 
        </div>
        <!--/.span--> 
      </div>
    </div>
    <!--/.row-fluid--> 
  </div>
  <!--/.page-content-->
  
  <?php include("kl_inc_ace_settings.php"); ?>
</div>
<!--/.main-content-->
</div>
<!--/.main-container-->

<?php include("kl_inc_scrollup.php"); ?>

<!--basic scripts--> 

<!--[if !IE]>--> 

<script type="text/javascript">
      window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
    </script> 

<!--<![endif]--> 

<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]--> 

<script type="text/javascript">
      if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script> 
<script src="assets/js/bootstrap.min.js"></script> 

<!--page specific plugin scripts-->

    <script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="assets/js/jquery.ui.touch-punch.min.js"></script>
    <script src="assets/js/jquery.slimscroll.min.js"></script>

    <!--ace scripts-->

    <!--inline scripts related to this page-->
    <script src="assets/js/chosen.jquery.min.js"></script>
    <script>

    jQuery(function($) {
        var unassignedData;
        var assignedData;

        $("#addSequence").on('click',function(e){
            console.log($('#select_sequence').val());
            $.each( unassignedData, function( obj ) {
              if(unassignedData[obj].sequence_id==$('#select_sequence').val())
                scheduler.loadEntity(new app.models.BannerEntity({id:unassignedData[obj].sequence_id, name:unassignedData[obj].sequence_name,photo:"http://cms.instillo.com/adv2/serve_image.php?icon=true&seqid="+unassignedData[obj].sequence_id}));
            });
        });

      
        $.getJSON( "db/process_sequence.php?action=assigned&segment_id=<?php echo $_GET['id'] ?>", function( data ) {
          assignedData = data;
          $.each( data, function( obj ) {
               console.log("Sequence:"+data[obj].sequence_name);
               scheduler.loadEntity(new app.models.BannerEntity({id:data[obj].sequence_id,name:data[obj].sequence_name,photo:"http://cms.instillo.com/adv2/serve_image.php?icon=true&seqid="+data[obj].sequence_id}));
          });
          new app.views.BannerScheduler({ model: scheduler }).render();

           $.getJSON( "db/process_runs.php?action=get&startDate=<?php echo date("Y-m-d") ?>&endDate=<?php echo date('Y-m-d', strtotime($Date. ' + 90 days')) ?>&segment_id=<?php echo $_GET['id'] ?>", function( data ) {

                  $.each( data, function( obj ) {
                    console.log(data[obj].id);
                      console.log(data[obj].startDate);
                      console.log(data[obj].endDate);
                       var bannerRun = new app.models.BannerRun({
                                id:data[obj].id,
                                start:data[obj].startDate,
                                end:data[obj].endDate
                            });
                       console.log(scheduler);
                       console.log(scheduler.entities);
                       console.log(scheduler.entities.models);
                       for (var i = scheduler.entities.models.length - 1; i >= 0; i--) {
                            if(scheduler.entities.models[i].id == data[obj].sequence_id)
                                  scheduler.entities.models[i].loadRun(bannerRun);
                       };            
                  });
                  scheduler.entities.first().runs.trigger("updateSchedulerSlots");
          
        });



        });




        $.getJSON( "db/process_sequence.php?action=unassigned&segment_id=<?php echo $_GET['id'] ?>", function( data ) {
            unassignedData = data;
            $.each( data, function( obj ) {
                 console.log(data[obj].sequence_id+data[obj].sequence_name);
                  $('#select_sequence').append($('<option>', { 
                        value: data[obj].sequence_id,
                        text : data[obj].advertiser_name+"::"+data[obj].campaign_name+"::"+data[obj].sequence_name 
                  }));

            });

           $(".chosen-select").chosen(); 
                //chosen plugin inside a modal will have a zero width because the select element is originally hidden
                //and its width cannot be determined.
                //so we set the width after modal is show
                $('#modal-form').on('show', function () {
                      $(this).find('.chosen-container').each(function(){
                        $(this).find('a:first-child').css('width' , '300px');
                        $(this).find('.chosen-drop').css('width' , '310px');
                        $(this).find('.chosen-search input').css('width' , '300px');
                      });
                   });
                

            });
          
      });

      </script>
 <script type="text/javascript" src="scheduler/js/jquery.js"></script>
   <script type="text/javascript" src="scheduler/js/underscore.js"></script>
   <script type="text/javascript" src="scheduler/js/backbone.js"></script>
   
   <script type="text/javascript" src="scheduler/js/jquery.ui.core.js"></script>
   <script type="text/javascript" src="scheduler/js/jquery.ui.widget.js"></script>
   <script type="text/javascript" src="scheduler/js/jquery.ui.mouse.js"></script>
   
   <script type="text/javascript" src="scheduler/js/jquery.ui.draggable.js"></script>
   <script type="text/javascript" src="scheduler/js/jquery.ui.droppable.js"></script>
   <script type="text/javascript" src="scheduler/js/jquery.ui.selectable.js"></script>
   <script type="text/javascript" src="scheduler/js/jquery.ui.sortable.js"></script>
   <script type="text/javascript" src="scheduler/js/jquery.ui.resizable.js"></script>
   <script type="text/javascript" src="scheduler/js/jquery.tipsy.js"></script>
   <script type="text/javascript" src="scheduler/js/scheduler.js"></script>
<script>
  var scheduler;
  $(function(){
      /*
       * set of entities
       */
    
     
 
     //Listening to entity events
     app.models.Entity.events._bind("sort",function( event, entities ){
       console.log("entity sorted-------------"); 
       var order=scheduler.entities.models.map(function(elem){return elem.id;}).join(",");
       console.log("Order:"+order);
       $.ajax({
                type:"post",
                url:"db/process_sequence.php",
                data:"action=sort&segment_id=<?php echo $_GET['id'] ?>&data="+order,
                success:function(data){
                   console.log(data);
                }

          });
        console.log("-----------------");
     });
     app.models.Entity.events._bind("delete", function( event, entity ){
          console.log("entity remove-------------");
          console.log(entity.attributes);
          console.log("-----------------"); 
           console.log("action=delete&sequence_id="+entity.attributes.id+"&segment_id=<?php echo $_GET['id'] ?>");
          $.ajax({
             type: "POST",
             url: "db/process_sequence.php",
             data: "action=delete&sequence_id="+entity.attributes.id+"&segment_id=<?php echo $_GET['id'] ?>"
             }).done(function(e) {
                     console.log("done"+e);
             });
         var order=scheduler.entities.models.map(function(elem){return elem.id;}).join(",");
         console.log("Order:"+order);
         $.ajax({
                  type:"post",
                  url:"db/process_sequence.php",
                  data:"action=sort&segment_id=<?php echo $_GET['id'] ?>&data="+order,
                  success:function(data){
                     console.log(data);
                  }

            });
          console.log("-----------------");              
     });

     //Listening to run events
     app.models.Run.events._bind("update",function(event,run){
         console.log("run update-----------------");
         console.log(run.attributes);
         console.log( "action=update&id="+run.attributes.id+"&sequence_id="+run.attributes.entity.id+"&segment_id=<?php echo $_GET['id'] ?>&startDate="+run.attributes.start+"&endDate="+run.attributes.end);
          $.ajax({
             type: "POST",
             url: "db/process_runs.php",
             data: "action=update&id="+run.attributes.id+"&sequence_id="+run.attributes.entity.id+"&segment_id=<?php echo $_GET['id'] ?>&startDate="+run.attributes.start+"&endDate="+run.attributes.end 
             })
            .done(function(e) {
                     console.log("done"+e);
             });
          var order=scheduler.entities.models.map(function(elem){return elem.id;}).join(",");
          console.log("Order:"+order);
          $.ajax({
                  type:"post",
                  url:"db/process_sequence.php",
                  data:"action=sort&segment_id=<?php echo $_GET['id'] ?>&data="+order,
                  success:function(data){
                     console.log(data);
                  }

            });
         console.log("-----------------");
     });
    app.models.Run.events._bind("delete",function(event, run){
        console.log("run delete--------------");
        console.log(run.attributes);
        console.log("action=delete&id="+run.attributes.id);
        $.ajax({
             type: "POST",
             url: "db/process_runs.php",
             data: "action=delete&id="+run.attributes.id }).done(function(e) {
                     console.log("delete done"+e);
             });
        var order=scheduler.entities.models.map(function(elem){return elem.id;}).join(",");
        console.log("Order:"+order);
        $.ajax({
                  type:"post",
                  url:"db/process_sequence.php",
                  data:"action=sort&segment_id=<?php echo $_GET['id'] ?>&data="+order,
                  success:function(data){
                     console.log(data);
                  }

            });
        console.log("------------------------");
     });
     app.models.Run.events._bind("create", function( event, run ){
          console.log("run create-------------");
          console.log(run.attributes);
          console.log("action=post&sequence_id="+run.attributes.entity.id+"&segment_id=<?php echo $_GET['id'] ?>&startDate="+run.attributes.start+"&endDate="+run.attributes.end);
          $.ajax({
             type: "POST",
             url: "db/process_runs.php",
             data: "action=create&sequence_id="+run.attributes.entity.id+"&segment_id=<?php echo $_GET['id'] ?>&startDate="+run.attributes.start+"&endDate="+run.attributes.end }).done(function(e) {
                     console.log("done"+e);
                     run.set( { id:e } );
             });
          var order=scheduler.entities.models.map(function(elem){return elem.id;}).join(",");
          console.log("Order:"+order);
          $.ajax({
                  type:"post",
                  url:"db/process_sequence.php",
                  data:"action=sort&segment_id=<?php echo $_GET['id'] ?>&data="+order,
                  success:function(data){
                     console.log(data);
                  }

            });
          console.log("run create");       
     });
 
     //listening to repeat events
     app.models.Repeat.events._bind("create", function( event, repeat,root,runs ){
          console.log("repeat create event");
          console.log(repeat.attributes);
          console.log(root.attributes);
          console.log(runs);
          console.log("end of repeat create event");
     })
     app.models.Repeat.events._bind("delete", function( event, repeat, runs ){
          console.log("repeat delete event");
          console.log(repeat.attributes);
          console.log(runs);
          console.log("end of repeat delete event ");
     })    
     app.models.Repeat.events._bind("detach", function( event, repeat, run ){
          console.log("repeat detach event");
          console.log(repeat.attributes);
          console.log(run);
          console.log("end of repeat detach event ");
     })  
     /*
      * create a media scheduler
      */
     scheduler = new app.models.BannerScheduler({ entitySortable : "on",slotSize:<?php echo $slotSize ?>, startDate:"<?php echo date("Y-m-d") ?>",endDate:"<?php echo date('Y-m-d', strtotime($Date. ' + 90 days')) ?>"});
     /*
      * add entities to the scheduler
      */
  


  
  });
</script>

    


</body>
</html>
