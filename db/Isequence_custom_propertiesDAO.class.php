<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/Isequence_custom_propertiesDAOAbstract.class.php';
class Isequence_custom_propertiesDAO extends Isequence_custom_propertiesDAOAbstract {

	public function updatevalue($isequence_custom_properties) {
	    $ps=new PreparedStatement("INSERT INTO isequence_custom_properties (id, property, value) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE value=?");
	    $ps->setInt($isequence_custom_properties->id);
	    $ps->setString($isequence_custom_properties->property);
	    $ps->setString($isequence_custom_properties->value);
	    $ps->setString($isequence_custom_properties->value);
	    return $this->connection->executeUpdate($ps);
	}

	public function findByIdAndProperty($id,$property){
		$sql = "SELECT * FROM isequence_custom_properties WHERE id=? AND property=?";
		$ps = new PreparedStatement($sql);
		$ps->setInt($id);
	    $ps->setString($property);
		return parent::findWithPreparedStatement($ps);
	}

}
