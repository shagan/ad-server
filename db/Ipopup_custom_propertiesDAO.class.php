<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/Ipopup_custom_propertiesDAOAbstract.class.php';
class Ipopup_custom_propertiesDAO extends Ipopup_custom_propertiesDAOAbstract {

	public function updatevalue($ipopup_custom_properties) {
	    $ps=new PreparedStatement("INSERT INTO ipopup_custom_properties (id, property, value) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE value=?");
	    $ps->setInt($ipopup_custom_properties->id);
	    $ps->setString($ipopup_custom_properties->property);
	    $ps->setString($ipopup_custom_properties->value);
	    $ps->setString($ipopup_custom_properties->value);
	    return $this->connection->executeUpdate($ps);
	}

	public function findByIdAndProperty($id,$property){
		$sql = "SELECT * FROM ipopup_custom_properties WHERE id=? AND property=?";
		$ps = new PreparedStatement($sql);
		$ps->setInt($id);
	    $ps->setString($property);
		return parent::findWithPreparedStatement($ps);
	}

}
