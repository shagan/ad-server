<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/IsegmentDAOAbstract.class.php';
class IsegmentDAO extends IsegmentDAOAbstract {

		public function findByRegion_id($id){
			$sql = "SELECT isegment.id, isegment.name FROM isegment,ilkregionsegment where region_id=? and isegment.id=ilkregionsegment.segment_id order by displayorder asc;";
			$ps = new PreparedStatement($sql);
			$ps->setInt($id);
			return parent::findWithPreparedStatement($ps);
		}

		public function getType($id){
			$sql = "select datatype from iregion where id in ( select region_id from ilkregionsegment where segment_id=?)";
			$ps = new PreparedStatement($sql);
			$ps->setInt($id);
			$rows = array();
			$rs = $this->connection->executeQuery($ps);
			while ($arr = $this->connection->fetchArray($rs)) {
				$rows[] = $arr;
			}
			$this->connection->freeResult($rs);
			return $rows[0]["datatype"];
		}
}
