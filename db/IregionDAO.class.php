<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/IregionDAOAbstract.class.php';
class IregionDAO extends IregionDAOAbstract {

	public function findByDisplayGroup_Id($id){
		$sql = "select * from iregion where layout_id = (select DefaultLayout from displaygroup where DisplayGroupID=?);";
		$ps = new PreparedStatement($sql);
		$ps->setInt($id);
		return parent::findWithPreparedStatement($ps);
	}
	
}
