<?php // kl_ads.php
include("kl_inc_header.php"); ?>

	<body>

<?php include("kl_inc_navbar.php"); ?>
<?php 
	include 'db/dbheader.php';
	include 'db/Isequence.class.php';
	include 'db/IsequenceDAO.class.php';

	// Get the locations where the media is stored from the setting table

	$sequenceDAO=new IsequenceDAO($con);
	$sequences=$sequenceDAO->findByCampaign_Id($_GET["campaign_id"]);
	$seqArray = array();
	foreach ($sequences as $sequence) {
		if(!array_key_exists($sequence->type, $seqArray))
			$seqArray[$sequence->type] = array();
		array_push($seqArray[$sequence->type],$sequence);
    }
		
?>


<?php include("kl_inc_sidebar.php"); ?>

                    

			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<script type="text/javascript">
						try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
					</script>

			
<?php include("kl_inc_breadcrumbs.php"); ?>

					<?php include("kl_inc_nav_search.php"); ?></div>

				<div class="page-content">
                
                					<div class="page-header position-relative">
						<h1>
							All Ads<small>
								<i class="icon-double-angle-right"></i>
								<?php echo $_GET["campaign_name"] ?> 
							</small>
						</h1>
					</div><!--/.page-header-->
                
					<div class="row-fluid">
						<div class="span12">

							
add /  / delete / filter buttons go here

						</div><!--/.span-->
					</div><!--/.row-fluid-->
					
					<div class="row-fluid">
					<div class="span12">
					
				
						<div class="row-fluid">
						
						<!-- begin widget>= -->
							<div class="span6 widget-container-span ui-sortable">
									
									
									<?php

								foreach (array_keys($seqArray) as $seq_group_keys) {

									?>

									<div class="widget-box">
										<div class="widget-header">
											<h5><?php $chk = substr($seq_group_keys, 6 + strpos($seq_group_keys, 'model.')); echo $chk; ?></h5>
											
											<div class="widget-toolbar">
											
                                            
											<a href="kl_ad_banner_new.php?campaign_id=<?php echo $_GET['campaign_id'].
											'&advertiser_id='.$_GET['advertiser_id']; ?>" 
                                            class="btn btn-small btn-success"><i class="icon-plus">
                                            </i> New <?php
											if ($chk == 'BannerSequence') {echo 'Banner';}
											if ($chk == 'MainContentSequence') {echo 'Main';}
											if ($chk == 'TickerSequence') {echo 'Ticker';}
											if ($chk == 'SponsorBannerSequence') {echo 'Sponsor';}
											?> Ad</a>
											</div>

										</div>

										<div class="widget-body">
											<div class="widget-main">
										
										
										
									<div class="row-fluid">
										<ul class="ace-thumbnails">
										
										<?php

											foreach ($seqArray[$seq_group_keys] as $sequence) {

										?>

										
										<!--begin advert item-->
										<li>
												<a data-rel="colorbox" href="http://cms.instillo.com/adv2/serve_image.php?seqid=<?php echo $sequence->id ?>" class="cboxElement">
                                                    <img width="150" src="http://cms.instillo.com/adv2/serve_image.php?thumb=true&seqid=<?php echo $sequence->id ?>">
                                                    <div class="normal-icon">
														<div class="inner"><a href=""></a></div>
													</div>
												</a>
												
												<div class="tools tools-bottom">
													<a href="kl_ad_banner_new.php?campaign_id=<?php echo $_GET['campaign_id'].
										        	'&advertiser_id='.$_GET['advertiser_id'].'&addedit=edit&sequence_id='.$sequence->id; ?>" 
                                                    class="btn-success" title="">
														<i class="icon-pencil"></i>
													</a>
		
													<a href="popupgraph.php?sequence_id=<?php echo $sequence->id; ?>" class="btn-pink" title="Schedule">
														<i class="icon-tasks"></i>
														<?php echo $sequence->id; ?>
										
													</a>
		
													<a href="#" class="btn-danger" title="Delete">
														<i class="icon-trash"></i>
													</a>
		
													</div>
									</li>
									<!--end advert item-->

									<?php
								}
									?>
									
																			
									
					
								
										
										
										
										</ul>
									</div>
								
										
										
							
											</div>
										</div>
									</div>
									
									<?php
								}
								?>
									
									
									
									
								</div>
								
								<!--end widget -->

<!-- begin widget>= --><!--end widget -->


					
					</div><!--/.span6-->
	
					</div>
	
					
				</div><!--/.page-content-->

<?php include("kl_inc_ace_settings.php"); ?>




			</div><!--/.main-content-->
		</div><!--/.main-container-->

		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
			<i class="icon-double-angle-up icon-only bigger-110"></i>
		</a>

		<!--basic scripts-->

		<!--[if !IE]>-->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!--<![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>

		<!--page specific plugin scripts-->
		<script src="assets/js/jquery.colorbox-min.js"></script>
		<!--ace scripts-->

		<script src="assets/js/ace-elements.min.js"></script>
		<script src="assets/js/ace.min.js"></script>

		<!--inline scripts related to this page-->
		<script>
		jQuery(function($) {
		var colorbox_params = {
                    reposition:true,
                    scalePhotos:true,
                    scrolling:false,
                    previous:'<i class="icon-arrow-left"></i>',
                    next:'<i class="icon-arrow-right"></i>',
                    close:'&times;',
                    current:'{current} of {total}',
                    maxWidth:'100%',
                    maxHeight:'100%',
                    onOpen:function(){
                        document.body.style.overflow = 'hidden';
                    },
                    onClosed:function(){
                        document.body.style.overflow = 'auto';
                    },
                    onComplete:function(){
                        $.colorbox.resize();
                    }
                };

                $('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);

            });
		</script>
	</body>
</html>
