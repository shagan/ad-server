<?php include("kl_inc_header.php"); ?>
<?php
	include 'db/dbheader.php';
	include 'db/Displaygroup.class.php';
	include 'db/DisplaygroupDAO.class.php';


	$displaygroupDAO=new DisplaygroupDAO($con);
	$displaygroup = $displaygroupDAO->load($_GET["id"]);
?>
<style>
#myModal {
	margin: 0px 0 0 -380px;
width: 900px; /* PLAY THE WITH THE VALUES TO SEE GET THE DESIRED EFFECT */
}
.modal-body{
	max-height: 1200px;
}

#loading {
    width: 300px;
    padding: 20px;
    background: orange;
    color: white;
    text-align: center;
    margin: 0 auto;
    display: none;
}
</style>
	<body>


<?php include("kl_inc_navbar.php"); ?>



<?php include("kl_inc_sidebar.php"); ?>

                    

			<div class="main-content">


<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Remote Management</h3>
	</div>
	<div class="modal-body">
		<div id='loading'>Page is loading...</div>
      <iframe src="" style="zoom:0.60" width="99.6%" height="1100" frameborder="0"></iframe>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal">OK</button>
	</div>
</div>
				<div class="breadcrumbs" id="breadcrumbs">
					<script type="text/javascript">
						try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
					</script>

			
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="#">Home</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
								<a href="kl_groups.php">Display Groups</a>
							</span>
						<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
								<?php echo $displaygroup->DisplayGroup ?>
						</span></li>
					</ul>

				</div>

				<div class="page-content">
                			<!--/.page-header-->


							<div class="row-fluid">
								<h3 class="header smaller lighter blue">Displays for Display Group: <b><?php echo $displaygroup->DisplayGroup ?></b></h3>
								<!-- <div class="table-header">
									Results for "Latest Registered Domains"
								</div> -->

								
								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>ID</th>
											<th>Display Name</th>
											<th>Screenshot</th>
											<th class="hidden-480">MAC Address</th>
											<th class="hidden-480">Client Address</th>
											<th>Status</th>	
											<th class="hidden-phone">
												<i class="icon-time bigger-110 hidden-phone"></i>
												Last Accessed
											</th>
											<th>Sync</th>										
											
											<th></th>
										</tr>
									</thead>

									<tbody>
										
									</tbody>
								</table>
							</div>





				</div><!--/.page-content-->

<?php include("kl_inc_ace_settings.php"); ?>




			</div><!--/.main-content-->
		</div><!--/.main-container-->

		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
			<i class="icon-double-angle-up icon-only bigger-110"></i>
		</a>

		<!--basic scripts-->

		<!--[if !IE]>-->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!--<![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>

		<!--page specific plugin scripts-->

		<script src="assets/js/jquery.dataTables.min.js"></script>
		<script src="assets/js/jquery.dataTables.bootstrap.js"></script>

		<!--ace scripts-->


		<!--inline scripts related to this page-->

		<script type="text/javascript">
	
			function showModel(src){
			    $('#myModal').on('show', function () {
			    	$('#loading').show();
			        $('iframe').attr("src",src);
			      
				});
			    $('#myModal').modal({show:true});
			    setTimeout("document.getElementById('loading').style.display=''",15000);
			};

			jQuery(function($) {

				var oTable1 = $('#sample-table-2').dataTable( {
					"iDisplayLength":50,
					"bProcessing": true,
					"bServerSide": true,
					"fnCreatedRow": function(){
						setTimeout( "$('[data-rel=tooltip]').tooltip();",1000 );
					},
					// "fnServerParams": function ( aoData ) {
     //  					aoData.push( { "name": "sSearch_8", "value": "<?php echo $_GET["id"] ?>" } );
    	// 			},
					"sAjaxSource": "db/datatable_display.php?id=<?php echo $_GET["id"] ?>",
				"aoColumns": [
			      null,null,
			      {  fnRender: set_screenshot }, null, null, {  fnRender: set_status },  {  fnRender: set_date_format },{  fnRender: set_sync },
				  { "bSortable": false, fnRender: show_icons }, { "bVisible":    false },{ "bVisible":    false }
				] } );
				
				var timeparts = [
				   {name: 'millenni', div: 31556736000, p: 'a', s: 'um'},
				   {name: 'centur', div: 3155673600, p: 'ies', s: 'y'},
				   {name: 'decade', div: 315567360},
				   {name: 'year', div: 31556736},
				   {name: 'month', div: 2629728},
				   {name: 'day', div: 86400},
				   {name: 'hour', div: 3600},
				   {name: 'minute', div: 60},
				   {name: 'second', div: 1}
				];

				function timeAgoNaive2(comparisonDate) {
				   var i = 0,
				      parts = [],
				      interval = Math.floor((new Date().getTime() - comparisonDate.getTime()) / 1000);
				   for ( ; interval > 0; i += 1) {
				      value = Math.floor(interval / timeparts[i].div);
				      interval = interval - (value * timeparts[i].div);
				      if (value) {
				         return value + ' ' + timeparts[i].name + (value != 1 ? timeparts[i].p || 's' : timeparts[i].s || '');
				      }
				   }
				   if (parts.length === 0) { return 'now'; }
				   return parts.join(', ') + ' ago';
				}

				function set_date_format(oObj){
					if(oObj.aData[oObj.aData.length-1]=="0")return "";
					var stamp = oObj.aData[oObj.iDataColumn];
					return timeAgoNaive2(new Date(stamp*1000)) + " ago";
				}
				
				function set_status(oObj){
					var status = oObj.aData[oObj.iDataColumn]; 
					if(oObj.aData[oObj.aData.length-1]=="0")return "<span class=\"label label-inverse arrowed-in\">Not Licensed</span>";
					if(status==0)
						return "<span class=\"label label-warning arrowed\">Offline</span>";
					else
						return "<span class=\"label label-success arrowed\">Online</span>";
				}

				function set_screenshot(oObj){
					var display = oObj.aData[oObj.iDataColumn];
					var src = "http://access.jabbers.ie/screenshots/"+display+"/latest";
					return "<img alt=\"No screenshot available\" width=\"150\" height=\"150\" src=\""+src+"\" />";
					
				}

				function set_sync(oObj){
					var status = oObj.aData[oObj.iDataColumn]; 
					if(oObj.aData[oObj.aData.length-1]=="0")return "<span class=\"label label-inverse arrowed-in\">Not Licensed</span>";
					if(status!=1)
						return "<span class=\"badge badge-important\">no</span>";
					else
						return "<span class=\"badge badge-success\">yes</span>";
				}
				
				function show_icons(oObj){
					console.log(oObj);
					return "<div class=\"visible-desktop btn-group\">"+
								"<button onclick=\"location.href='kl_show_display.php?id="+ oObj.aData[0]+"'\" data-rel=\"tooltip\" title=\"View\" class=\"btn btn-mini btn-info\">"+
									"<i class=\"icon-edit bigger-120\"></i>"+
								"</button>"+
								"<button onclick=\"showModel('http://access.jabbers.ie/"+ oObj.aData[1]+"/phone.html')\" data-rel=\"tooltip\" title=\"Remote Manage\" class=\"btn btn-mini btn-other\">"+
									"<i class=\"icon-laptop bigger-120\"></i>"+
								"</button>"+

								// "<button class=\"btn btn-mini btn-warning\">"+
								// 	"<i class=\"icon-flag bigger-120\"></i>"+
								// "</button>"+
							"</div>";

													

				}
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});

	
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}

				setTimeout( "$('[data-rel=tooltip]').tooltip();",1000 );
			})
			
		</script>



	</body>
</html>
