<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/IrunDAOAbstract.class.php';
class IrunDAO extends IrunDAOAbstract {

	public function find($segment_id,$startDate,$endDate){
		$sql = "SELECT * FROM irun where segment_id = ? AND startDate < ? AND ( endDate > ? OR endDate > ? );";
		$ps = new PreparedStatement($sql);
		$ps->setInt($segment_id);
		$ps->setString($endDate);
		$ps->setString($endDate);
		$ps->setString($startDate);
		return parent::findWithPreparedStatement($ps);
	}

	public function countRunsForToday($segment_id){
		$sql = "SELECT count(*) as count FROM irun WHERE segment_id =? AND ( DATE(NOW( )) =startDate OR DATE(NOW( ))= endDate  OR DATE( NOW( ) )  BETWEEN startDate AND endDate )";
		error_log($sql.":".$segment_id);
		$ps = new PreparedStatement($sql);
		$ps->setInt($segment_id);
		$rows = array();
		$rs = $this->connection->executeQuery($ps);
		while ($arr = $this->connection->fetchArray($rs)) {
			$rows[] = $arr;
		}
		$this->connection->freeResult($rs);
		error_log(json_encode($rows[0]['count']));
		return $rows[0]['count'];
	}

}
