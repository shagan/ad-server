
<div class="main-container container-fluid">
<a class="menu-toggler" id="menu-toggler" href="#"> <span class="menu-text"></span> </a>
<div class="sidebar" id="sidebar"> 
	<script type="text/javascript">
                            try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
                        </script>
	<ul class="nav nav-list">
		<li> <a href="kl_groups.php"><i class="icon-tablet"></i> <span class="menu-text">Display Groups</span></a></li>
		<li><a href="kl_advertisers.php"><i class="icon-user"></i><span class="menu-text">Advertisers + Adverts</span></a></li>
		<li><a href="kl_medias.php"><i class="icon-film"></i><span class="menu-text">Media Library</span></a></li>
	</ul>
	<!--/.nav-list-->
	
	<div class="sidebar-collapse" id="sidebar-collapse"> <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i> </div>
	<script type="text/javascript">
                            try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
                        </script> 
</div>
