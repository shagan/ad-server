<?php
// Generated automatically by phpdaogen.
// Do NOT edit this file.
// Any changes made to this file will be overwritten the next time it is generated.

if (!class_exists('Iadvertiser', false)) include dirname(dirname(__FILE__)).'/Iadvertiser.class.php';

abstract class IadvertiserDAOAbstract {
	public static $ALLOWED_QUERY_OPERATORS = array('=', '<>', '<', '<=', '>', '>=', 'beginsWith', 'contains', 'endsWith');
	public static $ALLOWED_NUMERIC_QUERY_OPERATORS = array('=', '<>', '<', '<=', '>', '>=');
	public static $ALLOWED_STRING_QUERY_OPERATORS = array('=', '<>', '<', '<=', '>', '>=', 'beginsWith', 'contains', 'endsWith');
	public static $ALLOWED_BINARY_QUERY_OPERATORS = array('=', '<>');
	protected $connection;
	protected $cache = null;

	public function __construct($connection, $cache = null) {
		$this->connection = $connection;
		$this->cache = $cache;
	}

	public function getCache() {
		return $this->cache;
	}

	public function setCache($cache) {
		$this->cache = $cache;
	}

	public function insert(&$iadvertiser) {
		$ps = new PreparedStatement("insert into iadvertiser (name, contactName, contactEmail, contactPhone) values (?, ?, ?, ?)");
		$ps->setString($iadvertiser->name);
		$ps->setString($iadvertiser->contactName);
		$ps->setString($iadvertiser->contactEmail);
		$ps->setString($iadvertiser->contactPhone);
		$result = $this->connection->executeUpdate($ps);
		$iadvertiser->id = $this->connection->getLastInsertId();
		return $result;
	}

	public function update($iadvertiser) {
		$ps = new PreparedStatement("update iadvertiser set name = ?, contactName = ?, contactEmail = ?, contactPhone = ? where id = ?");
		$ps->setString($iadvertiser->name);
		$ps->setString($iadvertiser->contactName);
		$ps->setString($iadvertiser->contactEmail);
		$ps->setString($iadvertiser->contactPhone);
		$ps->setInt($iadvertiser->id);
		return $this->connection->executeUpdate($ps);
	}

	public function delete($id) {
		$ps = new PreparedStatement("delete from iadvertiser where id = ?");
		$ps->setInt($id);
		return $this->connection->executeUpdate($ps);
	}

	public function load($id) {
		$ps = new PreparedStatement("select * from iadvertiser where id = ?", 0, 1);
		$ps->setInt($id);
		$rows = $this->findWithPreparedStatement($ps);
		if (count($rows) > 0) return $rows[0];
		return false;
	}

	public function findByIdPS($id, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from iadvertiser where id $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($id);
		return $ps;
	}

	public function findById($id, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByIdPS($id, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByNamePS($name, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_STRING_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_STRING_QUERY_OPERATORS[0];
		if (($queryOperator == 'beginsWith') || ($queryOperator == 'endsWith') || ($queryOperator == 'contains')) {
			$sqlQueryOperator = $this->connection->likeOperator;
			$needLower = (!$this->connection->hasCaseInsensitiveLike);
		} else {
			$sqlQueryOperator = $queryOperator;
			$needLower = false;
		}
		$ps = new PreparedStatement("select * from iadvertiser where ".($needLower ? 'lower(name)' : 'name').' '.$sqlQueryOperator.' '.($needLower ? 'lower(?)' : '?').((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		if ($queryOperator == 'beginsWith') {
			$ps->setString($name.'%');
		} else if ($queryOperator == 'endsWith') {
			$ps->setString('%'.$name);
		} else if ($queryOperator == 'contains') {
			$ps->setString('%'.$name.'%');
		} else {
			$ps->setString($name);
		}
		return $ps;
	}

	public function findByName($name, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByNamePS($name, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByContactNamePS($contactName, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_STRING_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_STRING_QUERY_OPERATORS[0];
		if (($queryOperator == 'beginsWith') || ($queryOperator == 'endsWith') || ($queryOperator == 'contains')) {
			$sqlQueryOperator = $this->connection->likeOperator;
			$needLower = (!$this->connection->hasCaseInsensitiveLike);
		} else {
			$sqlQueryOperator = $queryOperator;
			$needLower = false;
		}
		$ps = new PreparedStatement("select * from iadvertiser where ".($needLower ? 'lower(contactName)' : 'contactName').' '.$sqlQueryOperator.' '.($needLower ? 'lower(?)' : '?').((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		if ($queryOperator == 'beginsWith') {
			$ps->setString($contactName.'%');
		} else if ($queryOperator == 'endsWith') {
			$ps->setString('%'.$contactName);
		} else if ($queryOperator == 'contains') {
			$ps->setString('%'.$contactName.'%');
		} else {
			$ps->setString($contactName);
		}
		return $ps;
	}

	public function findByContactName($contactName, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByContactNamePS($contactName, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByContactEmailPS($contactEmail, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_STRING_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_STRING_QUERY_OPERATORS[0];
		if (($queryOperator == 'beginsWith') || ($queryOperator == 'endsWith') || ($queryOperator == 'contains')) {
			$sqlQueryOperator = $this->connection->likeOperator;
			$needLower = (!$this->connection->hasCaseInsensitiveLike);
		} else {
			$sqlQueryOperator = $queryOperator;
			$needLower = false;
		}
		$ps = new PreparedStatement("select * from iadvertiser where ".($needLower ? 'lower(contactEmail)' : 'contactEmail').' '.$sqlQueryOperator.' '.($needLower ? 'lower(?)' : '?').((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		if ($queryOperator == 'beginsWith') {
			$ps->setString($contactEmail.'%');
		} else if ($queryOperator == 'endsWith') {
			$ps->setString('%'.$contactEmail);
		} else if ($queryOperator == 'contains') {
			$ps->setString('%'.$contactEmail.'%');
		} else {
			$ps->setString($contactEmail);
		}
		return $ps;
	}

	public function findByContactEmail($contactEmail, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByContactEmailPS($contactEmail, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByContactPhonePS($contactPhone, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_STRING_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_STRING_QUERY_OPERATORS[0];
		if (($queryOperator == 'beginsWith') || ($queryOperator == 'endsWith') || ($queryOperator == 'contains')) {
			$sqlQueryOperator = $this->connection->likeOperator;
			$needLower = (!$this->connection->hasCaseInsensitiveLike);
		} else {
			$sqlQueryOperator = $queryOperator;
			$needLower = false;
		}
		$ps = new PreparedStatement("select * from iadvertiser where ".($needLower ? 'lower(contactPhone)' : 'contactPhone').' '.$sqlQueryOperator.' '.($needLower ? 'lower(?)' : '?').((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		if ($queryOperator == 'beginsWith') {
			$ps->setString($contactPhone.'%');
		} else if ($queryOperator == 'endsWith') {
			$ps->setString('%'.$contactPhone);
		} else if ($queryOperator == 'contains') {
			$ps->setString('%'.$contactPhone.'%');
		} else {
			$ps->setString($contactPhone);
		}
		return $ps;
	}

	public function findByContactPhone($contactPhone, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByContactPhonePS($contactPhone, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findAllPS($orderBy = null, $offset = 0, $limit = 0) {
		$ps = new PreparedStatement("select * from iadvertiser".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		return $ps;
	}

	public function findAll($orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findAllPS($orderBy, $offset, $limit));
	}

	public function findWithPreparedStatement($ps) {
		$cacheKey = null;
		if ($this->cache !== null) {
			$cacheKey = $ps->toSQL($this->connection);
			if (($rows = $this->cache->get($cacheKey)) !== false) {
				return $rows;
			}
		}
		$rows = array();
		$rs = $this->connection->executeQuery($ps);
		while ($arr = $this->connection->fetchArray($rs)) {
			$row = new Iadvertiser();
			$row->loadFromArray($arr);
			$rows[] = $row;
		}
		$this->connection->freeResult($rs);
		if ($this->cache !== null) {
			$this->cache->set($cacheKey, $rows);
		}
		return $rows;
	}
}
