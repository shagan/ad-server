<?php
// Generated automatically by phpdaogen.
// Do NOT edit this file.
// Any changes made to this file will be overwritten the next time it is generated.

if (!class_exists('Ilkcampaignsequence', false)) include dirname(dirname(__FILE__)).'/Ilkcampaignsequence.class.php';

abstract class IlkcampaignsequenceDAOAbstract {
	public static $ALLOWED_QUERY_OPERATORS = array('=', '<>', '<', '<=', '>', '>=', 'beginsWith', 'contains', 'endsWith');
	public static $ALLOWED_NUMERIC_QUERY_OPERATORS = array('=', '<>', '<', '<=', '>', '>=');
	public static $ALLOWED_STRING_QUERY_OPERATORS = array('=', '<>', '<', '<=', '>', '>=', 'beginsWith', 'contains', 'endsWith');
	public static $ALLOWED_BINARY_QUERY_OPERATORS = array('=', '<>');
	protected $connection;
	protected $cache = null;

	public function __construct($connection, $cache = null) {
		$this->connection = $connection;
		$this->cache = $cache;
	}

	public function getCache() {
		return $this->cache;
	}

	public function setCache($cache) {
		$this->cache = $cache;
	}

	public function insert(&$ilkcampaignsequence) {
		$ps = new PreparedStatement("insert into ilkcampaignsequence (campaign_id, sequence_id) values (?, ?)");
		$ps->setInt($ilkcampaignsequence->campaign_id);
		$ps->setInt($ilkcampaignsequence->sequence_id);
		$result = $this->connection->executeUpdate($ps);
		return $result;
	}

	public function update($ilkcampaignsequence) {
		$ps = new PreparedStatement("update ilkcampaignsequence set campaign_id = ?, sequence_id = ?");
		$ps->setInt($ilkcampaignsequence->campaign_id);
		$ps->setInt($ilkcampaignsequence->sequence_id);
		return $this->connection->executeUpdate($ps);
	}

	public function delete() {
		return true;
	}

	public function load() {
		$rows = $this->findWithPreparedStatement($ps);
		if (count($rows) > 0) return $rows[0];
		return false;
	}

	public function findByCampaign_idPS($campaign_id, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from ilkcampaignsequence where campaign_id $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($campaign_id);
		return $ps;
	}

	public function findByCampaign_id($campaign_id, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByCampaign_idPS($campaign_id, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findBySequence_idPS($sequence_id, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from ilkcampaignsequence where sequence_id $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($sequence_id);
		return $ps;
	}

	public function findBySequence_id($sequence_id, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findBySequence_idPS($sequence_id, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findAllPS($orderBy = null, $offset = 0, $limit = 0) {
		$ps = new PreparedStatement("select * from ilkcampaignsequence".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		return $ps;
	}

	public function findAll($orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findAllPS($orderBy, $offset, $limit));
	}

	public function findWithPreparedStatement($ps) {
		$cacheKey = null;
		if ($this->cache !== null) {
			$cacheKey = $ps->toSQL($this->connection);
			if (($rows = $this->cache->get($cacheKey)) !== false) {
				return $rows;
			}
		}
		$rows = array();
		$rs = $this->connection->executeQuery($ps);
		while ($arr = $this->connection->fetchArray($rs)) {
			$row = new Ilkcampaignsequence();
			$row->loadFromArray($arr);
			$rows[] = $row;
		}
		$this->connection->freeResult($rs);
		if ($this->cache !== null) {
			$this->cache->set($cacheKey, $rows);
		}
		return $rows;
	}
}
