<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/IlksegmentsequenceorderDAOAbstract.class.php';
class IlksegmentsequenceorderDAO extends IlksegmentsequenceorderDAOAbstract {

	public function sortSequences($segment_id, $order){
		$sql="";
		$i=1;
		$deletesql ="DELETE from ilksegmentsequenceorder where segment_id=$segment_id";
		$ps = new PreparedStatement($deletesql);
		$this->connection->executeQuery($ps);
		foreach ($order as $item) {
			$sql="INSERT INTO ilksegmentsequenceorder (`segment_id`,`sequence_id`,`displayorder`) values ($segment_id, $item, $i)";
			$i++;
			$ps = new PreparedStatement($sql);
			$this->connection->executeQuery($ps);
		}
		return;
	}
}
