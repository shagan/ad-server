<?php include("kl_inc_header.php"); ?>
<?php 
	include 'db/dbheader.php';
	include 'db/Iadvertiser.class.php';
	include 'db/IadvertiserDAO.class.php';
?>
	<body>

<?php include("kl_inc_navbar.php"); ?>



<?php include("kl_inc_sidebar.php"); ?>

                    

			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<script type="text/javascript">
						try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
					</script>

			
<?php include("kl_inc_breadcrumbs.php"); ?>

					<?php include("kl_inc_nav_search.php"); ?></div>

				<div class="page-content">
                
                					<div class="page-header position-relative">
						<h1>All Advertisers</h1>
					</div>
                					
				  <div class="row-fluid">
				    <table id="layout_regions" class="table table-striped table-bordered table-hover">
					<thead>
							<tr>
											<th class="center">
												<label>
													
													<span class="lbl">ID</span>
												</label>
											</th>
											<th>Name</th>
											<th></th>
										</tr>
									</thead>
					<?php
						$advertiserDAO=new IadvertiserDAO($con);
						$advertisers=$advertiserDAO->findall();
						foreach ($advertisers as $advertiser) {
							
							?>
							

									<tbody>
										<tr>
											<td class="center">
												<label>
													
													<span class="lbl"><?php echo $advertiser->id ?></span>
												</label>
											</td>

											<td>
												<a href="kl_advertiser.php?id=<?php echo $advertiser->id ?>"><h5><?php echo $advertiser->name ?></h5></a>
											</td>
											<td>
										
										<a class="btn btn-small btn-purple" href="kl_campaigns.php?advertiser_id=<?php echo $advertiser->id ?>">
											<i class="icon-inbox bigger-125"></i>
											campaigns
										</a>
										
										
										<a class="btn btn-small btn-yellow" href="kl_advertiser.php?id=<?php echo $advertiser->id ?>">
											<i class="icon-user bigger-125"></i>
											client details
										</a>

										<a class="btn btn-small btn-important" href="kl_advertiser.php?id=<?php echo $advertiser->id ?>">
											<i class="icon-user bigger-125"></i>
											delete
										</a>
											
											
											
											</td>
										</tr>



							<?php
							}
							?>
										
									</tbody>
								</table>
				  </div>






				</div><!--/.page-content-->

<?php include("kl_inc_ace_settings.php"); ?>




			</div><!--/.main-content-->
		</div><!--/.main-container-->

		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
			<i class="icon-double-angle-up icon-only bigger-110"></i>
		</a>

		<!--basic scripts-->

		<!--[if !IE]>-->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!--<![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>

		<!--page specific plugin scripts-->
        
        	<script src="assets/js/jquery.dataTables.min.js"></script>
		<script src="assets/js/jquery.dataTables.bootstrap.js"></script>

		<!--ace scripts-->

		<script src="assets/js/ace-elements.min.js"></script>
		<script src="assets/js/ace.min.js"></script>

		<!--inline scripts related to this page-->

		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#layout_regions').dataTable( );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
		</script>
	</body>
</html>
