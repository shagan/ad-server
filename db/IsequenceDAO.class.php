<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/IsequenceDAOAbstract.class.php';
class IsequenceDAO extends IsequenceDAOAbstract {

	public function findBySegment_Id($id){
		$sql = "SELECT * FROM isequence,`ilksegmentsequence` where id in ( select sequence_id from `ilksegmentsequence` WHERE segment_id=?) and `ilksegmentsequence`.sequence_id = isequence.id and `ilksegmentsequence`.segment_id=? order by `ilksegmentsequence`.displayorder";
		$ps = new PreparedStatement($sql);
		$ps->setInt($id);
		$ps->setInt($id);
		return parent::findWithPreparedStatement($ps);
	}

	public function findByCampaign_Id($id){
		$sql = "SELECT * FROM isequence where id in ( select sequence_id from `ilkcampaignsequence` WHERE campaign_id=?)";
		$ps = new PreparedStatement($sql);
		$ps->setInt($id);
		return parent::findWithPreparedStatement($ps);
	}

	public function findSequenceByTypeAndCampaign_id($id,$type){
		$sql = "SELECT * FROM isequence where type=? and id in ( select sequence_id from `ilkcampaignsequence` WHERE campaign_id=?)";
		$ps = new PreparedStatement($sql);
		$ps->setString($type);
		$ps->setInt($id);
		return parent::findWithPreparedStatement($ps);
	}
}
