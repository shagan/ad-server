<?php include("kl_inc_header.php"); ?>

<?php 
	include 'db/dbheader.php';
	include 'db/Iadvertiser.class.php';
	include 'db/IadvertiserDAO.class.php';
?>
<?php
	
	$advertiser=NULL;
	if(isset($_GET["id"])) {
		$advertiserDAO=new IadvertiserDAO($con);
		$advertiserArr=$advertiserDAO->findById($_GET["id"]);
		$advertiser=$advertiserArr[0];
	}
?>
	<body>

<?php include("kl_inc_navbar.php"); ?>



<?php include("kl_inc_sidebar.php"); ?>

                    

			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<script type="text/javascript">
						try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
					</script>

			
<?php include("kl_inc_breadcrumbs.php"); ?>

					<?php include("kl_inc_nav_search.php"); ?></div>

				<div class="page-content">
                
                					<div class="page-header position-relative">
						<h1>
							 <?php if($advertiser!=null) { echo "Edit";} else {echo "Add";} ?> Advertiser
							<small>
								<i class="icon-double-angle-right"></i>
							Set up name and contact details</small>
						</h1>
					</div><!--/.page-header-->
                
	
    <div class="row-fluid">
					
                    
          <form class="form-horizontal" id="advertiser_form" name="advertiser_form">
            <input type="hidden" id="id" name="id" <?php if($advertiser!=null) echo ' value="'.$advertiser->id.'"' ?>/>
            
            <div class="control-group">
              <label class="control-label" for="form-field-1">Advertiser Name</label>
              <div class="controls">
                <input type="text" name="name" id="name" placeholder="" <?php if($advertiser!=null) echo ' value="'.$advertiser->name.'"' ?> required/>
              </div>
            </div>
           
           
            <div class="control-group">
              <label class="control-label" for="form-field-1">Contact Name</label>
              <div class="controls">
                <input type="text" name="contactName" id="contactName" placeholder="" <?php if($advertiser!=null) echo ' value="'.$advertiser->contactName.'"' ?>/>
              </div>
            </div>
           
            <div class="control-group">
              <label class="control-label" for="form-field-1">Contact Email</label>
              <div class="controls">
                <input type="text" name="contactEmail" id="contactEmail" placeholder="" <?php if($advertiser!=null) echo ' value="'.$advertiser->contactEmail.'"' ?>/>
              </div>
            </div>
           
            <div class="control-group">
              <label class="control-label" for="form-field-1">Contact Phone</label>
              <div class="controls">
                <input type="text" name="contactPhone" id="contactPhone" placeholder="	" <?php if($advertiser!=null) echo ' value="'.$advertiser->contactPhone.'"' ?>/>
              </div>
            </div>

	
         
       
       <div class="form-actions">
									<button class="btn btn-info" type="submit" id="button" name="button" >
										<i class="icon-ok bigger-110" ></i>
										Save
									</button>
									<button class="btn btn-info" id="button" name="button" onclick="history.back()" >
										<i class="icon-ok bigger-110" ></i>
										Back
									</button>
									<p id="info"></p>

								</div>
       							
     	 </form>    
		  
		  </div> 
                    
                    
					</div>
    
    
    
				</div><!--/.page-content-->

<?php include("kl_inc_ace_settings.php"); ?>




			</div><!--/.main-content-->
		</div><!--/.main-container-->

		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
			<i class="icon-double-angle-up icon-only bigger-110"></i>
		</a>

		<!--basic scripts-->

		<!--[if !IE]>-->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!--<![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>

		<!--page specific plugin scripts-->
		<script src="assets/js/jquery.validate.min.js"></script>
		<!--ace scripts-->

		<script src="assets/js/ace-elements.min.js"></script>
				 <script type="text/javascript">
		 jQuery(function($) {
               	console.log("alerting");
               	$.fn.serializeObject = function()
				{
				    var o = {};
				    var a = this.serializeArray();
				    $.each(a, function() {
				        if (o[this.name] !== undefined) {
				            if (!o[this.name].push) {
				                o[this.name] = [o[this.name]];
				            }
				            o[this.name].push(this.value || '');
				        } else {
				            o[this.name] = this.value || '';
				        }
				    });
				    return o;
				};
               	$("#advertiser_form").validate({
							rules: {
								name: "required"
								
							},
							messages: {
								name: "Please enter your name"
					
							},
							submitHandler: function(form) {
    							console.log("submitting");
    							var formdata= JSON.stringify($("#advertiser_form").serializeObject());
 
		                          $.ajax({
		                              type:"post",
		                              url:"db/process_advertiser.php",
		                              data:"data="+formdata,
		                              success:function(data){
		                                 $("#info").html(data);
		                              }
		 
		                          });
  							}
					});

              
           })
        </script>
		<script src="assets/js/ace.min.js"></script>

		<!--inline scripts related to this page-->


	</body>
</html>
