<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/IstatstryDAOAbstract.class.php';
class IstatstryDAO extends IstatstryDAOAbstract {

	public function findBySequence_Id($id){
		$sql = "SELECT `id`, `layout_id`, `sequence_id`, `statDate`, sum(`views`), sum(`screentime`), group_concat( `custom`)as custom FROM `istatstry`  where sequence_id=? group by sequence_id, statDate";
		$ps = new PreparedStatement($sql);
		$ps->setInt($id);
		return parent::findWithPreparedStatement($ps);
	}

}
