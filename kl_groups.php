<?php include("kl_inc_header.php"); ?>
<?php 
	include 'db/dbheader.php';
	include 'db/Display.class.php';
	include 'db/DisplayDAO.class.php';
	include 'db/Displaygroup.class.php';
	include 'db/DisplaygroupDAO.class.php';
?>
<body>
<?php include("kl_inc_navbar.php"); ?>
<?php include("kl_inc_sidebar.php"); ?>
	<div class="main-content">
		<div class="breadcrumbs" id="breadcrumbs"> 
			<script type="text/javascript">
						try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
					</script>
			<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="#">Home</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>

							Display Groups
						</span></li>
					</ul>
			
		<div class="page-content">
			<div class="row-fluid">
				<div class="span12">
					<div class="span6 widget-container-span ui-sortable">
						<?php
						$displayDAO = new DisplayDAO($con);
						$displaygroupDAO=new DisplaygroupDAO($con);
						$displayGroups=$displaygroupDAO->findByIsDisplaySpecific(0);
						foreach ($displayGroups as $displayGroup) {
							$groupID = $displayGroup->DisplayGroupID;
							?>
						<div class="widget-box">
							<div class="widget-header">
								<h4>Display Group : <?php echo $displayGroup->DisplayGroup ?></h4>
								<div class="widget-toolbar"> 
								<div style="float:right;">


												<a class="btn btn-app btn-light btn-mini" href="kl_group_manage.php?id=<?php echo $groupID ?>">
													<i class="icon-tablet bigger-160"></i>
													manage
												</a>

												<a class="btn btn-app btn-pink btn-mini" href="kl_group.php?id=<?php echo $groupID ?>">
													<i class="icon-th-large bigger-160"></i>
													layout
												</a>
										</div></div>
							</div>
							<div class="widget-body">
								<div>
									<div class="widget-main">
										
									
										<div class="infobox infobox-blue">
											<div class="infobox-icon"> <i class="icon-tablet"></i> </div>
											<div class="infobox-data"> <span class="infobox-data-number"><?php echo $displayDAO->countByDisplayGroup_id($groupID) ?></span>
												<div class="infobox-content">displays</div>
											</div>
										</div>
										<div class="infobox infobox-green">
											<div class="infobox-icon"> <i class="icon-thumbs-up"></i> </div>
											<div class="infobox-data"> <span class="infobox-data-number"><?php echo $displayDAO->countOnlineByDisplayGroup_id($groupID) ?></span>
												<div class="infobox-content">displays</div>
											</div>
										</div>
										<div class="infobox infobox-red">
											<div class="infobox-icon"> <i class="icon-thumbs-down"></i> </div>
											<div class="infobox-data"> <span class="infobox-data-number"><?php echo $displayDAO->countOfflineByDisplayGroup_id($groupID) ?></span>
												<div class="infobox-content">displays</div>
											</div>
										</div>

										
										
									</div>
								</div>
							</div>
						</div>
						<br/>
						<?php 

						}
						?>
					</div>
				</div>
				<!--/.span12--> 
				
			</div>
			<!--/.page-content--> 
			
		</div>
		<!--/.page-content-->
		
		<?php include("kl_inc_ace_settings.php"); ?>
	</div>
	<!--/.main-content--> 
</div>
<!--/.main-container--> 

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse"> <i class="icon-double-angle-up icon-only bigger-110"></i> </a> 

<!--basic scripts--> 

<!--[if !IE]>--> 

<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script> 

<!--<![endif]--> 

<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]--> 

<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script> 
<script src="assets/js/bootstrap.min.js"></script> 

<!--page specific plugin scripts--> 

<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script> 
<script src="assets/js/jquery.ui.touch-punch.min.js"></script> 
<script src="assets/js/jquery.slimscroll.min.js"></script> 

<!--ace scripts--> 

<script src="assets/js/ace-elements.min.js"></script> 
<script src="assets/js/ace.min.js"></script> 

<!--inline scripts related to this page--> 

<script type="text/javascript">
			jQuery(function($) {
			
				// Portlets (boxes)
			    $('.widget-container-span').sortable({
			        connectWith: '.widget-container-span',
					items:'> .widget-box',
					opacity:0.8,
					revert:true,
					forceHelperSize:true,
					placeholder: 'widget-placeholder',
					forcePlaceholderSize:true,
					tolerance:'pointer'
			    });
			
			});
		</script>
</body>
</html>
