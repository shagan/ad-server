<?php
// process_banner.php. Author: Niall Ó Brolcháin
//
// Function to get file information
function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}

// Function to create a thumbnail
function resize($in_file, $out_file, $new_width, $new_height=FALSE)
{
	$image = null;
	$extension = strtolower(preg_replace('/^.*\./', '', $in_file));
	switch($extension)
	{
		case 'jpg':
		case 'jpeg':
			$image = imagecreatefromjpeg($in_file);
		break;
		case 'png':
			$image = imagecreatefrompng($in_file);
		break;
		case 'gif':
			$image = imagecreatefromgif($in_file);
		break;
	}
	if(!$image || !is_resource($image)) return false;


	$width = imagesx($image);
	$height = imagesy($image);
	if($new_height === FALSE)
	{
		$new_height = (int)(($height * $new_width) / $width);
	}

	
	$new_image = imagecreatetruecolor($new_width, $new_height);
	imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
//error_log('IMAGES: >>>> '.$new_image.' * '.$out_file);
	$ret = imagejpeg($new_image, $out_file, 80);

	imagedestroy($new_image);
	imagedestroy($image);

	return $ret;
}

// Include file headers fro DAO
include 'dbheader.php';
include 'Isequence.class.php';
include 'IsequenceDAO.class.php';
include 'Isequence_custom_properties.class.php';
include 'Isequence_custom_propertiesDAO.class.php';
include 'Ipopup.class.php';
include 'IpopupDAO.class.php';
include 'Ipopup_custom_properties.class.php';
include 'Ipopup_custom_propertiesDAO.class.php';
include 'Ilkbannerpopup.class.php';
include 'IlkbannerpopupDAO.class.php';
include 'Iaction.class.php';
include 'IactionDAO.class.php';
include 'Iaction_custom_properties.class.php';
include 'Iaction_custom_propertiesDAO.class.php';
include 'Ilkpopupaction.class.php';
include 'IlkpopupactionDAO.class.php';
include 'Iemail_action.class.php';
include 'Iemail_actionDAO.class.php';
include 'Isms_action.class.php';
include 'Isms_actionDAO.class.php';
include 'Iqrcode_action.class.php';
include 'Iqrcode_actionDAO.class.php';
include 'Media.class.php';
include 'MediaDAO.class.php';
include 'Ilkadvertisermedia.class.php';
include 'IlkadvertisermediaDAO.class.php';
include 'Ilkcampaignsequence.class.php';
include 'IlkcampaignsequenceDAO.class.php';
include 'Setting.class.php';
include 'SettingDAO.class.php';
include 'Ilkadvertisercampaign.class.php';
include 'IlkadvertisercampaignDAO.class.php';

// Get data data from bannaer sequence ajax request 
$obj = new DOMDocument();
if (isset($_POST['campaign_id']))   {$obj->campaign_id   = $_POST['campaign_id'];}   else {$obj->campaign_id = 0;}
if (isset($_POST['type']))          {$obj->type          = $_POST['type'];}          else {$obj->type = '';}
if (isset($_POST['name']))          {$obj->name          = $_POST['name'];}          else {$obj->name = '';}
if (isset($_POST['title']))         {$obj->title         = $_POST['title'];}         else {$obj->title = '';}
if (isset($_POST['buttonname']))    {$obj->buttonname    = $_POST['buttonname'];}    else {$obj->buttonname = '';}
if (isset($_POST['popupname']))     {$obj->popupname     = $_POST['popupname'];}     else {$obj->popupname = '';}

if (isset($_POST['video_id']))      {$obj->video_id      = $_POST['video_id'];}      else {$obj->video_id = '';}
if (isset($_POST['websitename']))   {$obj->websitename   = $_POST['websitename'];}   else {$obj->websitename = '';}
if (isset($_POST['radiopopup']))    {$obj->radiopopup    = $_POST['radiopopup'];}    else {$obj->radiopopup = 'radio_1';}  // Default value

if (isset($_POST['subtitle']))      {$obj->subtitle      = $_POST['subtitle'];}      else {$obj->subtitle = '';}
if (isset($_POST['email']))         {$obj->email         = $_POST['email'];}         else {$obj->email = '';}
if (isset($_POST['sms']))           {$obj->sms           = $_POST['sms'];}           else {$obj->sms = '';}
if (isset($_POST['qrcode']))        {$obj->qrcode        = $_POST['qrcode'];}        else {$obj->qrcode = '';}
if (isset($_POST['introtext']))     {$obj->introtext     = $_POST['introtext'];}     else {$obj->introtext = '';}
if (isset($_POST['exittext']))      {$obj->exittext      = $_POST['exittext'];}      else {$obj->exittext = '';}
if (isset($_POST['emailsubject']))  {$obj->emailsubject  = $_POST['emailsubject'];}  else {$obj->emailsubject = '';}
if (isset($_POST['emailcontent']))  {$obj->emailcontent  = $_POST['emailcontent'];}  else {$obj->emailcontent = '';}
if (isset($_POST['smscontent']))    {$obj->smscontent    = $_POST['smscontent'];}    else {$obj->smscontent = '';}
if (isset($_POST['qrcontent']))     {$obj->qrcontent     = $_POST['qrcontent'];}     else {$obj->qrcontent = '';}
if (isset($_POST['radioqrcode']))   {$obj->radioqrcode   = $_POST['radioqrcode'];}   else {$obj->radioqrcode = 'qrcheck1';}  // Default value

//findByCampaign_id
$IlkadvertisercampaignDAO = new IlkadvertisercampaignDAO($con);
$advertisers = $IlkadvertisercampaignDAO->findByCampaign_id($obj->campaign_id);
foreach ($advertisers as $advertiser) {$obj->advertiser_id = $advertiser->advertiser_id;}
//error_log('AD: '.$obj->advertiser_id.' CAM: '.$obj->campaign_id);

// Initialis media object
$media = new DOMDocument();
$media->mediaID = '';
$media->name = '';
$media->notes = '';
$media->type = '';
$media->duration = 0;
$media->mediaTags = '';
$media->originalFilename = '';
$media->storedAs = '';
$media->MD5 = '';
$media->FileSize = 0;
$media->onclick = '';
$media->userID = 0;
$media->retired = 0;
$media->isEdited = 0;
$media->editedMediaID = 0;
$mediaDAO = new MediaDAO($con);

// Get the locations where the media is stored from the setting table
$settingDAO = new SettingDAO($con);
$settings = $settingDAO->findBySetting('LIBRARY_LOCATION');
foreach ($settings as $setting) {$filedir = $setting->value;}
$settings = $settingDAO->findBySetting('THUMBNAIL_LOCATION');
foreach ($settings as $setting) {$thumbdir = $setting->value;}

// Initialise working variables
$buttonfile = '';
$popupfile  = '';
$buttontype = '';
$popuptype  = '';
$mode = 'add';
if (isset($_GET['addedit'])) {$mode = 'edit';}
if ($obj->radiopopup == 'radio_1') {$popuptype  = 'image';}
if ($obj->radiopopup == 'radio_2') {$popuptype  = 'video';}
if ($obj->radiopopup == 'radio_3') {$popuptype  = 'website';}

error_log($obj->radiopopup.' * '.$popuptype.' *videoid: '.$obj->video_id);

//////////////////////////////////
// Step 1 - Update isequence table
//////////////////////////////////
$sequenceDAO=new IsequenceDAO($con);
if ($mode == 'edit') {
	$obj->id = $_GET['sequence_id'];
	$sequenceDAO->update($obj);
} else {
	$sequenceDAO->insert($obj);
	$obj->id = mysql_insert_id();
}
$obj->sequence_id = $obj->id;


//////////////////////////////////////////////////////////////////////////////////
// Step 2 - Save button file & update isequence_custom_properties with value pairs
//////////////////////////////////////////////////////////////////////////////////

// Get the current media id for the button file if it exists
$sequencePropertiesDAO=new Isequence_custom_propertiesDAO($con);
if ($mode == 'edit') {
    $buttonids = $sequencePropertiesDAO->findByIdAndProperty($obj->sequence_id,'mediaid');
    foreach ($buttonids as $buttonid) {$media->mediaID = $buttonid->value;}
}

$userfile1 = isset($_FILES['userfile1']);

if ($userfile1) {
    $file_ary   = reArrayFiles($_FILES['userfile1']);
	
    foreach ($file_ary as $file) {

        // Update media table
		$media->name = $file['name'];
        $media->originalFilename = $file['name'];
		if (strlen($obj->buttonname) > 0) {$media->name = $obj->buttonname;}
		if ($mode == 'edit') {
            $mediaDAO->update($media);
			$media->media_id = $media->mediaID;
        } else {
            $mediaDAO->insert($media);
            $media->media_id = mysql_insert_id();
        }
		$media->mediaID  = $media->media_id;
				
        // Find media type
        $extension = strtolower(substr($file['name'], strrpos($file['name'], '.')+1));
		$media->storedAs = $media->media_id . '.' . $extension;
		$thumbnail = $media->storedAs;
		$media_type = 'mtype'; // Generic library media type
		if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'gif')
   			{$media_type = 'image';}
		if ($extension == 'mp4')
   			{$media_type = 'video';}
		$media->type = $media_type;

        // Populate file names
		$buttonfile = $media->storedAs; 
		$buttontype = $media->type;

        // Write files to Library
       	$save_path  = $filedir.$media->storedAs;
		if ($media_type == 'video') {
        	$tn_path  = '../fileupload/server/php/video.png';
		} else {
        	$tn_path  = $filedir.$media->storedAs;
		}
	    $thumb_path = $thumbdir.$media->storedAs;

	    if( ! move_uploaded_file($file['tmp_name'] , $save_path)
		    OR
		    !resize($tn_path, $thumb_path, 80) ) 
    	{
	    	$result['status'] = 'ERR';
		    $result['message'] = 'Unable to save file!'.$file['name'];
	    }

        // Calculate the MD5 and the file size
        $media->MD5 = md5_file($save_path);
        $media->FileSize = filesize($save_path);

        $mediaDAO->update($media);

        // Populate the Advertiser -> Media link table if in Add mode 
		if ($mode == 'Add') {
    		$media->advertiser_id = $obj->advertiser_id;
            $advmediaDAO = new IlkadvertisermediaDAO($con);
            $advmediaDAO->insert($media);
		}
    }
}

//////////////////////////////////////////////////////
// Update isequence_custom_properties with value pairs
//////////////////////////////////////////////////////
// Insert title
$obj->value     = $obj->title;
$obj->property  = 'title';
if ($mode == 'edit') {
    $sequencePropertiesDAO->updatevalue($obj);
} else {
    $sequencePropertiesDAO->insert($obj);
}
// Insert subtitle
$obj->value     = $obj->subtitle;
$obj->property  = 'subtitle';
if ($mode == 'edit') {
    $sequencePropertiesDAO->updatevalue($obj);
} else {
    $sequencePropertiesDAO->insert($obj);
}
// Insert buttonname
$obj->value     = $obj->buttonname;
$obj->property  = 'buttonname';
if ($mode == 'edit') {
    $sequencePropertiesDAO->updatevalue($obj);
} else {
    $sequencePropertiesDAO->insert($obj);
}
// Insert type
$obj->value     = $buttontype;
$obj->property  = 'type';
if ($mode == 'edit') {
    if ($userfile1) $sequencePropertiesDAO->updatevalue($obj);
} else {
    $sequencePropertiesDAO->insert($obj);
}
// Insert media id
$obj->value     = $media->mediaID;
$obj->property  = 'mediaid';
if ($mode == 'edit') {
    if ($userfile1) $sequencePropertiesDAO->updatevalue($obj);
} else {
    $sequencePropertiesDAO->insert($obj);
}
// Insert uri
$obj->value     = $buttonfile;
$obj->property  = 'uri';
if ($mode == 'edit') {
    if ($userfile1) $sequencePropertiesDAO->updatevalue($obj);
} else {
    $sequencePropertiesDAO->insert($obj);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////	
// Step 3 - Update popup media details and the tables ipopup, ipopup_custom_properties and ilkbannerpopup
/////////////////////////////////////////////////////////////////////////////////////////////////////////

// Create a new popup id if Add nothing required if edit
if ($mode == 'add') {
    $popupDAO=new IpopupDAO($con);
    $popupDAO->insert($obj);
    $obj->id = mysql_insert_id();
    $obj->popup_id = $obj->id;
}
// Insert into ilkbannerpopup
$ilkbannerpopupDAO=new IlkbannerpopupDAO($con);
if ($mode == 'edit') {
    $sequences = $ilkbannerpopupDAO->findBySequence_id($obj->sequence_id);
	foreach ($sequences as $sequence) {$obj->id = $sequence->popup_id; $obj->popup_id = $obj->id;}
} else {
    $ilkbannerpopupDAO->insert($obj);
}

// Get the current media id for the popup if it exists
$popupPropertiesDAO=new Ipopup_custom_propertiesDAO($con);
if ($mode == 'edit') {
    $popupids = $popupPropertiesDAO->findByIdAndProperty($obj->popup_id,'mediaid');
    foreach ($popupids as $popupid) {$media->mediaID = $popupid->value;}
    $popupids = $popupPropertiesDAO->findByIdAndProperty($obj->sequence_id,'uri');
    foreach ($popupids as $popupid) {$popupfile = $popupid->value;}
}

$userfile2 = isset($_FILES['userfile2']);

// Save popup file if uploaded
if ($userfile2) {
    $file_ary   = reArrayFiles($_FILES['userfile2']);
	
    foreach ($file_ary as $file) {

        // Update media table
		$media->name = $file['name'];
        $media->originalFilename = $file['name'];
		if (strlen($obj->popupname) > 0)   {$media->name = $obj->popupname;}
		if ($mode == 'edit') {
            $mediaDAO->update($media);
			$media->media_id = $media->mediaID;
        } else {
            $mediaDAO->insert($media);
            $media->media_id = mysql_insert_id();
        }
		$media->mediaID  = $media->media_id;
				
        // Find media type
        $extension = strtolower(substr($file['name'], strrpos($file['name'], '.')+1));
		$media->storedAs = $media->media_id . '.' . $extension;
		$thumbnail = $media->storedAs;
		$media_type = 'mtype'; // Generic library media type
		if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'gif')
   			{$media_type = 'image';}
		if ($extension == 'mp4')
   			{$media_type = 'video';}
		$media->type = $media_type;

        // Populate file names
		$popupfile  = $media->storedAs;

        // Write files to Library
       	$save_path  = $filedir.$media->storedAs;
		if ($media_type == 'video') {
        	$tn_path  = '../fileupload/server/php/video.png';
		} else {
        	$tn_path  = $filedir.$media->storedAs;
		}
	    $thumb_path = $thumbdir.$media->storedAs;

	    if( ! move_uploaded_file($file['tmp_name'] , $save_path)
		    OR
		    !resize($tn_path, $thumb_path, 80) ) 
    	{
	    	$result['status'] = 'ERR';
		    $result['message'] = 'Unable to save file!'.$file['name'];
	    }

        // Calculate the MD5 and the file size
        $media->MD5 = md5_file($save_path);
        $media->FileSize = filesize($save_path);

        $mediaDAO->update($media);

        // Populate the Advertiser -> Media link table if in Add mode 
		if ($mode == 'Add') {
    		$media->advertiser_id = $obj->advertiser_id;
            $advmediaDAO = new IlkadvertisermediaDAO($con);
            $advmediaDAO->insert($media);
		}
    }

}

$uri = $popupfile;
if ($obj->radiopopup == 'radio_2') {
    // Query the media table for the storedAs value if a video has been selected
    $row = $mediaDAO->load($obj->video_id);    
	$uri = $row->storedAs;
}
if ($obj->radiopopup == 'radio_3') {$uri = $obj->websitename;}

///////////////////////////
// ipopup custom properties
///////////////////////////

// Insert popupname into ipopup custom properties
$obj->value     = $obj->popupname;
$obj->property  = 'popupname';
if ($mode == 'edit') {
    $popupPropertiesDAO->updatevalue($obj);
} else {
    $popupPropertiesDAO->insert($obj);
}
// Insert type into ipopup custom properties
$obj->value     = $popuptype;
$obj->property  = 'type';
if ($mode == 'edit') {
    $popupPropertiesDAO->updatevalue($obj);
} else {
    $popupPropertiesDAO->insert($obj);
}
// Insert media id into ipopup custom properties
$obj->value     = $media->mediaID;
$obj->property  = 'mediaid';
if ($mode == 'edit') {
    if ($userfile2) $popupPropertiesDAO->updatevalue($obj);
} else {
    $popupPropertiesDAO->insert($obj);
}
// Insert video id into ipopup custom properties if relevant
if ($obj->radiopopup == 'radio_2') {
    $obj->value     = $obj->video_id;
    $obj->property  = 'videoid';
    if ($mode == 'edit') {
        $popupPropertiesDAO->updatevalue($obj);
    } else {
        $popupPropertiesDAO->insert($obj);
    }
}
// Insert uri into ipopup custom properties
$obj->value     = $uri;
$obj->property  = 'uri';
if ($mode == 'edit') {
    $popupPropertiesDAO->updatevalue($obj);
} else {
    $popupPropertiesDAO->insert($obj);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Step 4 - Update the tables iaction, iaction_custom_properties, ilkpopupaction, iemail_action, isms_action, iqrcode_action
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Create a new action id
$actionDAO = new IactionDAO($con);
$ilkpopupactionDAO=new IlkpopupactionDAO($con);
$type = '';
if (isset($obj->email)  && $obj->email  == 'on') {$type .= 'EMAIL,';}
if (isset($obj->sms)    && $obj->sms    == 'on') {$type .= 'SMS,';}
if (isset($obj->qrcode) && $obj->qrcode == 'on') {$type .= 'QRCODE,';}
$obj->type = rtrim($type, ',');
if ($mode == 'edit') {
	$actions = $ilkpopupactionDAO->findByPopup_id($obj->popup_id);
    foreach ($actions as $action) {$obj->action_id = $action->action_id;}
	$obj->id = $obj->action_id;
	$actionDAO->update($obj);
} else {
    $actionDAO->insert($obj);
    $obj->id = mysql_insert_id();
}
$obj->action_id = $obj->id;

// Insert into ilkpopupaction
if ($mode == 'add') {
    $ilkpopupactionDAO->insert($obj);
}

// Insert info into iaction_custom_properties
$actionPropertiesDAO = new Iaction_custom_propertiesDAO($con);
$obj->value     = $obj->introtext;
$obj->property  = 'introtext';
if ($mode == 'edit') {
    $actionPropertiesDAO->updatevalue($obj);
} else {
    $actionPropertiesDAO->insert($obj);
}
$obj->value     = $obj->exittext;
$obj->property  = 'exittext';
if ($mode == 'edit') {
    $actionPropertiesDAO->updatevalue($obj);
} else {
    $actionPropertiesDAO->insert($obj);
}
// Insert email info into iemail_action
if (isset($obj->email)  && $obj->email  == 'on') 
{
    $emailactionDAO = new Iemail_actionDAO($con);
    $obj->subject = $obj->emailsubject;
    $obj->body    = $obj->emailcontent;
    $emailactionDAO->updatevalue($obj);
}
// Insert sms info into isms_action
if (isset($obj->sms)    && $obj->sms    == 'on')
{
	$smsactionDAO = new Isms_actionDAO($con);
    $obj->content = $obj->smscontent;
    $smsactionDAO->updatevalue($obj);
}
// Insert qrcode info into iqrcode_action
if (isset($obj->qrcode) && $obj->qrcode == 'on')
{
    $qrcodeactionDAO = new Iqrcode_actionDAO($con);
    $obj->content = $obj->qrcontent;
	$qrcodetype  = 'EMAIL';
    if ($obj->radioqrcode == 'qrcheck2') {$qrcodetype  = 'PHONE';}
    if ($obj->radioqrcode == 'qrcheck3') {$qrcodetype  = 'TEXT';}
    if ($obj->radioqrcode == 'qrcheck4') {$qrcodetype  = 'WEBSITE';}
    $obj->type = $qrcodetype;
	error_log('TYPE::: '.$obj->type.'**'.$obj->radioqrcode);
    $qrcodeactionDAO->updatevalue($obj);
}

// Link this banner/sequence to the campaign in ilkcampaignsequence
if ($mode == 'add') {
    $ilkcampaignsequenceDAO=new IlkcampaignsequenceDAO($con);
    $ilkcampaignsequenceDAO->insert($obj);
}
?>