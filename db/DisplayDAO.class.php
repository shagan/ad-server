<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/DisplayDAOAbstract.class.php';
class DisplayDAO extends DisplayDAOAbstract {

	public function findByDisplayGroup_id($id){
		$sql = "SELECT * FROM display where displayid in ( select DisplayID from `lkdisplaydg` WHERE DisplayGroupID=? ) ";
		$ps = new PreparedStatement($sql);
		$ps->setInt($id);
		return parent::findWithPreparedStatement($ps);
	}

	public function countByDisplayGroup_id($id){
		$sql = "SELECT count(*) as count FROM display where displayid in ( select DisplayID from `lkdisplaydg` WHERE DisplayGroupID=? ) ";
		$ps = new PreparedStatement($sql);
		$ps->setInt($id);
		$rows = array();
		$rs = $this->connection->executeQuery($ps);
		while ($arr = $this->connection->fetchArray($rs)) {
			$rows[] = $arr;
		}
		$this->connection->freeResult($rs);
		return $rows[0]['count'];
	}

	public function countOnlineByDisplayGroup_id($id){
		$sql = "SELECT count(*) as count FROM display where loggedin = 1 AND displayid in ( select DisplayID from `lkdisplaydg` WHERE DisplayGroupID=? ) ";
		$ps = new PreparedStatement($sql);
		$ps->setInt($id);
		$rows = array();
		$rs = $this->connection->executeQuery($ps);
		while ($arr = $this->connection->fetchArray($rs)) {
			$rows[] = $arr;
		}
		$this->connection->freeResult($rs);
		return $rows[0]['count'];
	}

	public function countOfflineByDisplayGroup_id($id){
		$sql = "SELECT count(*) as count FROM display where loggedin = 0 AND displayid in ( select DisplayID from `lkdisplaydg` WHERE DisplayGroupID=? ) ";
		$ps = new PreparedStatement($sql);
		$ps->setInt($id);
		$rows = array();
		$rs = $this->connection->executeQuery($ps);
		while ($arr = $this->connection->fetchArray($rs)) {
			$rows[] = $arr;
		}
		$this->connection->freeResult($rs);
		return $rows[0]['count'];
	}
}
