<?php
// THIS FILE IS PART OF THE phpdaogen PACKAGE.  DO NOT EDIT.
// THIS FILE GETS RE-WRITTEN EACH TIME THE DAO GENERATOR IS EXECUTED.
// ANY MANUAL EDITS WILL BE LOST.

// PostgreSQLConnection.class.php
// Copyright (c) 2010 Ronald B. Cemer
// All rights reserved.
// This software is released under the BSD license.
// Please see the accompanying LICENSE.txt for details.

if (!class_exists('Connection', false)) include(dirname(__FILE__).'/Connection.class.php');

class PostgreSQLConnection extends Connection {
	private $conn;
	private $transactionDepth = 0;
	private $transactionRolledBack = false;
	private $updatedRowCount = 0;

	public function PostgreSQLConnection($server, $username, $password, $database) {
		$this->likeOperator = 'ilike';

		$this->transactionDepth = 0;
		$this->transactionRolledBack = false;
		if (($colonIdx = strpos($server, ':')) !== false) {
			$port = trim(substr($server, $colonIdx+1));
			$server = trim(substr($server, 0, $colonIdx));
		} else {
			$port = '';
		}
		$connectionString = "host=$server";
		if ($port != '') $connectionString .= " port=$port";
		$connectionString .= " dbname=$database user=$username password=$password";
		$this->conn = pg_connect($connectionString, PGSQL_CONNECT_FORCE_NEW);
	}

	public function close() {
		$this->transactionDepth = 0;
		$this->transactionRolledBack = false;
		if ($this->conn !== false) {
			$cn = $this->conn;
			$this->conn = false;
			pg_close($cn);
		}
	}

	public function encode($val, $encodeAsBinary = false) {
		if ($val === null) return 'null';
		if ($encodeAsBinary) {
			if ($val == '') {
				return "''";
			} else {
				$arrData = unpack("H*hex", $val);
				return '0x'.$arrData['hex'];
			}
		}
		if (is_bool($val)) return $val ? '1' : '0';
		if (is_string($val)) return "'".pg_escape_string($this->conn, $val)."'";
		return (string)$val;
	}

	public function executeUpdate($preparedStatement) {
		$this->updatedRowCount = 0;
		$sql = $preparedStatement->toSQL($this);
		$result = pg_query($this->conn, $sql);
		if ($result === false) {
			// Update or query failed.
			if ($this->throwExceptionOnFailedQuery) {
				throw new Exception(
					'PostgreSQL Error '.pg_last_error($this->conn).
					($this->showSQLInExceptions ? (': '.$sql) : '').
					(isset($_SERVER['REQUEST_URI']) ? ('   page: '.$_SERVER['REQUEST_URI']) : '')
				);
			}
			$this->updatedRowCount = 0;
			return $result;
		}
		$this->updatedRowCount = pg_affected_rows($result);
		pg_free_result($result);
		return true;
	}

	public function getUpdatedRowCount() {
		return $this->updatedRowCount;
	}

	public function executeQuery($preparedStatement) {
		$sql = $preparedStatement->toSQL($this);
		if (($preparedStatement->selectOffset > 0) || ($preparedStatement->selectLimit > 0)) {
			if ((strlen($sql) >= 6) &&
				(strncasecmp($sql, 'select', 6) == 0) &&
				(ctype_space($sql[6]))) {

				if ($preparedStatement->selectOffset > 0) {
					$sql .= sprintf(' offset %d', $preparedStatement->selectOffset);
				}
				if ($preparedStatement->selectLimit > 0) {
					$sql .= sprintf(' limit %d', $preparedStatement->selectLimit);
				}
			} else {
				throw new Exception(
					'selectOffset and selectLimit cannot be applied to'.
					' the specified SQL statement');
			}
		}
		$result = pg_query($this->conn, $sql);
		if ($result === false) {
			// Query failed.
			if ($this->throwExceptionOnFailedQuery) {
				throw new Exception(
					'PostgreSQL Error '.pg_last_error($this->conn).
					($this->showSQLInExceptions ? (': '.$sql) : '').
					(isset($_SERVER['REQUEST_URI']) ? ('   page: '.$_SERVER['REQUEST_URI']) : '')
				);
			}
			return $result;
		}
		return $result;
	}

	public function fetchArray($resultSetIdentifier, $freeResultBeforeReturn = false) {
		$result = pg_fetch_assoc($resultSetIdentifier);
		if ($freeResultBeforeReturn) $this->freeResult($resultSetIdentifier);
		return $result;
	}

	public function fetchObject($resultSetIdentifier, $freeResultBeforeReturn = false) {
		$result = pg_fetch_object($resultSetIdentifier);
		if ($freeResultBeforeReturn) $this->freeResult($resultSetIdentifier);
		return $result;
	}

	public function freeResult($resultSetIdentifier) {
		$result = pg_free_result($resultSetIdentifier);
		if ($resultSetIdentifier === false) $retval = false;
		if ( ($this->throwExceptionOnFailedFreeResult) && ($result === false) ) {
			throw new Exception(
				'Attempt to free invalid result set identifier: '.$resultSetIdentifier.
				(isset($_SERVER['REQUEST_URI']) ? (' page: '.$_SERVER['REQUEST_URI']) : '')
			);
		}
		return $result;
	}

	public function getLastInsertId() {
		$rs = @pg_query($this->conn, 'select lastval() as id');
		if ($rs !== false) {
			$row = @pg_fetch_assoc($rs);
			@pg_free_result($rs);
			if ( ($row) && (isset($row['id'])) ) return $row['id'];
		}
		return false;
	}

	public function beginTransaction() {
		$this->transactionDepth++;
		if ($this->transactionDepth == 1) {
			$this->transactionRolledBack = false;
			$retval = pg_query($this->conn, 'start transaction');
			if ($retval !== false) $retval = true;
		} else {
			$retval = true;
		}
		return $retval;
	}

	public function commitTransaction() {
		if ($this->transactionDepth > 0) {
			$retval = true;
			$this->transactionDepth--;
			if ($this->transactionDepth == 0) {
				if ($this->transactionRolledBack) {
					$retval = pg_query($this->conn, 'rollback');
				} else {
					$retval = pg_query($this->conn, 'commit');
				}
				if ($retval !== false) $retval = true;
			}
		} else {
			$retval = false;
		}
		return $retval;
	}

	public function rollbackTransaction() {
		if ($this->transactionDepth > 0) {
			$this->transactionRolledBack = true;
			$retval = true;
			$this->transactionDepth--;
			if ($this->transactionDepth == 0) {
				$retval = pg_query($this->conn, 'rollback');
				if ($retval !== false) $retval = true;
			}
		} else {
			$retval = false;
		}
		return $retval;
	}
}
