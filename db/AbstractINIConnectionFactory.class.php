<?php
// AbstractINIConnectionFactory.class.php
// Copyright (c) 2010-2011 Ronald B. Cemer
// All rights reserved.
// This software is released under the BSD license.
// Please see the accompanying LICENSE.txt for details.

if (!class_exists('Connection', false)) include(dirname(__FILE__).'/Connection.class.php');
if (!class_exists('PreparedStatement', false)) include(dirname(__FILE__).'/PreparedStatement.class.php');

abstract class AbstractINIConnectionFactory {
	// These must be set in the implementing class.
	public static $INI_FILE;
	public static $DEFAULT_CONNECTION_CLASS;
	public static $DEFAULT_SERVER;
	public static $DEFAULT_USERNAME;
	public static $DEFAULT_PASSWORD;
	public static $DEFAULT_DATABASE;

	public static function getConnection() {
		if (@file_exists(self::$INI_FILE)) {
			$cfg = parse_ini_file(self::$INI_FILE);
			if (($cfg === false) || (!is_array($cfg))) $cfg = array();
		} else {
			$cfg = array();
		}

		$connectionClass = isset($cfg['connectionClass']) ? (string)$cfg['connectionClass'] : self::$DEFAULT_CONNECTION_CLASS;
		$server = isset($cfg['server']) ? (string)$cfg['server'] : self::$DEFAULT_SERVER;
		$username = isset($cfg['username']) ? (string)$cfg['username'] : self::$DEFAULT_USERNAME;
		$password = isset($cfg['password']) ? (string)$cfg['password'] : self::$DEFAULT_PASSWORD;
		$database = isset($cfg['database']) ? (string)$cfg['database'] : self::$DEFAULT_DATABASE;

		if (!class_exists($connectionClass, false)) {
			include dirname(__FILE__).'/'.$connectionClass.'.class.php';
		}

		return new $connectionClass($server, $username, $password, $database);
	}
}
