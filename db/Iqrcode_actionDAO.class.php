<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/Iqrcode_actionDAOAbstract.class.php';
class Iqrcode_actionDAO extends Iqrcode_actionDAOAbstract {
	
	public function updatevalue($iqrcode_action) {
		$ps = new PreparedStatement("INSERT INTO iqrcode_action 
		(action_id, type, content) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE type=?, content=?");
		$ps->setInt($iqrcode_action->action_id);
		$ps->setString($iqrcode_action->type);
		$ps->setString($iqrcode_action->content);
		$ps->setString($iqrcode_action->type);
		$ps->setString($iqrcode_action->content);
		return $this->connection->executeUpdate($ps);
	}
	
}
