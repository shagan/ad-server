<?php include("kl_inc_header.php"); ?>

<body>
<?php include("kl_inc_navbar.php"); ?>
<?php include("kl_inc_sidebar.php"); ?>
	<div class="main-content">
		<div class="breadcrumbs" id="breadcrumbs"> 
			<script type="text/javascript">
						try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
					</script>
			<?php include("kl_inc_breadcrumbs.php"); ?>
			<?php include("kl_inc_nav_search.php"); ?></div>
		
		
		<div class="page-content">
			<div class="page-header position-relative">
				<h1> Form Wizard <small> <i class="icon-double-angle-right"></i> and Validation </small> </h1>
			</div>
			<!--/.page-header-->
			
			<div class="row-fluid">
				<div class="span12"> 
					<!--PAGE CONTENT BEGINS-->
					
					<div data-target="#step-container" id="fuelux-wizard" class="wizard">
						<ul class="wizard-steps">
							<li data-target="#step1" class="active"> <span class="step">1</span> <span class="title">Step # 1</span> </li>
							<li data-target="#step2"> <span class="step">2</span> <span class="title">Step # 2</span> </li>
						</ul>
					</div>
					<!--/.row-fluid-->
					
					<div id="step-container" class="step-content row-fluid position-relative">
							<div class="step-pane active" id="step1">
							form one appears here
							</div>
							<!--#step1-->
						
							<div class="step-pane" id="step2">
							step two content appears here
							</div>
						<!--#step2--> 
					</div>
					<!--/.step-content-->
					
					<div class="row-fluid wizard-actions">
						<button class="btn btn-prev"> <i class="icon-arrow-left"></i> Prev </button>
						<button class="btn btn-success btn-next" data-last="Finish "> Next <i class="icon-arrow-right icon-on-right"></i> </button>
					</div>
					<!--/.wizard-actions--> 
					
					<!--PAGE CONTENT ENDS--> 
				</div>
				<!--/.span--> 
			</div>
			<!--/.row-fluid--> 
		</div>
	</div>
	<!--/.row-fluid--> 
</div>
<!--/.page-content-->

<?php include("kl_inc_ace_settings.php"); ?>
</div>
<!--/.main-content-->
</div>
<!--/.main-container--> 

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse"> <i class="icon-double-angle-up icon-only bigger-110"></i> </a> 

<!--basic scripts--> 

<!--[if !IE]>--> 

<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script> 

<!--<![endif]--> 

<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]--> 

<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script> 
<script src="assets/js/bootstrap.min.js"></script> 

<!--page specific plugin scripts--> 

<script src="assets/js/fuelux/fuelux.wizard.min.js"></script> 
<script src="assets/js/jquery.validate.min.js"></script> 
<script src="assets/js/additional-methods.min.js"></script> 
<script src="assets/js/bootbox.min.js"></script> 
<script src="assets/js/jquery.maskedinput.min.js"></script> 
<script src="assets/js/select2.min.js"></script> 

<!--ace scripts--> 

<script src="assets/js/ace-elements.min.js"></script> 
<script src="assets/js/ace.min.js"></script> 

<!--inline scripts related to this page-->
<script>
    $('#fuelux-wizard').ace_wizard().on('change', function(e, info){
    if(info.step == 1 && !$('#form-1').valid()) return false;
    }).on('finished', function(){
    alert('Success!');
    }).on('stepclick', function(){
    //return false;//if you don't want users click on backward steps
    });

</script>


</body>
</html>
