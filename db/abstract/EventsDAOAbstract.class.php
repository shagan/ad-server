<?php
// Generated automatically by phpdaogen.
// Do NOT edit this file.
// Any changes made to this file will be overwritten the next time it is generated.

if (!class_exists('Events', false)) include dirname(dirname(__FILE__)).'/Events.class.php';

abstract class EventsDAOAbstract {
	public static $ALLOWED_QUERY_OPERATORS = array('=', '<>', '<', '<=', '>', '>=', 'beginsWith', 'contains', 'endsWith');
	public static $ALLOWED_NUMERIC_QUERY_OPERATORS = array('=', '<>', '<', '<=', '>', '>=');
	public static $ALLOWED_STRING_QUERY_OPERATORS = array('=', '<>', '<', '<=', '>', '>=', 'beginsWith', 'contains', 'endsWith');
	public static $ALLOWED_BINARY_QUERY_OPERATORS = array('=', '<>');
	protected $connection;
	protected $cache = null;

	public function __construct($connection, $cache = null) {
		$this->connection = $connection;
		$this->cache = $cache;
	}

	public function getCache() {
		return $this->cache;
	}

	public function setCache($cache) {
		$this->cache = $cache;
	}

	public function insert(&$events) {
		$ps = new PreparedStatement("insert into events (code, message, level, displayID, eventDate) values (?, ?, ?, ?, ?)");
		$ps->setString($events->code);
		$ps->setString($events->message);
		$ps->setString($events->level);
		$ps->setInt($events->displayID);
		$ps->setString($events->eventDate);
		$result = $this->connection->executeUpdate($ps);
		$events->eventID = $this->connection->getLastInsertId();
		return $result;
	}

	public function update($events) {
		$ps = new PreparedStatement("update events set code = ?, message = ?, level = ?, displayID = ?, eventDate = ? where eventID = ?");
		$ps->setString($events->code);
		$ps->setString($events->message);
		$ps->setString($events->level);
		$ps->setInt($events->displayID);
		$ps->setString($events->eventDate);
		$ps->setInt($events->eventID);
		return $this->connection->executeUpdate($ps);
	}

	public function delete($eventID) {
		$ps = new PreparedStatement("delete from events where eventID = ?");
		$ps->setInt($eventID);
		return $this->connection->executeUpdate($ps);
	}

	public function load($eventID) {
		$ps = new PreparedStatement("select * from events where eventID = ?", 0, 1);
		$ps->setInt($eventID);
		$rows = $this->findWithPreparedStatement($ps);
		if (count($rows) > 0) return $rows[0];
		return false;
	}

	public function findByEventIDPS($eventID, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from events where eventID $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($eventID);
		return $ps;
	}

	public function findByEventID($eventID, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByEventIDPS($eventID, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByCodePS($code, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_STRING_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_STRING_QUERY_OPERATORS[0];
		if (($queryOperator == 'beginsWith') || ($queryOperator == 'endsWith') || ($queryOperator == 'contains')) {
			$sqlQueryOperator = $this->connection->likeOperator;
			$needLower = (!$this->connection->hasCaseInsensitiveLike);
		} else {
			$sqlQueryOperator = $queryOperator;
			$needLower = false;
		}
		$ps = new PreparedStatement("select * from events where ".($needLower ? 'lower(code)' : 'code').' '.$sqlQueryOperator.' '.($needLower ? 'lower(?)' : '?').((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		if ($queryOperator == 'beginsWith') {
			$ps->setString($code.'%');
		} else if ($queryOperator == 'endsWith') {
			$ps->setString('%'.$code);
		} else if ($queryOperator == 'contains') {
			$ps->setString('%'.$code.'%');
		} else {
			$ps->setString($code);
		}
		return $ps;
	}

	public function findByCode($code, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByCodePS($code, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByMessagePS($message, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_STRING_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_STRING_QUERY_OPERATORS[0];
		if (($queryOperator == 'beginsWith') || ($queryOperator == 'endsWith') || ($queryOperator == 'contains')) {
			$sqlQueryOperator = $this->connection->likeOperator;
			$needLower = (!$this->connection->hasCaseInsensitiveLike);
		} else {
			$sqlQueryOperator = $queryOperator;
			$needLower = false;
		}
		$ps = new PreparedStatement("select * from events where ".($needLower ? 'lower(message)' : 'message').' '.$sqlQueryOperator.' '.($needLower ? 'lower(?)' : '?').((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		if ($queryOperator == 'beginsWith') {
			$ps->setString($message.'%');
		} else if ($queryOperator == 'endsWith') {
			$ps->setString('%'.$message);
		} else if ($queryOperator == 'contains') {
			$ps->setString('%'.$message.'%');
		} else {
			$ps->setString($message);
		}
		return $ps;
	}

	public function findByMessage($message, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByMessagePS($message, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByLevelPS($level, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_STRING_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_STRING_QUERY_OPERATORS[0];
		if (($queryOperator == 'beginsWith') || ($queryOperator == 'endsWith') || ($queryOperator == 'contains')) {
			$sqlQueryOperator = $this->connection->likeOperator;
			$needLower = (!$this->connection->hasCaseInsensitiveLike);
		} else {
			$sqlQueryOperator = $queryOperator;
			$needLower = false;
		}
		$ps = new PreparedStatement("select * from events where ".($needLower ? 'lower(level)' : 'level').' '.$sqlQueryOperator.' '.($needLower ? 'lower(?)' : '?').((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		if ($queryOperator == 'beginsWith') {
			$ps->setString($level.'%');
		} else if ($queryOperator == 'endsWith') {
			$ps->setString('%'.$level);
		} else if ($queryOperator == 'contains') {
			$ps->setString('%'.$level.'%');
		} else {
			$ps->setString($level);
		}
		return $ps;
	}

	public function findByLevel($level, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByLevelPS($level, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByDisplayIDPS($displayID, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_NUMERIC_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_NUMERIC_QUERY_OPERATORS[0];
		$ps = new PreparedStatement("select * from events where displayID $queryOperator ?".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		$ps->setInt($displayID);
		return $ps;
	}

	public function findByDisplayID($displayID, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByDisplayIDPS($displayID, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findByEventDatePS($eventDate, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		if (!in_array($queryOperator, self::$ALLOWED_STRING_QUERY_OPERATORS)) $queryOperator = self::$ALLOWED_STRING_QUERY_OPERATORS[0];
		if (($queryOperator == 'beginsWith') || ($queryOperator == 'endsWith') || ($queryOperator == 'contains')) {
			$sqlQueryOperator = $this->connection->likeOperator;
			$needLower = (!$this->connection->hasCaseInsensitiveLike);
		} else {
			$sqlQueryOperator = $queryOperator;
			$needLower = false;
		}
		$ps = new PreparedStatement("select * from events where ".($needLower ? 'lower(eventDate)' : 'eventDate').' '.$sqlQueryOperator.' '.($needLower ? 'lower(?)' : '?').((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		if ($queryOperator == 'beginsWith') {
			$ps->setString($eventDate.'%');
		} else if ($queryOperator == 'endsWith') {
			$ps->setString('%'.$eventDate);
		} else if ($queryOperator == 'contains') {
			$ps->setString('%'.$eventDate.'%');
		} else {
			$ps->setString($eventDate);
		}
		return $ps;
	}

	public function findByEventDate($eventDate, $queryOperator = '=', $orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findByEventDatePS($eventDate, $queryOperator, $orderBy, $offset, $limit));
	}

	public function findAllPS($orderBy = null, $offset = 0, $limit = 0) {
		$ps = new PreparedStatement("select * from events".((($orderBy!==null)&&($orderBy!='')) ? (' order by '.$orderBy) : ''), $offset, $limit);
		return $ps;
	}

	public function findAll($orderBy = null, $offset = 0, $limit = 0) {
		return $this->findWithPreparedStatement($this->findAllPS($orderBy, $offset, $limit));
	}

	public function findWithPreparedStatement($ps) {
		$cacheKey = null;
		if ($this->cache !== null) {
			$cacheKey = $ps->toSQL($this->connection);
			if (($rows = $this->cache->get($cacheKey)) !== false) {
				return $rows;
			}
		}
		$rows = array();
		$rs = $this->connection->executeQuery($ps);
		while ($arr = $this->connection->fetchArray($rs)) {
			$row = new Events();
			$row->loadFromArray($arr);
			$rows[] = $row;
		}
		$this->connection->freeResult($rs);
		if ($this->cache !== null) {
			$this->cache->set($cacheKey, $rows);
		}
		return $rows;
	}
}
