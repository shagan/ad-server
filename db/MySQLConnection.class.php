<?php
// THIS FILE IS PART OF THE phpdaogen PACKAGE.  DO NOT EDIT.
// THIS FILE GETS RE-WRITTEN EACH TIME THE DAO GENERATOR IS EXECUTED.
// ANY MANUAL EDITS WILL BE LOST.

// MySQLConnection.class.php
// Copyright (c) 2010 Ronald B. Cemer
// All rights reserved.
// This software is released under the BSD license.
// Please see the accompanying LICENSE.txt for details.

if (!class_exists('Connection', false)) include(dirname(__FILE__).'/Connection.class.php');

class MySQLConnection extends Connection {
	private $conn;
	private $transactionDepth = 0;
	private $transactionRolledBack = false;
	private $updatedRowCount = 0;

	public function MySQLConnection($server, $username, $password, $database, $newLink = true) {
		$this->transactionDepth = 0;
		$this->transactionRolledBack = false;
		$this->conn = mysql_connect($server, $username, $password, $newLink);
		mysql_set_charset('utf8',$this->conn);
		if ($this->conn !== false) mysql_select_db($database, $this->conn);
	}

	public function close() {
		$this->transactionDepth = 0;
		$this->transactionRolledBack = false;
		if ($this->conn !== false) {
			$cn = $this->conn;
			$this->conn = false;
			mysql_close($cn);
		}
	}

	public function encode($val, $encodeAsBinary = false) {
		if ($val === null) return 'null';
		if ($encodeAsBinary) {
			if ($val == '') {
				return "''";
			} else {
				$arrData = unpack("H*hex", $val);
				return '0x'.$arrData['hex'];
			}
		}
		if (is_bool($val)) return $val ? '1' : '0';
		if (is_string($val)) return "'".mysql_real_escape_string($val, $this->conn)."'";
		return (string)$val;
	}

	public function executeUpdate($preparedStatement) {
		$this->updatedRowCount = 0;
		$sql = $preparedStatement->toSQL($this);
		$result = mysql_unbuffered_query($sql, $this->conn);
		if ( ($this->throwExceptionOnFailedQuery) && ($result === false) ) {
			throw new Exception(
				'MySQL Error '.mysql_errno($this->conn).' '.mysql_error($this->conn).
				($this->showSQLInExceptions ? (': '.$sql) : '').
				(isset($_SERVER['REQUEST_URI']) ? ('   page: '.$_SERVER['REQUEST_URI']) : '')
			);
		}
		if ( ($result === true) || ($result === false) ) {
			$this->updatedRowCount = mysql_affected_rows($this->conn);
			return $result;
		}
		// This looks like a query.  Better free the result set.
		mysql_free_result($result, $this->conn);
		$this->updatedRowCount = mysql_affected_rows($this->conn);
		return true;
	}

	public function getUpdatedRowCount() {
		return $this->updatedRowCount;
	}

	public function executeQuery($preparedStatement) {
		$sql = $preparedStatement->toSQL($this);
		if (($preparedStatement->selectOffset > 0) || ($preparedStatement->selectLimit > 0)) {
			if ((strlen($sql) >= 6) &&
				(strncasecmp($sql, 'select', 6) == 0) &&
				(ctype_space($sql[6]))) {
				if ($preparedStatement->selectLimit > 0) {
					$sql .= sprintf(
						' limit %d,%d',
						$preparedStatement->selectOffset,
						$preparedStatement->selectLimit
					);
				} else {
					$sql .= sprintf(
						' limit %d,18446744073709551615',
						$preparedStatement->selectOffset
					);
				}
			} else {
				throw new Exception(
					'selectOffset and selectLimit cannot be applied to'.
					' the specified SQL statement');
			}
		}
		$result = mysql_unbuffered_query($sql, $this->conn);
		if ( ($this->throwExceptionOnFailedQuery) && ($result === false) ) {
			throw new Exception(
				'MySQL Error '.mysql_errno($this->conn).' '.mysql_error($this->conn).
				($this->showSQLInExceptions ? (': '.$sql) : '').
				(isset($_SERVER['REQUEST_URI']) ? ('   page: '.$_SERVER['REQUEST_URI']) : '')
			);
		}
		if ($result === false) return $result;
		// This looks like an update.
		// Better return 0 so callers expecting a result set don't blow up.
		if ($result === true) return 0;
		return $result;
	}

	public function fetchArray($resultSetIdentifier, $freeResultBeforeReturn = false) {
		$result = mysql_fetch_assoc($resultSetIdentifier);
		if ($freeResultBeforeReturn) $this->freeResult($resultSetIdentifier);
		return $result;
	}

	public function fetchObject($resultSetIdentifier, $freeResultBeforeReturn = false) {
		$result = mysql_fetch_object($resultSetIdentifier);
		if ($freeResultBeforeReturn) $this->freeResult($resultSetIdentifier);
		return $result;
	}

	public function freeResult($resultSetIdentifier) {
		$result = mysql_free_result($resultSetIdentifier);
		if ($resultSetIdentifier === false) $retval = false;
		if ( ($this->throwExceptionOnFailedFreeResult) && ($result === false) ) {
			throw new Exception(
				'Attempt to free invalid result set identifier: '.$resultSetIdentifier.
				(isset($_SERVER['REQUEST_URI']) ? (' page: '.$_SERVER['REQUEST_URI']) : '')
			);
		}
		return $result;
	}

	public function getLastInsertId() {
		$result = mysql_insert_id($this->conn);
		if ( ($result === false) || ($result == 0) ) return false;
		return $result;
	}

	public function beginTransaction() {
		$this->transactionDepth++;
		if ($this->transactionDepth == 1) {
			$this->transactionRolledBack = false;
			$retval = mysql_unbuffered_query('start transaction', $this->conn);
			if ($retval !== false) $retval = true;
		} else {
			$retval = true;
		}
		return $retval;
	}

	public function commitTransaction() {
		if ($this->transactionDepth > 0) {
			$retval = true;
			$this->transactionDepth--;
			if ($this->transactionDepth == 0) {
				if ($this->transactionRolledBack) {
					$retval = mysql_unbuffered_query('rollback', $this->conn);
				} else {
					$retval = mysql_unbuffered_query('commit', $this->conn);
				}
				if ($retval !== false) $retval = true;
			}
		} else {
			$retval = false;
		}
		return $retval;
	}

	public function rollbackTransaction() {
		if ($this->transactionDepth > 0) {
			$this->transactionRolledBack = true;
			$retval = true;
			$this->transactionDepth--;
			if ($this->transactionDepth == 0) {
				$retval = mysql_unbuffered_query('rollback', $this->conn);
				if ($retval !== false) $retval = true;
			}
		} else {
			$retval = false;
		}
		return $retval;
	}
}
