<?php include("kl_inc_header_fileupload.php"); ?>
<?php //include("kl_inc_header.php"); ?>
<body>
<?php 
// Include functionality
include("kl_inc_navbar.php");
include("kl_inc_sidebar.php");
// Include DAO file handlers 
include 'db/dbheader.php';
include 'db/Iadvertiser.class.php';
include 'db/IadvertiserDAO.class.php';
include 'db/Media.class.php';
include 'db/MediaDAO.class.php';
include 'db/Ilkadvertisermedia.class.php';
include 'db/IlkadvertisermediaDAO.class.php';

/////////////////////////
// Get Advertiser List //
/////////////////////////
$advertiser_id = 0;
if (isset($_GET['advertiser_id'])) {$advertiser_id = $_GET['advertiser_id'];}
$select_advertiser = '<select name="advertiser" id="advertiser">';
if (!isset($_GET['advertiser_id'])) {
   $select_advertiser .= '<option value="0">(Select Advertiser)</option>';
}
$advertiserDAO=new IadvertiserDAO($con);
$advertisers=$advertiserDAO->findall();
foreach ($advertisers as $advertiser) {
   	$select_advertiser .= '<option value="' . $advertiser->id . '"';
	if ($advertiser->id == $advertiser_id) {$select_advertiser .= ' selected="selected"';}
	$select_advertiser .= '>' . $advertiser->name . '</option>'; 
}
$select_advertiser .= '</select>';
?>

                    
<div class="main-content">
	<div class="breadcrumbs" id="breadcrumbs">
		<script type="text/javascript">
			try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
		</script>

		<div class="page-content">
   			<!--/.page-header-->
            <div class="row-fluid">
				<h3 class="header smaller lighter blue">Media File Upload : <b><?php echo $select_advertiser; ?></b></h3>
  
            <!--<div class="row-fluid">-->                    			
            <!--<div class="container">-->
            <!--<div class="row">-->
            <!--<div class="col-12">-->
            <!-- The file upload form used as target for the file upload widget -->
            <form id="fileupload"  action="fileupload/server/php/" method="POST" enctype="multipart/form-data">
                <!-- Redirect browsers with JavaScript disabled to the origin page -->
                <noscript><input type="hidden" name="redirect" value="http://blueimp.github.io/jQuery-File-Upload/"></noscript>
                <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                <div class="row fileupload-buttonbar">
                    <div class="col-lg-7">
                        <!-- The fileinput-button span is used to style the file input field as button -->
                        <span class="btn btn-success fileinput-button">
                            <i class="icon-plus icon-white"></i>
                            <span>Add files</span>
                            <input id="fileupload" type="file" name="files[]" data-url="fileupload/server/php/handler.json" multiple>
                            <input name="extrapost" type="hidden" value="set">
                            <!--<input type="file" name="files[]" multiple>-->
                        </span>
                        <button type="submit" class="btn btn-primary start" id="uploadbtn">
                            <i class="icon-upload icon-white"></i>
                            <span>Upload All</span>
                        </button>
                        <button type="reset" class="btn btn-warning cancel">
                            <i class="icon-ban-circle icon-white"></i>
                            <span>Cancel All</span>
                        </button>
                    </div>
                    <!-- The global progress information -->
                    <!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The loading indicator is shown during file processing -->
                    <span class="fileupload-loading"></span>
                    <div class="col-lg-5 fileupload-progress fade">
                        <!-- The global progress bar -->
                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                        </div>
                        <!-- The extended global progress information -->
                        <div class="progress-extended">&nbsp;</div>
                    </div>
                </div>
                <!-- The table listing the files available for upload/download -->
                <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
            </form>

            <!-- The blueimp Gallery widget -->
            <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                <div class="slides"></div>
                <h3 class="title"></h3>
                <a class="prev">‹</a>
                <a class="next">›</a>
                <a class="close">×</a>
                <a class="play-pause"></a>
                <ol class="indicator"></ol>
            </div>



            <!--</div>-->






			<div class="row-fluid">
				<h3 class="header smaller lighter blue">Media Library</h3>
				<!-- <div class="table-header">
				Results for "Latest Registered Domains"
				</div> -->

				<table id="sample-table-2" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
						</tr>
					</thead>

					<tbody>
					</tbody>
				</table>
			</div>
            
            </div>

		</div><!--/.page-content-->

        <?php include("kl_inc_ace_settings.php"); ?>

	</div><!--/.main-content-->
</div><!--/.main-container-->

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
	<i class="icon-double-angle-up icon-only bigger-110"></i>
</a>




<!--basic scripts-->

<!--[if !IE]>-->

<script type="text/javascript">
	window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>

<script type="text/javascript">
	if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="assets/js/bootstrap.min.js"></script>

<!--page specific plugin scripts-->

<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/jquery.dataTables.bootstrap.js"></script>

<!--ace scripts-->



<!--page specific plugin scripts

<!--ace scripts -->
<script src="assets/js/ace-elements.min.js"></script>
<!-- CONFLICT 1 <script src="assets/js/ace.min.js"></script>-->

<!--inline scripts related to this page-->
<script id="template-upload" type="text/x-tmpl">

{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
            <input type="hidden" name="advertiser" 
			value="{%=document.getElementById('advertiser').options[document.getElementById('advertiser').selectedIndex].text%}"/>
            <input type="hidden" name="ad_id" value="{%=advertiser.value%}"/>
        </td>

        <td>
            <p class="fname"><label>Description: <input type="text" name="fname" value="{%=file.name%}" required></label></p>
        </td>

        <td>
            <p class="name">{%=file.name%}</p>
            {% if (file.error) { %}
                <div><span class="label label-important">Error</span> {%=file.error%}</div>
            {% } %}
        </td>

        <td>
            <p class="size">{%=o.formatFileSize(file.size)%}</p>
            {% if (!o.files.error) { %}
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
            {% } %}
        </td>
        <td>
            {% if (!o.files.error && !i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start">
                    <i class="icon-upload icon-white"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="icon-ban-circle icon-white"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>

<!-- The template to display files available for download file.url -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
{% var fname   = file.fname; %}
{% var advertiser = file.advertiser; %}
{% var mediaId = file.mediaId; %}
{% var selected_adv_id = getURLParameter('advertiser_id'); %}
{% if (selected_adv_id != file.advertiser_id) {continue;} %}
{% continue; %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="fname"><label>Description: <?php $output = '{%=fname%}'; echo $output; ?></label></p>
            <p class="advertiser"><label>Advertiser: <?php $output1 = '{%=advertiser%}'; echo $output1; ?></label></p>
        </td>

        <td>
            <p class="name">
                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
            </p>
            {% if (file.error) { %}
                <div><span class="label label-important">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.error) { %}
      			<a class="btn btn-yellow" href="kl_media_library.php?advertiser_id={%=selected_adv_id%}">
        			<i class="icon-user bigger-125"></i>
                    <span>Continue</span>
		        </a>
            {% } %}
            {% if (!file.error) { %}
			    <a class="btn btn-yellow" href="kl_media.php?mediaID={%=mediaId%}">
				    <i class="icon-user bigger-125"></i>
                    <span>Details</span>
			    </a>
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="icon-trash icon-white"></i>
                    <span>Delete</span>
                </button>
            {% } %}
            <!--<input type="checkbox" name="delete" value="1" class="toggle">-->
        </td>
    </tr>
{% } %}
</script>
<!-- CONFLICT 2 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>-->
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="fileupload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="http://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="http://blueimp.github.io/JavaScript-Load-Image/js/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/js/bootstrap.min.js"></script>
<!-- blueimp Gallery script -->
<script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="fileupload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="fileupload/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="fileupload/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="fileupload/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="fileupload/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="fileupload/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="fileupload/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="fileupload/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<!-- CONFLICT 3 <script src="fileupload/js/main.js"></script>-->
<script src="fileupload/js/main.js"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="js/cors/jquery.xdr-transport.js"></script>
<![endif]-->
















<!--inline scripts related to this page-->
<script type="text/javascript">
$('#advertiser').change(function() {
    window.location = "kl_medias.php?advertiser_id=" + $(this).val();
});
</script>

<script type="text/javascript">
	jQuery(function($) {
		var oTable1 = $('#sample-table-2').dataTable( {
			"bProcessing": true,
			"bServerSide": true,
			"fnCreatedRow": function(){
				setTimeout( "$('[data-rel=tooltip]').tooltip();",1000 );
			},
			"sAjaxSource": "db/datatable_media.php?advertiser_id=<?php echo $_GET['advertiser_id'] ?>",
		"aoColumns": [
	      null,
	      null
		] } );
	})
</script>

<script>

jQuery(document).ready( function(){       
    jQuery(".edit-button").click( showDialog );

        //variable to reference window
        $myWindow = jQuery('#myDiv');

        //instantiate the dialog
        $myWindow.dialog({ height: 600,
                width: 800,
                modal: true,
                position: 'center',
                autoOpen:false,
                title:'Bewerk therapeut',
                overlay: { opacity: 0.5, background: 'black'}
                });
        }

);
//function to show dialog   
var showDialog = function() {
    $myWindow.show(); 
    //open the dialog
    $myWindow.dialog("open");
    }

var closeDialog = function() {
    $myWindow.dialog("close");
}

</script>

<script type="text/javascript">
	jQuery(function($) {
		var oTable1 = $('#layout_regions').dataTable( );
				
		$('table th input:checkbox').on('click' , function(){
			var that = this;
			$(this).closest('table').find('tr > td:first-child input:checkbox')
			.each(function(){
				this.checked = that.checked;
				$(this).closest('tr').toggleClass('selected');
			});
						
		});
			
		$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
		function tooltip_placement(context, source) {
			var $source = $(source);
			var $parent = $source.closest('table')
			var off1 = $parent.offset();
			var w1 = $parent.width();
			
			var off2 = $source.offset();
			var w2 = $source.width();
			
			if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
			return 'left';
		}
	})
</script>
<script>
$('#fileupload').bind('fileuploadsubmit', function (e, data) {
    var inputs = data.context.find(':input');
    if (inputs.filter('[required][value="title"]').first().focus().length) {
        return false;
    }
    data.formData = inputs.serializeArray();
    console.log(data.formData);
});
</script>
<script>
function getURLParameter(name) {
    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
    );
}
</script>


</body>

</html>
