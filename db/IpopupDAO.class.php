<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/IpopupDAOAbstract.class.php';
class IpopupDAO extends IpopupDAOAbstract {


	
	public function findBySequence_id($sequence_id){
		$sql = "select * from ipopup where FIND_IN_SET(id,(CASE (select type from isequence where id=?)WHEN 'com.instillo.layout.model.BannerSequence' THEN (select popup_id from ilkbannerpopup where sequence_id=?)WHEN 'com.instillo.layout.model.MainContentSequence' THEN (select popup_id from ilkmaincontentpopup where sequence_id=?)WHEN 'com.instillo.layout.model.SponsorBannerSequence' THEN (select group_concat(CONVERT(popup_id, CHAR(8))) from ilkhotareapopup where hotarea_id in ( select hotarea_id from ilksequencehotarea where sequence_id=?) group by 'all')WHEN 'com.instillo.layout.model.TickerSequence' THEN (select popup_id from ilksponsorpopup where sponsor_id in ( select sponsor_id from ilksequencesponsor where sequence_id=?)) END));";
		$ps = new PreparedStatement($sql);
		$ps->setInt($sequence_id);
		$ps->setInt($sequence_id);
		$ps->setInt($sequence_id);
		$ps->setInt($sequence_id);
		$ps->setInt($sequence_id);
		return parent::findWithPreparedStatement($ps);
	}
}
