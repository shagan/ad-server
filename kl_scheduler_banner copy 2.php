<?php include("kl_inc_header.php"); ?>
   
   

   <?php 
  include 'db/dbheader.php';
  include 'db/IAvailablesequenceDAO.php';

?>
<body>
<?php include("kl_inc_navbar.php"); ?>

<?php include("kl_inc_sidebar.php"); ?>

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
          <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
          </script>

      
<?php include("kl_inc_breadcrumbs.php"); ?>

          <?php include("kl_inc_nav_search.php"); ?></div>

        <div class="page-content">
<!-- including scripts and css for scheduler -->
   <script type="text/javascript" src="scheduler/js/scheduler.js"></script>
   <link href="scheduler/css/scheduler.css" type="text/css" rel="stylesheet">

<div id="scheduler_buttons">
    <a data-toggle="modal" class="btn blue"  role="button" href="#modal-form"> <i class="icon-plus"></i>existing banner</a>
    <a class="btn blue" href="kl_ad_banner_new.php"> <i class="icon-plus"></i>new banner</a>

</div>
<div id="sch-timeline">
   <div id="sch-entities-info">
      <div id="sch-section">
        <ul id="sch-entity-names">

        </ul>

      </div>
      <div class="clearfix"></div>
   </div>
   <div id="sch-scheduler">
        <div id="sch-sbody">
              <div id="sch-shead">
                    <div id="sch-months"></div>
                    <div class="clearfix"></div>
                    <div id="sch-days">
                        <div class="sch-wrapper"></div>
                        <div class="clearfix"></div>
                     </div>
                     <div id="sch-slot-records">
                         <div class="sch-wrapper"></div>
                         <div class="clearfix"></div>
                     </div>
              </div>
              <div>
                    <div class="clearfix"></div>
                    <div id="sch-entities">
                       <div id="sch-runs-container">
                       </div>
                     </div>
               </div>
          </div>
   </div>
   <div class="clearfix"></div>
</div>
<div class="sch-modal-backdrop sch-hide"></div>
<div id="sch-status"></div>
<br clear="all">



 <!--- beginning modal form -->

                <div id="modal-form" class="modal hide" tabindex="-1">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="blue bigger">Please fill the following form fields</h4>
                </div>

                <div class="modal-body overflow-visible">
                  <div class="row-fluid">
                    
                      <div class="control-group">
                        <label for="form-field-select-3">Location</label>

                        <div class="controls">
                          <select class="chosen-select" data-placeholder="Choose a Country..." id="select_sequence">
                            <option value="">&nbsp;</option>
                            <?php
                              $sequenceDAO=new IAvailablesequenceDAO($con);
                              $items=$sequenceDAO->getSequencesNotinSegment(1);
                              foreach ($items as $item){
    
                            ?>
                                    <option value="<?php echo $item["sequence_id"].___. $item["sequence_name"] ?>"><?php echo $item["advertiser_name"].":".$item["campaign_name"].":".$item["sequence_name"]."  " ?></option>
                            <?php
                                  
                              }
                            ?>

                            
                          </select>
                        </div>
                      </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <button class="btn btn-small" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Cancel
                  </button>

                  <button class="btn btn-small btn-primary" id="addSequence" data-dismiss="modal">
                    <i class="icon-ok"></i>
                    Save
                  </button>
                </div>
              </div>


<!-- ending modal form code -->
 <script type="text/javascript" src="scheduler/js/jquery.js"></script>
   <script type="text/javascript" src="scheduler/js/underscore.js"></script>
   <script type="text/javascript" src="scheduler/js/backbone.js"></script>
   
   <script type="text/javascript" src="scheduler/js/jquery.ui.core.js"></script>
   <script type="text/javascript" src="scheduler/js/jquery.ui.widget.js"></script>
   <script type="text/javascript" src="scheduler/js/jquery.ui.mouse.js"></script>
   
   <script type="text/javascript" src="scheduler/js/jquery.ui.draggable.js"></script>
   <script type="text/javascript" src="scheduler/js/jquery.ui.droppable.js"></script>
   <script type="text/javascript" src="scheduler/js/jquery.ui.selectable.js"></script>
   <script type="text/javascript" src="scheduler/js/jquery.ui.sortable.js"></script>
   <script type="text/javascript" src="scheduler/js/jquery.ui.resizable.js"></script>
   
   <script type="text/javascript" src="scheduler/js/scheduler.js"></script>
<script>
var scheduler;
  $(function(){
      /*
       * set of entities
       */
    
      var data = [
            <?php
            $sequenceDAO=new IAvailablesequenceDAO($con);
            $items=$sequenceDAO->getSequencesinSegment(1);
            foreach ($items as $item){    
            ?>
              {entity:{ id:"<?php echo $item["sequence_id"] ?>", name:"<?php echo $item["sequence_name"] ?>"} },
            <?php } ?>
                 ];
 
     //Listening to entity events
     app.models.Entity.events._bind("sort",function( event, entities ){
       console.log("entity sorted-------------"); 
       console.log(entities);
       console.log("-----------------");
     });
     app.models.Entity.events._bind("delete", function( event, entity ){
          console.log("entity remove-------------");
          console.log(entity.attributes);
          console.log("-----------------");        
     });

     //Listening to run events
     app.models.Run.events._bind("update",function(event,run){
         console.log("run update-----------------");
         console.log(run.attributes);
         console.log("-----------------");
     });
    app.models.Run.events._bind("delete",function(event, run){
        console.log("run delete--------------");
        console.log(run.attributes);
        console.log("------------------------");
     });
     app.models.Run.events._bind("create", function( event, run ){
          console.log("run create-------------");
          console.log(run.attributes);
          console.log("run create");        
     });
 
     //listening to repeat events
     app.models.Repeat.events._bind("create", function( event, repeat,root,runs ){
          console.log("repeat create event");
          console.log(repeat.attributes);
          console.log(root.attributes);
          console.log(runs);
          console.log("end of repeat create event");
     })
     app.models.Repeat.events._bind("delete", function( event, repeat, runs ){
          console.log("repeat delete event");
          console.log(repeat.attributes);
          console.log(runs);
          console.log("end of repeat delete event ");
     })    
     app.models.Repeat.events._bind("detach", function( event, repeat, run ){
          console.log("repeat detach event");
          console.log(repeat.attributes);
          console.log(run);
          console.log("end of repeat detach event ");
     })  
     /*
      * create a media scheduler
      */
     scheduler = new app.models.BannerScheduler({ startDate:"2013-1-1",endDate:"2013-5-28"});
     /*
      * add entities to the scheduler
      */
     for( var i in data ){
        var 
           entityData = data[ i ].entity,
           entity     = new app.models.BannerEntity( entityData ),
           runsData   = data[ i ].runs;
         scheduler.loadEntity( entity );
         for( var j in runsData ){
           var 
             runData = runsData[j],
             run    = new app.models.BannerRun( runData );
             run.addRepeat( new app.models.Repeat(runData.repeat));
           entity.loadRun( run );
         }
     }
     
     /*
      * render the scheduler
      */
     new app.views.BannerScheduler({ model: scheduler }).render();
     scheduler.loadEntity(new app.models.BannerEntity({name:"test"}));

  var tempentity = new app.models.BannerEntity( {name:"shankar"} );
  scheduler.loadEntity( tempentity );
  
  });
</script>
<script type="text/javascript">

 $(function() {
$( ".resizable" ).resizable();
});


</script>



        </div><!--//.page-content-->

<?php include("kl_inc_ace_settings.php"); ?>

      </div><!--/.main-content-->
    </div><!--/.main-container-->

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
      <i class="icon-double-angle-up icon-only bigger-110"></i>
    </a>

    <!--basic scripts-->

    <!--[if !IE]>-->

    <script type="text/javascript">
      window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
    </script>

    <!--<![endif]-->

    <!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

    <script type="text/javascript">
      if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!--page specific plugin scripts-->



    <!--[if lte IE 8]>
      <script src="assets/js/excanvas.min.js"></script>
    <![endif]-->


    <script src="assets/js/chosen.jquery.min.js"></script>
    <script src="assets/js/fuelux/fuelux.spinner.min.js"></script>
    <script src="assets/js/date-time/bootstrap-datepicker.min.js"></script>
    <script src="assets/js/date-time/bootstrap-timepicker.min.js"></script>
    <script src="assets/js/date-time/moment.min.js"></script>
    <script src="assets/js/date-time/daterangepicker.min.js"></script>
    <script src="assets/js/bootstrap-colorpicker.min.js"></script>
    <script src="assets/js/jquery.knob.min.js"></script>
    <script src="assets/js/jquery.autosize-min.js"></script>
    <script src="assets/js/jquery.inputlimiter.1.3.1.min.js"></script>
    <script src="assets/js/jquery.maskedinput.min.js"></script>
    <script src="assets/js/bootstrap-tag.min.js"></script>



    <!--ace scripts-->

    <script src="assets/js/ace-elements.min.js"></script>
    
    <!--inline scripts related to this page-->

    <script>

    jQuery(function($) {
        $(".chosen-select").chosen(); 
        //chosen plugin inside a modal will have a zero width because the select element is originally hidden
        //and its width cannot be determined.
        //so we set the width after modal is show
        $('#modal-form').on('show', function () {
          $(this).find('.chosen-container').each(function(){
            $(this).find('a:first-child').css('width' , '300px');
            $(this).find('.chosen-drop').css('width' , '310px');
            $(this).find('.chosen-search input').css('width' , '300px');
          });
        })
        

        $("#addSequence").on('click',function(e){
          alert("Added:"+ $("#select_sequence").val());
               var tempentity = new app.models.BannerEntity( {name:$("#select_sequence").val()} );
              scheduler.loadEntity( tempentity );
          /*
          if error 
          e.preventDefault();
          */

        });
      });
      </script>

    <script src="assets/js/ace.min.js"></script>
</body>
</html>
