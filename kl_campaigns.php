<?php include("kl_inc_header.php"); ?>
<?php 
	include 'db/dbheader.php';
	include 'db/Icampaign.class.php';
	include 'db/IcampaignDAO.class.php';
?>
	<body>

<?php include("kl_inc_navbar.php"); ?>



<?php include("kl_inc_sidebar.php"); ?>

                    

			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<script type="text/javascript">
						try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
					</script>

			
<?php include("kl_inc_breadcrumbs.php"); ?>

					<?php include("kl_inc_nav_search.php"); ?></div>

				<div class="page-content">
                
                					<div class="page-header position-relative">
						<h1>All Campaigns</h1>
					</div>
                					
				  <div class="row-fluid">
				    <table id="layout_regions" class="table table-striped table-bordered table-hover">
					<thead>
							<tr>
											<th class="center">
												<label>
													<input type="checkbox" class="ace" />
													<span class="lbl"></span>
												</label>
											</th>
											<th>Name</th>
											<th></th>
										</tr>
									</thead>
					<?php
						$campaignDAO=new IcampaignDAO($con);
						$campaigns=$campaignDAO->findByAdvertiser_Id($_GET["advertiser_id"]);
						foreach ($campaigns as $campaign) {
							
							?>
							

									<tbody>
										<tr>
											<td class="center">
												<label>
													<input type="checkbox" class="ace" />
													<span class="lbl"></span>
												</label>
											</td>

											<td>
												<a href="kl_campaign.php?id=<?php echo $campaign->id ?>"><h5><?php echo $campaign->name ?></h5></a>
											</td>
											<td>
						
										
										<!-- adding button group for adding adver types -->
										<div class="btn-group">
											<a class="btn btn-small btn-primary" href="kl_ads.php?<?php echo 'campaign_id='.$campaign->id.'&campaign_name='.$campaign->name.'&advertiser_id='.$_GET['advertiser_id']; ?>">
												<i class="icon-picture bigger-125"></i>
												Edit Ads
											</a>

											<button data-toggle="dropdown" class="btn btn-small btn-primary dropdown-toggle">
												<span class="icon-plus"></span>
											</button>

											<ul class="dropdown-menu">
												<li>
													<a href="#">Video Ad</a>
												</li>
												<li>
													<a href="#">Banner Ad</a>
												</li>
											</ul>

										</div><!--/btn-group-->
										
										
										<a class="btn btn-small btn-yellow" href="kl_campaign.php?id=<?php echo $campaign->id ?>">
											<i class="icon-user bigger-125"></i>
											campaign details
										</a>

										<a class="btn btn-small btn-yellow" href="kl_stats_sequence.php?campaign_id=<?php echo $campaign->id ?>">
											<i class="icon-user bigger-125"></i>
											Show stats
										</a>
											
											
											
											</td>
										</tr>



							<?php
							}
							?>
										
									</tbody>
								</table>
				  </div>






				</div><!--/.page-content-->

<?php include("kl_inc_ace_settings.php"); ?>




			</div><!--/.main-content-->
		</div><!--/.main-container-->

		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
			<i class="icon-double-angle-up icon-only bigger-110"></i>
		</a>

		<!--basic scripts-->

		<!--[if !IE]>-->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!--<![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>

		<!--page specific plugin scripts-->
        
        	<script src="assets/js/jquery.dataTables.min.js"></script>
		<script src="assets/js/jquery.dataTables.bootstrap.js"></script>

		<!--ace scripts-->

		<script src="assets/js/ace-elements.min.js"></script>
		<script src="assets/js/ace.min.js"></script>

		<!--inline scripts related to this page-->

		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#layout_regions').dataTable( );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
		</script>
	</body>
</html>
