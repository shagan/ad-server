<?php

	include 'dbheader.php';
	include 'Iaction.class.php';
	include 'Iaction_custom_properties.class.php';
	include 'Iaction_custom_propertiesDAO.class.php';
	include 'IactionDAO.class.php';
	include 'Iadvertiser.class.php';
	include 'IadvertiserDAO.class.php';
	include 'Ibanner_sequence.class.php';
	include 'Ibanner_sequenceDAO.class.php';
	include 'Icampaign.class.php';
	include 'IcampaignDAO.class.php';
	include 'Ilkadvertisercampaign.class.php';
	include 'IlkadvertisercampaignDAO.class.php';
	include 'Ilkbannerpopup.class.php';
	include 'IlkbannerpopupDAO.class.php';
	include 'Ilkmaincontentpopup.class.php';
	include 'IlkmaincontentpopupDAO.class.php';
	include 'Ilkpopupaction.class.php';
	include 'IlkpopupactionDAO.class.php';
	include 'Ilkregionsegment.class.php';
	include 'IlkregionsegmentDAO.class.php';
	include 'Ilksegmentsequence.class.php';
	include 'IlksegmentsequenceDAO.class.php';
	include 'Imain_content_sequence.class.php';
	include 'Imain_content_sequenceDAO.class.php';
	include 'Ipopup.class.php';
	include 'Ipopup_custom_properties.class.php';
	include 'Ipopup_custom_propertiesDAO.class.php';
	include 'IpopupDAO.class.php';
	include 'Iregion.class.php';
	include 'Iregion_custom_properties.class.php';
	include 'Iregion_custom_propertiesDAO.class.php';
	include 'IregionDAO.class.php';
	include 'Isegment.class.php';
	include 'Isegment_custom_properties.class.php';
	include 'Isegment_custom_propertiesDAO.class.php';
	include 'IsegmentDAO.class.php';
	include 'Isequence.class.php';
	include 'Isequence_custom_properties.class.php';
	include 'Isequence_custom_propertiesDAO.class.php';
	include 'IsequenceDAO.class.php';
	include 'Isponsor.class.php';
	include 'IsponsorDAO.class.php';
	include 'Isponsor_custom_properties.class.php';
	include 'Isponsor_custom_propertiesDAO.class.php';
	include 'Ilksequencesponsor.class.php';
	include 'IlksequencesponsorDAO.class.php';
	include 'Ilksponsorpopup.class.php';
	include 'IlksponsorpopupDAO.class.php';
	include 'Iticker_sequence.class.php';
	include 'Iticker_sequenceDAO.class.php';
	include 'Layout.class.php';
	include 'LayoutDAO.class.php';
	include 'Media.class.php';
	include 'MediaDAO.class.php';
	include 'Lklayoutmedia.class.php';
	include 'LklayoutmediaDAO.class.php';
	include 'Ilkhotareapopup.class.php';
	include 'IlkhotareapopupDAO.class.php';
	include 'Ilksequencehotarea.class.php';
	include 'IlksequencehotareaDAO.class.php';
	include 'Ihotarea.class.php';
	include 'Ihotarea_custom_properties.class.php';
	include 'Ihotarea_custom_propertiesDAO.class.php';
	include 'IhotareaDAO.class.php';
	include 'Iqrcode_actionDAO.class.php';
	include 'util.php';

	$filelist=array();
	function getPopupJson($genid, $con, &$filelist, $popup_id){
		$popupDAO=new IpopupDAO($con);
		$popup = $popupDAO->findById($popup_id);
		$popupObj = new stdClass();		
		$popupObj->id=$popup[0]->id;
		$popupObj->genid=$genid.":P".$popup[0]->id;
		$popupObj->options = new stdClass();
		$popup_cust_prop=new Ipopup_custom_propertiesDAO($con);
		foreach ($popup_cust_prop->findById($popup[0]->id) as $popup_custom_properties) {
			if(is_numeric($popup_custom_properties->value))
				$popup_custom_properties->value=intval($popup_custom_properties->value);
			$popupObj->options->{$popup_custom_properties->property}=$popup_custom_properties->value;
			if($popup_custom_properties->property=="uri")
			{
				if(strstr($popup_custom_properties->value,"http")===false)
					array_push($filelist, $popup_custom_properties->value);
			}
		}

		
		$popactDAO = new IlkpopupactionDAO($con);
		$actionId = $popactDAO->findByPopup_id($popup[0]->id);
		if($actionId[0]->action_id!=null)
			$popupObj->action=getActionJson($popupObj->genid, $con, $filelist, $actionId[0]->action_id);
		return $popupObj;
	}

	function getActionJson($genid, $con, &$filelist, $action_id){
		$actionObj = new stdClass();
		$actionDAO = new IactionDAO($con);
		$action = $actionDAO->findById($action_id);
		$actionObj->id=$action_id;
		$actionObj->genid=$genid.":A".$action_id;
		$actionObj->type=$action[0]->type;
		$action_cust_prop=new Iaction_custom_propertiesDAO($con);
		foreach ($action_cust_prop->findById($action_id) as $action_custom_properties) {
			if(is_numeric($action_custom_properties->value))
				$action_custom_properties->value=intval($action_custom_properties->value);
			$actionObj->options->{$action_custom_properties->property}=$action_custom_properties->value;
			if($action_custom_properties->property=="uri")
			{
				array_push($filelist, $action_custom_properties->value);
			}
			$qrcodeDAO = new Iqrcode_actionDAO($con);
			$qrcode = $qrcodeDAO->findByAction_id($action_id);
			if($qrcode!=null)
				$actionObj->options->qrcode=$qrcode[0]->content;
		}
		return $actionObj;
	}

	function getSponsorJson($genid, $con, &$filelist, $sponsor_id){
		$sponsorObj = new stdClass();
		$sponsorDAO = new IsponsorDAO($con);
		$sponsor = $sponsorDAO->findById($sponsor_id);
		$sponsorObj->id=$sponsor_id;
		$sponsorObj->genid=$genid.":SP".$sponsor_id;
		$sponsor_cust_prop=new Isponsor_custom_propertiesDAO($con);
		foreach ($sponsor_cust_prop->findById($sponsor_id) as $sponsor_custom_properties) {
			if(is_numeric($sponsor_custom_properties->value))
				$sponsor_custom_properties->value=intval($sponsor_custom_properties->value);
			$sponsorObj->options->{$sponsor_custom_properties->property}=$sponsor_custom_properties->value;
			if($sponsor_custom_properties->property=="uri")
			{
				array_push($filelist, $sponsor_custom_properties->value);
			}
		}
		return $sponsorObj;
	}

	function getHotAreaJson($genid, $con, &$filelist, $hotarea_id){
		$hotareaObj = new stdClass();
		$hotareaDAO = new IhotareaDAO($con);
		$hotarea = $hotareaDAO->findById($hotarea_id);
		$hotareaObj->id=$hotarea_id;
		$hotareaObj->genid=$genid.":HA".$hotarea_id;
		$hotarea_cust_prop=new Ihotarea_custom_propertiesDAO($con);
		foreach ($hotarea_cust_prop->findById($hotarea_id) as $hotarea_custom_properties) {
			if(is_numeric($hotarea_custom_properties->value))
				$hotarea_custom_properties->value=intval($hotarea_custom_properties->value);
			$hotareaObj->options->{$hotarea_custom_properties->property}=$hotarea_custom_properties->value;
			if($hotarea_custom_properties->property=="uri")
			{
				array_push($filelist, $hotarea_custom_properties->value);
			}
		}
		return $hotareaObj;
	}

	$layout_id = 12;
	//echo "Starting up";
	$layoutObj = new stdClass();
	$layoutObj->id=$layout_id;
	$layoutObj->options=new stdClass();
	$layoutObj->options->background='88.png';
	$layoutObj->options->height=768;
	$layoutObj->options->width=1024;
	//array_push($filelist, $layoutObj->options->background);
	error_log("Starting up Regions");
	$regionDAO=new IregionDAO($con);
	$regions=$regionDAO->findByLayout_id($layout_id);
	$regionlist=array();
	foreach ($regions as $region) {
		$regionObj = new stdClass();
		$regionObj->id=$region->id;
		error_log("Region:".$region->id);
		$regionObj->type=$region->type;
		$regionObj->options = new stdClass();
		$region_cust_prop=new Iregion_custom_propertiesDAO($con);
		foreach ($region_cust_prop->findById($region->id) as $region_custom_properties) {
			if(is_numeric($region_custom_properties->value))
				$region_custom_properties->value=intval($region_custom_properties->value);
					
			$regionObj->options->{$region_custom_properties->property}=$region_custom_properties->value;
		}
		error_log("Starting up Segments");
		$segmentDAO=new IsegmentDAO($con);
		$segments=$segmentDAO->findByRegion_id($region->id);
		$segmentList=array();
		foreach ($segments as $segment) {
			$segmentObj = new stdClass();
			$segmentObj->name=$segment->name;
			error_log("Segment:".$segment->id);
			$segment_cust_prop=new Isegment_custom_propertiesDAO($con);
			foreach ($segment_cust_prop->findById($segment->id) as $segment_custom_properties) {
				$segmentObj->options->{$segment_custom_properties->property}=$segment_custom_properties->value;
			}
			/*
			Setup for today's run
			*/
			$lksegmentsequenceDAO = new IlksegmentsequenceDAO($con);
			$lksegmentsequenceDAO->deleteSequenceForSegment($segment->id);
			$lksegmentsequenceDAO->insertTodaysSequencesForSegment($segment->id);
			error_log("Done resetting sequence list for segment");
			/*
			done
			*/
			error_log("Starting up Sequence");
			$sequenceDAO=new IsequenceDAO($con);
			$sequences = $sequenceDAO->findBySegment_id($segment->id);
			$sequenceList=array();
			foreach ($sequences as $sequence) {
				$sequenceObj = new stdClass();
				$dataObj = new stdClass();
				error_log("Sequence:".$sequence->id);
				$dataObj->id = $sequence->id;
				$dataObj->genid = "L".$layoutObj->id.":S".$sequence->id;
				$sequence_cust_prop=new Isequence_custom_propertiesDAO($con);
				foreach ($sequence_cust_prop->findById($sequence->id) as $sequence_custom_properties) {
					if(is_numeric($sequence_custom_properties->value))
						$sequence_custom_properties->value=intval($sequence_custom_properties->value);
					$dataObj->options->{$sequence_custom_properties->property}=$sequence_custom_properties->value;
					error_log("Property:".$sequence_custom_properties->property);
					error_log("Value:".$sequence_custom_properties->value);
					if($sequence_custom_properties->property=="uri")
					{
						array_push($filelist, $sequence_custom_properties->value);
					}
					error_log("Options:".json_encode($dataObj->options));
				}
				

				// /* Type specific work */
				
				if($sequence->type=="com.instillo.layout.model.BannerSequence"){
					$bannerpopDAO = new IlkbannerpopupDAO($con);
					$banner = $bannerpopDAO->findBySequence_id($sequence->id);
					if($banner[0]->popup_id!=null)
						$dataObj->popup=getPopupJson($dataObj->genid, $con, $filelist, $banner[0]->popup_id);
				}
				elseif ($sequence->type=="com.instillo.layout.model.MainContentSequence") {
					$mainactDAO = new IlkmaincontentpopupDAO($con);
					$mainseq = $mainactDAO->findBySequence_id($sequence->id);
					if($mainseq[0]->popup_id!=null)
						$dataObj->popup=getPopupJson($dataObj->genid, $con, $filelist, $mainseq[0]->popup_id);
				}
				elseif ($sequence->type=="com.instillo.layout.model.SponsorBannerSequence") {
					$seqhotDAO = new IlksequencehotareaDAO($con);
					$hotareas = $seqhotDAO->findBySequence_id($sequence->id);
					$hotareaList = array();
					foreach ($hotareas as $hotarea) {
						$hotareaObj = getHotAreaJson($dataObj->genid, $con, $filelist, $hotarea->hotarea_id);
						$hotareapopDAO = new IlkhotareapopupDAO($con);
						$popupObj = $hotareapopDAO->findByHotarea_id($hotarea->hotarea_id);
						if($popupObj[0]->popup_id!=null)
							$hotareaObj->popup=getPopupJson($hotareaObj->genid,$con, $filelist, $popupObj[0]->popup_id);
						array_push($hotareaList , $hotareaObj);
					}
					$dataObj->hotAreas=$hotareaList;
				}
				elseif ($sequence->type=="com.instillo.layout.model.TickerSequence") {
					$seqSponDAO = new IlksequencesponsorDAO($con);
					$sponsor = $seqSponDAO->findBySequence_id($sequence->id);
					if($sponsor[0]->sponsor_id!=null)
						$dataObj->sponsor=getSponsorJson($dataObj->genid,$con, $filelist, $sponsor[0]->sponsor_id);

					$sponsorpopDAO = new IlksponsorpopupDAO($con);
					$popup = $sponsorpopDAO->findBySponsor_id($sponsor[0]->sponsor_id);
					if($popup[0]->popup_id!=null)
						$dataObj->sponsor->popup=getPopupJson($dataObj->sponsor->genid,$con, $filelist, $popup[0]->popup_id);
				}
				// else{

				// }
				$sequenceObj->type=$sequence->type;
				$sequenceObj->data = $dataObj;
				array_push($sequenceList,$sequenceObj);

			}
			if(count($sequenceList)>0){
				$segmentObj->sequences=$sequenceList;
				array_push($segmentList,$segmentObj);
			}
		}
		if(count($segmentList)>0){
			$regionObj->segments=$segmentList;
			array_push($regionlist,$regionObj);
		}
		
	}
	$layoutObj->regions=$regionlist;
	$con->beginTransaction();
	//echo jsonpp(json_encode($filelist));
	$lklayoutmediaDAO = new LklayoutmediaDAO($con);
	$lklayoutmediaDAO->deleteAllforLayoutId($layout_id);
	$filelist = array_unique($filelist);
	foreach ($filelist as $file) {
		error_log($file);
		$lklayoutmedia = new Lklayoutmedia();
		$mediaDAO = new MediaDAO($con);
		$media  = $mediaDAO->findByStoredAs($file);
		//var_dump($media);
		$lklayoutmedia->mediaID = $media[0]->mediaID;
		$lklayoutmedia->layoutID = $layout_id;
		$lklayoutmedia->regionID = "dummy";
		error_log(json_encode($lklayoutmedia));
		$lklayoutmediaDAO->insert($lklayoutmedia);
	}

	$layoutDAO = new LayoutDAO($con);
	$layout = $layoutDAO->load($layout_id);
	$layout->xml = jsonpp(json_encode($layoutObj));
	$layoutDAO->update($layout);
	$con->commitTransaction();
	echo jsonpp(json_encode($layoutObj));
?>