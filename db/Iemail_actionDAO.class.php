<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/Iemail_actionDAOAbstract.class.php';
class Iemail_actionDAO extends Iemail_actionDAOAbstract {

	public function updatevalue($iemail_action) {
		$ps = new PreparedStatement("INSERT INTO iemail_action 
		(action_id, subject, body) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE subject=?, body=?");
		$ps->setInt($iemail_action->action_id);
		$ps->setString($iemail_action->subject);
		$ps->setString($iemail_action->body);
		$ps->setString($iemail_action->subject);
		$ps->setString($iemail_action->body);
		return $this->connection->executeUpdate($ps);
	}

}