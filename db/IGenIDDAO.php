<?php

class IGenIDDAO {

	public function __construct($connection) {
		$this->connection = $connection;
	}

	public function getPopUpforBannerSequence($layout_id,$sequence_id){
		$sql = "SELECT concat('L',CONVERT(layout.layoutid,CHAR(8)),':S',CONVERT(isequence.id,CHAR(8)),':HA',CONVERT(ilksequencehotarea.hotarea_id,CHAR(8)),':P',CONVERT(ilkhotareapopup.popup_id,CHAR(8))) as genID FROM layout,isequence,ilksequencehotarea,ilkhotareapopup where layout.layoutid=? and isequence.id=? and ilksequencehotarea.sequence_id=? and ilkhotareapopup.hotarea_id = ilksequencehotarea.hotarea_id";
		$ps = new PreparedStatement($sql);
		$ps->setInt($layout_id);
		$ps->setInt($sequence_id);
		$ps->setInt($sequence_id);
		$rows = array();
		$rs = $this->connection->executeQuery($ps);
		while ($arr = $this->connection->fetchArray($rs)) {
			$rows[] = $arr['genID'];
		}
		$this->connection->freeResult($rs);
		return $rows;
	}
}

