<?php
// This file can be edited (within reason) to extend the functionality
// of the generated (abstract) DAO class.

include dirname(__FILE__).'/abstract/IcampaignDAOAbstract.class.php';
class IcampaignDAO extends IcampaignDAOAbstract {

	public function findByAdvertiser_Id($id){
		$sql = "SELECT * FROM icampaign where id in ( select campaign_id from `ilkadvertisercampaign` WHERE advertiser_id=?)";
		$ps = new PreparedStatement($sql);
		$ps->setInt($id);
		return parent::findWithPreparedStatement($ps);
	}

	public function listAdvertiserCampaigns(){
		$sql = "SELECT icampaign.id AS campaign_id, iadvertiser.id AS advertiser_id, iadvertiser.name as advertiser_name, icampaign.name as campaign_name FROM iadvertiser, icampaign, ilkadvertisercampaign WHERE ilkadvertisercampaign.advertiser_id = iadvertiser.id AND ilkadvertisercampaign.campaign_id = icampaign.id";
		$ps = new PreparedStatement($sql);
		$rows = array();
		$rs = $this->connection->executeQuery($ps);
		while ($arr = $this->connection->fetchArray($rs)) {
			$rows[] = $arr;
		}
		$this->connection->freeResult($rs);
		return $rows;
	}
}
