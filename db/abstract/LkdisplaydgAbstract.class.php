<?php
// Generated automatically by phpdaogen.
// Do NOT edit this file.
// Any changes made to this file will be overwritten the next time it is generated.

abstract class LkdisplaydgAbstract {
	public $LkDisplayDGID;
	public $DisplayGroupID;
	public $DisplayID;

	public static function createDefault() {
		$v = new Lkdisplaydg();
		$v->defaultAllFields();
		return $v;
	}

	public function defaultAllFields() {
		$this->LkDisplayDGID = 0;
		$this->DisplayGroupID = 0;
		$this->DisplayID = 0;
		return $this;
	}

	public function loadFromArray($arr) {
		$this->LkDisplayDGID = isset($arr['LkDisplayDGID']) ? (int)$arr['LkDisplayDGID'] : 0;
		$this->DisplayGroupID = isset($arr['DisplayGroupID']) ? (int)$arr['DisplayGroupID'] : 0;
		$this->DisplayID = isset($arr['DisplayID']) ? (int)$arr['DisplayID'] : 0;
		return $this;
	}
}
