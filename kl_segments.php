<?php include("kl_inc_header.php"); ?>
<?php 
		include 'db/dbheader.php';
		include 'db/Isegment.class.php';
		include 'db/IsegmentDAO.class.php';
		include 'db/IrunDAO.class.php';
?>
<body>
<?php include("kl_inc_navbar.php"); ?>
<?php include("kl_inc_sidebar.php"); ?>
	<div class="main-content">
		<div class="breadcrumbs" id="breadcrumbs"> 
			<script type="text/javascript">
						try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
					</script>
			<?php include("kl_inc_breadcrumbs.php"); ?>
			
			
			<?php include("kl_inc_nav_search.php"); ?></div>
		<div class="page-content">
			<div class="page-header position-relative">
				<h1> All Segments <small> <i class="icon-double-angle-right"></i>for KL Group Alpha , </small> </h1>
			</div>
			<!--/.page-header-->
			
			

			<div class="row-fluid">
				
					<!--PAGE CONTENT BEGINS-->
					
					<div class="span12"> 
					
					<div class="row-fluid">

					
					
						<div  class="span6 widget-container-span ui-sortable">
					<?php
						$segmentDAO=new IsegmentDAO($con);
						$segments=$segmentDAO->findByRegion_id($_GET['id']);
						foreach ($segments as $segment) {
								
							?>
					
						

						
							<div id="<?php echo $segment->id ?>" class="widget-box" style="opacity: 1;">
								<div class="widget-header">
									
									
									<h2><?php echo $segment->name ?></h2>
									
									<div class="widget-toolbar">
												<a data-action="collapse" href="#">
													<i class="icon-chevron-up"></i>
												</a>
											</div> 
		
								
								</div>
								<div class="widget-body">
									<div class="widget-body-inner" style="display: block;">
										<div class="widget-main">
										
										
																		<div class="infobox infobox-green  ">
																		<div class="infobox-icon">
																			<i class="icon-check-sign"></i>
																		</div>
								
																		<div class="infobox-data">
																			<span class="infobox-data-number"><?php $runDAO = new IrunDAO($con);$runs=$runDAO->countRunsForToday($segment->id); echo $runs; ?></span>
																			<div class="infobox-content">running today</div>
																		</div>
																		
																	</div>
										
															
																<div style="float:right">
															
																	<a class="btn btn-app btn-light btn-mini" href="kl_segments_settings.php#">
																		<i class="icon-gears bigger-150"></i>
																		settings
																	</a>
															
															
																<a class="btn btn-app btn-pink btn-mini" href="kl_scheduler_banner.php?id=<?php echo $segment->id ?>">
																	<i class="icon-tasks bigger-150"></i>
																	timeline
																</a>
																
																
																
															</div>
															
													
											
										
										
										</div>
									</div>
								</div>
							</div>
						

					<?php
							}
						?>
					</div>
						<!--/span--> 

					</div>
					
			
					</div>
					
					<!--PAGE CONTENT ENDS--> 
				</div>
				<!--/.span--> 
			</div>
		</div>
		<!--/.row-fluid--> 
	</div>
	<!--/.page-content-->
	
	<?php include("kl_inc_ace_settings.php"); ?>
</div>
<!--/.main-content-->
</div>
<!--/.main-container-->

<?php include("kl_inc_scrollup.php"); ?>

<!--basic scripts--> 

<!--[if !IE]>--> 

<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script> 

<!--<![endif]--> 

<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]--> 

<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script> 
<script src="assets/js/bootstrap.min.js"></script> 

<!--page specific plugin scripts-->

		<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>

		<!--ace scripts-->

		<!--inline scripts related to this page-->

		<script type="text/javascript">
				var initialOrder = new Array();
				<?php 
				//echo json_encode($segments);
				$i=0;
				foreach ($segments as $segment) {
							echo "initialOrder[$i]=$segment->id;";
							$i++;
						}		
				?>

			    $('.widget-container-span').sortable({
			        connectWith: '.widget-container-span',
					items:'> .widget-box',
					opacity:0.8,
					revert:true,
					forceHelperSize:true,
					placeholder: 'widget-placeholder',
					forcePlaceholderSize:true,
					tolerance:'pointer',
					update: function( event, ui ) {
						console.log($('.widget-container-span').sortable('toArray'));
						$.ajax({
		                              type:"post",
		                              url:"db/process_segments.php",
		                              data:"action=sort&region_id=<?php echo $_GET['id'] ?>&data="+$('.widget-container-span').sortable('toArray'),
		                              success:function(data){
		                                 console.log(data);
		                              }
		 
		                        });
					}
			    });
				
		</script>


		


</body>
</html>
